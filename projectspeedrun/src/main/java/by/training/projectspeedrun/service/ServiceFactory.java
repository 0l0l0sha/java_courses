package by.training.projectspeedrun.service;

import by.training.projectspeedrun.exeptions.PersistentException;

public interface ServiceFactory {
    <Type extends Service> Type getService(Class<Type> key) throws PersistentException;

}
