package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.connectionpool.ConnectionPool;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ServiceFactoryImpl implements ServiceFactory {

    private static final Logger LOGGER = LogManager.getLogger(ServiceFactoryImpl.class);

    private static final Map<Class<? extends Service>, Service> SERVICES = new ConcurrentHashMap<>();

    static {
        SERVICES.put(RunnerService.class, new RunnerServiceImpl());
        SERVICES.put(GameService.class, new GameServiceImpl());
        SERVICES.put(CategoryOfRunService.class, new CategoryOfRunServiceImpl());
        SERVICES.put(RunService.class, new RunServiceImpl());
        SERVICES.put(RunnerGameService.class, new RunnerGameServiceImpl());
    }


    @Override
    @SuppressWarnings("unchecked")
    public <Type extends Service> Type getService(Class<Type> key) throws PersistentException {
        return (Type) SERVICES.get(key);
    }


}
