package by.training.projectspeedrun.service;

import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunService extends Service {

    Run findByIdentity(Integer identity) throws PersistentException;

    List<Run>  findByCategory(int categoryId) throws PersistentException;

    List<Run>  findByRunner(int runnerId) throws PersistentException;

    List<Run>  findByCategoryAndRunner(int categoryId, int runnerId) throws PersistentException;

    void save(Run run) throws PersistentException;

    void delete(Integer identity) throws PersistentException;
}
