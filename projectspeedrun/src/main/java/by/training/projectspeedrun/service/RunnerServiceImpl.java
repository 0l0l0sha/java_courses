package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.RunnerDao;
import by.training.projectspeedrun.dao.RunnerGameDao;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class RunnerServiceImpl extends ServiceImpl implements RunnerService {
    RunnerServiceImpl() {
        super();
    }

    @Override
    public List<Runner> findAll() throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        List<Runner> list = dao.read();
        factory.close(dao);
        return list;
    }

    @Override
    public List<Runner> findLike(String like) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        List<Runner> list = dao.readLike("%"+like+"%");
        factory.close(dao);
        return list;
    }

    @Override
    public List<Runner> findModerators(int gameId) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        List<Integer> runnerIds = runnerGameDao.readModerators(gameId);
        List<Runner> runners = new ArrayList<>();
        factory.close(runnerGameDao);
        for (int id : runnerIds) {
            Runner runner = dao.read(id);
            runners.add(runner);
        }
        return runners;

    }

    @Override
    public Runner findByIdentity(Integer identity) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        Runner runner = dao.read(identity);
        factory.close(dao);
        return runner;
    }


    @Override
    public Runner findByLoginAndPassword(String login, String password) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        Runner runner = dao.read(login, password);
        factory.close(dao);
        return runner;
    }

    @Override
    public Runner findByLogin(String login) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        Runner runner = dao.read(login);
        factory.close(dao);
        return runner;
    }

    @Override
    public void save(Runner runner) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        if (runner.getId() != null) {
            if (runner.getPass() != null) {
                runner.setPass(runner.getPass());
            } else {
                Runner oldUser = dao.read(runner.getId());
                runner.setPass(oldUser.getPass());
            }
            dao.update(runner);
        } else {
            if (runner.getPass() == null) {
                runner.setPass(new String());
            }
            runner.setId(dao.create(runner));
        }
        factory.close(dao);
    }

    @Override
    public void delete(Integer identity) throws PersistentException {
        RunnerDao dao = factory.createDao(RunnerDao.class);
        dao.delete(identity);
        factory.close(dao);
    }

    private String md5(String string) {
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("md5");
            digest.reset();
            digest.update(string.getBytes());
            byte hash[] = digest.digest();
            Formatter formatter = new Formatter();
            for (int i = 0; i < hash.length; i++) {
                formatter.format("%02X", hash[i]);
            }
            String md5summ = formatter.toString();
            formatter.close();
            return md5summ;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
