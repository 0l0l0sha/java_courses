package by.training.projectspeedrun.service;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface CategoryOfRunService extends Service {
    List<CategoryOfRun> findAllInGame(Game game) throws PersistentException;

    CategoryOfRun findByIdentity(Integer identity) throws PersistentException;

    CategoryOfRun findByTitleAndGame( String categoryTitle,Game game) throws PersistentException;

    void save(CategoryOfRun game) throws PersistentException;

    void delete(Integer identity) throws PersistentException;
}
