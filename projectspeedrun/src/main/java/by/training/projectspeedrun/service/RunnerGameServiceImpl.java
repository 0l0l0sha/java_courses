package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.RunnerGameDao;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public class RunnerGameServiceImpl extends ServiceImpl implements RunnerGameService {




    @Override
    public RunnerGame findbyPlayerAndGame(int runnerId, int gameId) throws PersistentException {
        RunnerGameDao dao = factory.createDao(RunnerGameDao.class);
        RunnerGame runnerGame = dao.readByPlayerAndGame(runnerId,gameId);
        factory.close(dao);
        return runnerGame;
    }



    @Override
    public void save(RunnerGame runnerGame) throws PersistentException {
        RunnerGameDao dao = factory.createDao(RunnerGameDao.class);
        RunnerGame check = dao.readByPlayerAndGame(runnerGame.getRunnerId(),runnerGame.getGameId());
        if(check != null)
        {
            dao.update(runnerGame);
        }else {
            dao.create(runnerGame);
        }
        factory.close(dao);
    }

    @Override
    public void delete(RunnerGame runnerGame) throws PersistentException {
        RunnerGameDao dao = factory.createDao(RunnerGameDao.class);
        dao.delete(runnerGame);
        factory.close(dao);
    }


    @Override
    public int QuantityByGame(int gameId) throws PersistentException {
        RunnerGameDao dao = factory.createDao(RunnerGameDao.class);
        int quantity = dao.QuantityByGame(gameId);
        factory.close(dao);
        return quantity;
    }
}
