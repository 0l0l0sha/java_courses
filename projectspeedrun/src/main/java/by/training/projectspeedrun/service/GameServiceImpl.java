package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.GameDao;
import by.training.projectspeedrun.dao.RunnerGameDao;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.ArrayList;
import java.util.List;

public class GameServiceImpl extends ServiceImpl implements GameService {
    @Override
    public List<Game> findAll() throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        List<Game> games = dao.read();
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        for (Game game : games) {
            int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
            game.setFollowersQuantity(followersQuantity);
        }
        factory.close(dao);
        return games;
    }

    @Override
    public List<Game> findByPlayer(int runnerId) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        List<Integer> gameIds = runnerGameDao.readByPlayer(runnerId);
        List<Game> games = new ArrayList<>();
        for (int id : gameIds) {
            Game game = dao.read(id);
            int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
            game.setFollowersQuantity(followersQuantity);
            games.add(game);
        }
        factory.close(dao);
        factory.close(runnerGameDao);
        return games;
    }

    @Override
    public List<Game> findModerate(int runnerId) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        List<Game> games = new ArrayList<>();
        List<Integer> gameIds = runnerGameDao.readModerate(runnerId);
        for (int id : gameIds) {
            Game game = dao.read(id);
            int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
            game.setFollowersQuantity(followersQuantity);
            games.add(game);
        }

        factory.close(dao);
        factory.close(runnerGameDao);
        return games;
    }

    @Override
    public Game findByIdentity(Integer identity) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        Game game = dao.read(identity);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
        game.setFollowersQuantity(followersQuantity);
        factory.close(dao);
        return game;
    }

    @Override
    public Game findByTitle(String gameTitle) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        Game game = dao.readByTitle(gameTitle);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
        game.setFollowersQuantity(followersQuantity);
        factory.close(dao);
        return game;
    }

    @Override
    public List<Game> findLike(String like) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        RunnerGameDao runnerGameDao = factory.createDao(RunnerGameDao.class);
        List<Game> games = dao.readLike("%"+like+"%");
        for (Game game : games) {
            int followersQuantity = runnerGameDao.QuantityByGame(game.getId());
            game.setFollowersQuantity(followersQuantity);
        }
        factory.close(dao);
        factory.close(runnerGameDao);
        return games;
    }

    @Override
    public void save(Game game) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        if (game.getId() != null) {
            dao.update(game);
        } else {
            game.setId(dao.create(game));
        }
        factory.close(dao);
    }

    @Override
    public void delete(Integer identity) throws PersistentException {
        GameDao dao = factory.createDao(GameDao.class);
        dao.delete(identity);
        factory.close(dao);
    }
}
