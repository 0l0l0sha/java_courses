package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.RunDao;
import by.training.projectspeedrun.dao.RunnerDao;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;


public class RunServiceImpl extends ServiceImpl implements RunService {

    @Override
    public Run findByIdentity(Integer identity) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        Run run = dao.read(identity);
        factory.close(dao);
        return run;
    }

    @Override
    public List<Run> findByCategory(int categoryId) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        List<Run> run = dao.readByCategory(categoryId);
        factory.close(dao);
        return run;
    }

    @Override
    public List<Run> findByRunner(int runnerId) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        List<Run> run = dao.readByRunner(runnerId);
        factory.close(dao);
        return run;
    }

    @Override
    public List<Run> findByCategoryAndRunner(int categoryId, int runnerId) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        List<Run> run = dao.readByCategoryAndRunner(categoryId, runnerId);
        factory.close(dao);
        return run;
    }

    @Override
    public void save(Run run) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        if (run.getId() != null) {
            dao.update(run);
        } else {
            run.setId(dao.create(run));
        }
        factory.close(dao);
    }

    @Override
    public void delete(Integer identity) throws PersistentException {
        RunDao dao = factory.createDao(RunDao.class);
        dao.delete(identity);
        factory.close(dao);
    }
}
