package by.training.projectspeedrun.service;

import by.training.projectspeedrun.dao.CategoryOfRunDao;
import by.training.projectspeedrun.dao.impl.CategoryOfRunDaoImpl;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public class CategoryOfRunServiceImpl extends ServiceImpl implements CategoryOfRunService {
    @Override
    public List<CategoryOfRun> findAllInGame(Game game) throws PersistentException {
        CategoryOfRunDao dao = factory.createDao(CategoryOfRunDao.class);
        List<CategoryOfRun> categories = dao.readByGame(game);
        factory.close(dao);
        return categories;
    }

    @Override
    public CategoryOfRun findByIdentity(Integer identity) throws PersistentException {
        CategoryOfRunDao dao = factory.createDao(CategoryOfRunDao.class);
        CategoryOfRun category = dao.read(identity);
        factory.close(dao);
        return category;
    }

    @Override
    public CategoryOfRun findByTitleAndGame(String categoryTitle, Game game) throws PersistentException {
        CategoryOfRunDao dao = factory.createDao(CategoryOfRunDao.class);
        CategoryOfRun category = dao.readByTitleAndGame(categoryTitle, game);
        factory.close(dao);
        return category;
    }

    @Override
    public void save(CategoryOfRun category) throws PersistentException {
        CategoryOfRunDao dao = factory.createDao(CategoryOfRunDao.class);
        if (category.getId() != null) {
            dao.update(category);
        } else {
            category.setId(dao.create(category));
        }
        factory.close(dao);
    }

    @Override
    public void delete(Integer identity) throws PersistentException {
        CategoryOfRunDao dao = factory.createDao(CategoryOfRunDao.class);

        dao.delete(identity);
        factory.close(dao);
    }
}
