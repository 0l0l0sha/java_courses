package by.training.projectspeedrun.service;

import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunnerService extends  Service{
    List<Runner> findAll() throws PersistentException;

    List<Runner> findLike(String like) throws PersistentException;

    List<Runner> findModerators(int gameId) throws PersistentException;

    Runner findByIdentity(Integer identity) throws PersistentException;

    Runner findByLoginAndPassword(String login, String password) throws PersistentException;

    Runner findByLogin(String login) throws PersistentException;

    void save(Runner user) throws PersistentException;

    void delete(Integer identity) throws PersistentException;

}
