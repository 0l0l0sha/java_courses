package by.training.projectspeedrun.service;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunnerGameService extends  Service {



    RunnerGame findbyPlayerAndGame(int runnerId, int gameId) throws PersistentException;

    void save(RunnerGame runnerGame) throws PersistentException;

    void delete(RunnerGame runnerGame) throws PersistentException;

    int QuantityByGame(int gameId) throws PersistentException;

}
