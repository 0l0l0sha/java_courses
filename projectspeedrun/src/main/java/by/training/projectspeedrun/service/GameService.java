package by.training.projectspeedrun.service;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface GameService extends Service  {
    List<Game> findAll() throws PersistentException;

    Game findByIdentity(Integer identity) throws PersistentException;

    Game findByTitle(String gameTitle) throws PersistentException;

    List<Game> findLike(String like) throws PersistentException;

    List<Game> findByPlayer(int runnerId) throws PersistentException;

    List<Game> findModerate(int runnerId) throws PersistentException;

    void save(Game game) throws PersistentException;

    void delete(Integer identity) throws PersistentException;



}
