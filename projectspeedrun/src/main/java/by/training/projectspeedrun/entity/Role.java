package by.training.projectspeedrun.entity;

public enum Role {
    RUNNER("ранер"),
    ADMINISTRATOR("администратор");


    private String name;

    Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getIdentity() {
        return ordinal();
    }

    public static Role getByIdentity(Integer identity) {
        return Role.values()[identity];
    }
}
