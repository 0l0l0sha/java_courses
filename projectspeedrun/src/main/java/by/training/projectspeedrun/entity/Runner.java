package by.training.projectspeedrun.entity;

public class Runner {
    private Integer id;
    private String email;
    private String login;
    private String pass;
    private Role role;
    private String country;
    private String twitch;
    private String youtube;
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTwitch() {
        return twitch;
    }

    public void setTwitch(String twitch) {
        this.twitch = twitch;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Runner{" + "id=" + id + ", email='" + email + '\'' + ", login='" + login + '\'' + ", pass='" + pass + '\'' + ", role=" + role + ", country='" + country + '\'' + ", twitch='" + twitch + '\'' + ", youtube='" + youtube + '\'' + ", image=" + image + '}';
    }
}
