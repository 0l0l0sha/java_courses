package by.training.projectspeedrun.entity;

import java.sql.Time;
import java.sql.Date;

public class Run {
    private Integer id;
    private CategoryOfRun category;
    private Runner runner;
    private Time realTime;
    private Time inGameTime;
    private Date date;
    private String video;
    private Platform platform;
    private boolean submited;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CategoryOfRun getCategory() {
        return category;
    }

    public void setCategory(CategoryOfRun category) {
        this.category = category;
    }

    public Runner getRunner() {
        return runner;
    }

    public void setRunner(Runner runner) {
        this.runner = runner;
    }

    public Time getRealTime() {
        return realTime;
    }

    public void setRealTime(Time realTime) {
        this.realTime = realTime;
    }

    public Time getInGameTime() {
        return inGameTime;
    }

    public void setInGameTime(Time inGameTime) {
        this.inGameTime = inGameTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public boolean getSubmited() {
        return submited;
    }

    public void setSubmited(boolean submited) {
        this.submited = submited;
    }
}
