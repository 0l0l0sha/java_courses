package by.training.projectspeedrun.dao;


import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface GameDao extends Dao<Game> {

    Game readByTitle (String title) throws PersistentException;

    List<Game> read() throws PersistentException;

    List<Game> readLike(String like) throws PersistentException;

}
