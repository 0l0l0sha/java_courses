package by.training.projectspeedrun.dao;


import by.training.projectspeedrun.exeptions.PersistentException;


public interface Dao<Type> {
    int  create(Type entity) throws PersistentException;

    Type read(Integer id) throws PersistentException;

    void update(Type entity) throws PersistentException;

    void delete(Integer id) throws PersistentException;
}
