package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.GameDao;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.plugins.convert.TypeConverters;
import org.apache.logging.log4j.core.util.IOUtils;

import javax.sql.rowset.serial.SerialBlob;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;



public class GameDaoImpl extends BaseDaoImpl implements GameDao {

    private static final Logger LOGGER = LogManager.getLogger(GameDaoImpl.class);

    @Override
    public Game readByTitle(String title) throws PersistentException {
        String sql = "SELECT `id`, `description`, `logo` FROM `game`" + "WHERE `gameTitle` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, title);
            try (ResultSet resultSet = statement.executeQuery()) {
                Game game = null;
                if (resultSet.next()) {
                    game = new Game();
                    game.setId(resultSet.getInt("id"));
                    game.setGameTitle(title);
                    game.setDescription(resultSet.getString("description"));
                    Blob blob = resultSet.getBlob("logo");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String logo = new String(Base64.getEncoder().encode(imgData));
                    game.setLogo(logo);
                }
                return game;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Game> read() throws PersistentException {
        String sql = "SELECT `id`, `gameTitle`,`description`, `logo` FROM `game` "
                + "ORDER BY `gameTitle`";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Game> games = new ArrayList<>();
                Game game = null;
                while (resultSet.next()) {
                    game = new Game();
                    game.setId(resultSet.getInt("id"));
                    game.setGameTitle(resultSet.getString("gameTitle"));
                    game.setDescription(resultSet.getString("description"));
                    Blob blob = resultSet.getBlob("logo");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String logo = new String(Base64.getEncoder().encode(imgData));
                    game.setLogo(logo);
                    games.add(game);
                }
                return games;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Game> readLike(String like) throws PersistentException {
        String sql = "SELECT `id`, `gameTitle`,`description`, `logo` FROM `game` "
                + "WHERE `gameTitle` LIKE ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1,like);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Game> games = new ArrayList<>();
                Game game;
                while (resultSet.next()) {
                    game = new Game();
                    game.setId(resultSet.getInt("id"));
                    game.setGameTitle(resultSet.getString("gameTitle"));
                    game.setDescription(resultSet.getString("description"));
                    Blob blob = resultSet.getBlob("logo");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String logo = new String(Base64.getEncoder().encode(imgData));
                    game.setLogo(logo);
                    games.add(game);
                }
                return games;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public int create(Game game) throws PersistentException {
        String sql = "INSERT INTO `game` (`gameTitle`, `description`, `logo`)"
                + " VALUES (?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, game.getGameTitle());
            statement.setString(2, game.getDescription());
            String encoded = game.getLogo();
            byte[] logo = Base64.getDecoder().decode(encoded.getBytes());
            Blob blob = connection.createBlob();
            blob.setBytes(1, logo);
            statement.setBlob(3, blob);
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    LOGGER.error("There is no autoincremented index after trying to add record into table `game`");
                    throw new PersistentException();
                }
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public Game read(Integer id) throws PersistentException {
        String sql = "SELECT `gameTitle`, `description`, `logo` FROM `game`"
                + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            Game game = null;
            if (resultSet.next()) {
                game = new Game();
                game.setId(id);
                game.setGameTitle(resultSet.getString("gameTitle"));
                game.setDescription(resultSet.getString("description"));
                Blob blob = resultSet.getBlob("logo");
                byte[] imgData = blob.getBytes(1,(int)blob.length());
                String logo = new String(Base64.getEncoder().encode(imgData));
                game.setLogo(logo);
            }
            return game;
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void update(Game game) throws PersistentException {
        String sql = "UPDATE `game` SET `gameTitle` = ?, `description` = ?, `logo` = ?" + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, game.getGameTitle());
            statement.setString(2, game.getDescription());
            String encoded = game.getLogo();
            byte[] logo = Base64.getDecoder().decode(encoded.getBytes());
            Blob blob = connection.createBlob();
            blob.setBytes(1, logo);
            statement.setBlob(3, blob);
            statement.setInt(4, game.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void delete(Integer id) throws PersistentException {
        String sql = "DELETE FROM `game` WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }
}

