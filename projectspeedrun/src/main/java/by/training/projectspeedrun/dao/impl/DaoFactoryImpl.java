package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.CategoryOfRunDao;
import by.training.projectspeedrun.dao.Dao;
import by.training.projectspeedrun.dao.DaoFactory;
import by.training.projectspeedrun.dao.GameDao;
import by.training.projectspeedrun.dao.RunDao;
import by.training.projectspeedrun.dao.RunnerDao;
import by.training.projectspeedrun.dao.RunnerGameDao;
import by.training.projectspeedrun.dao.connectionpool.ConnectionPool;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DaoFactoryImpl implements DaoFactory {

    private Map daoConection = new ConcurrentHashMap();

    private static final Logger LOGGER = LogManager.getLogger(CategoryOfRunDaoImpl.class);

    private static Map<Class<? extends Dao<?>>, Class<? extends BaseDaoImpl>> daoClass = new ConcurrentHashMap<>();
    static {
        daoClass.put(RunnerDao.class, RunnerDaoImpl.class);
        daoClass.put(GameDao.class, GameDaoImpl.class);
        daoClass.put(CategoryOfRunDao.class, CategoryOfRunDaoImpl.class);
        daoClass.put(RunDao.class, RunDaoImpl.class);
        daoClass.put(RunnerGameDao.class, RunnerGameDaoImpl.class);
    }


    @SuppressWarnings("unchecked")
    @Override
    public <Type extends Dao<?>> Type createDao(Class<Type> key) throws PersistentException {
        Class<? extends BaseDaoImpl> value = daoClass.get(key);
        if(value != null) {
            try {
                BaseDaoImpl dao = value.newInstance();
                Connection connection = ConnectionPool.getInstance().getConnection();
                dao.setConnection(connection);
                daoConection.put(dao,connection);
                return (Type) dao;
            } catch (InstantiationException | IllegalAccessException e) {
                LOGGER.error("It is impossible to create data access object", e);
                throw new PersistentException(e);
            }
        }
        return null;
    }


    @Override
    public void close(Dao dao){
        Connection connection = (Connection) daoConection.get(dao);
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        daoConection.remove(dao);
    }

}
