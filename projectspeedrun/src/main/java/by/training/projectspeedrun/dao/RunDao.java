package by.training.projectspeedrun.dao;

import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunDao extends Dao<Run> {

    List<Run> readByCategory (int categoryIdentity) throws PersistentException;

    List<Run> readByRunner (int runnerIdentity) throws PersistentException;

    List<Run> readByCategoryAndRunner (int categoryIdentity, int runnerIdentity) throws PersistentException;

}
