package by.training.projectspeedrun.dao;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface CategoryOfRunDao extends Dao<CategoryOfRun>{
    CategoryOfRun readByTitleAndGame (String title, Game game) throws PersistentException;

    List<CategoryOfRun> readByGame (Game game) throws PersistentException;

}
