package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.CategoryOfRunDao;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CategoryOfRunDaoImpl extends BaseDaoImpl implements CategoryOfRunDao {
    private static final Logger LOGGER = LogManager.getLogger(CategoryOfRunDaoImpl.class);

    @Override
    public CategoryOfRun readByTitleAndGame(String title, Game game) throws PersistentException {
        String sql = "SELECT `id`, `rules` FROM `categoryOfRun`"
                + "WHERE `categoryTitle` = ? AND `gameId` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setString(1,title);
            statement.setInt(2,game.getId());
            try ( ResultSet resultSet = statement.executeQuery()) {
                CategoryOfRun category = null;
                if (resultSet.next()) {
                    category = new CategoryOfRun();
                    category.setId(resultSet.getInt("id"));
                    category.setGame(game);
                    category.setCategoryTitle(title);
                    category.setRules(resultSet.getString("rules"));
                }
                return category;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<CategoryOfRun> readByGame(Game game) throws PersistentException {
        String sql = "SELECT `id`, `categoryTitle`, `rules` FROM `categoryOfRun`"
                + "WHERE `gameId` = ? ORDER BY `categoryTitle`";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1,game.getId());
            try ( ResultSet resultSet = statement.executeQuery()) {
                List<CategoryOfRun> categories = new ArrayList<>();
                CategoryOfRun category = null;
                while (resultSet.next()) {
                    category = new CategoryOfRun();
                    category.setId(resultSet.getInt("id"));
                    category.setGame(game);
                    category.setCategoryTitle(resultSet.getString("categoryTitle"));
                    category.setRules(resultSet.getString("rules"));
                    categories.add(category);
                }
                return categories;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public int create(CategoryOfRun category) throws PersistentException {
        String sql = "INSERT INTO `categoryOfRun` (`gameId`, `categoryTitle`, `rules` )"
                + " VALUES (?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ){
            statement.setInt(1, category.getGame().getId());
            statement.setString(2, category.getCategoryTitle());
            statement.setString(3, category.getRules());
            statement.executeUpdate();
            try ( ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    LOGGER.error("There is no autoincremented index after trying to add record into table `categoryOfRun`");
                    throw new PersistentException();
                }
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public CategoryOfRun read(Integer id) throws PersistentException {
        String sql = "SELECT `gameid`, `categoryTitle`, `rules` FROM `categoryOfRun`"
                + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, id);
            try ( ResultSet resultSet = statement.executeQuery()) {
                CategoryOfRun category = null;
                if (resultSet.next()) {
                    category = new CategoryOfRun();
                    category.setId(id);
                    Game game = new Game();
                    game.setId(resultSet.getInt("gameId"));
                    category.setGame(game);
                    category.setCategoryTitle(resultSet.getString("categoryTitle"));
                    category.setRules(resultSet.getString("rules"));
                }
                return category;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void update(CategoryOfRun category) throws PersistentException {
        String sql = "UPDATE `categoryOfRun` SET `gameId` = ?,"
                + " `categoryTitle` = ?, `rules` = ?"
                + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, category.getGame().getId());
            statement.setString(2, category.getCategoryTitle());
            statement.setString(3, category.getRules());
            statement.setInt(4, category.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void delete(Integer id) throws PersistentException {
        String sql = "DELETE FROM `categoryOfRun` WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }
}

