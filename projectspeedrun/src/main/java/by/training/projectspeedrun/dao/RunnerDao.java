package by.training.projectspeedrun.dao;

import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunnerDao extends Dao<Runner> {
    Runner read(String login, String pass) throws PersistentException;

    Runner read(String login) throws PersistentException;

    List<Runner> read() throws PersistentException;

    List<Runner> readLike (String like) throws PersistentException;
}
