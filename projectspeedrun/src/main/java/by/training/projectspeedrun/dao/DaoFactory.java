package by.training.projectspeedrun.dao;

import by.training.projectspeedrun.exeptions.PersistentException;

import java.sql.Connection;

public interface DaoFactory {
    <Type extends Dao<?>> Type createDao(Class<Type> key) throws PersistentException;

    void close(Dao dao);

}
