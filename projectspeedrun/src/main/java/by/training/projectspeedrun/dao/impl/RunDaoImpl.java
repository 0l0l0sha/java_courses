package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.RunDao;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Platform;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class RunDaoImpl extends BaseDaoImpl implements RunDao {

    private static final Logger LOGGER = LogManager.getLogger(RunDaoImpl.class);

    @Override
    public List<Run> readByCategory(int categoryIdentity) throws PersistentException {
        String sql = "SELECT `id`, `runnerId`, `realTime`, `inGameTime`, `date`, `video`, `platform`," + " `isSubmited` FROM `run` WHERE `categoryId` = ? ORDER BY `realTime`";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, categoryIdentity);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Run> runs = new ArrayList<>();
                Run run = null;
                while (resultSet.next()) {
                    int offset = TimeZone.getDefault().getRawOffset();
                    run = new Run();
                    run.setId(resultSet.getInt("id"));
                    CategoryOfRun category = new CategoryOfRun();
                    category.setId(categoryIdentity);
                    run.setCategory(category);
                    Runner runner = new Runner();
                    runner.setId(resultSet.getInt("runnerId"));
                    run.setRunner(runner);
                    long tableTime;
                    if (resultSet.getTime("realTime") != null) {
                        tableTime = resultSet.getTime("realTime").getTime();
                        Time runTime = new Time(tableTime - offset);
                        run.setRealTime(runTime);
                    }
                    if (resultSet.getTime("inGameTime") != null) {
                        tableTime = resultSet.getTime("inGameTime").getTime();
                        run.setInGameTime(new Time(tableTime - offset));
                    }


                    run.setDate(resultSet.getDate("date"));
                    run.setVideo(resultSet.getString("video"));
                    Platform platform = null;
                    String platStr = resultSet.getString("platform");
                    if (!(platStr == null || platStr.isEmpty())) {
                        platform = Platform.valueOf(platStr);
                    }
                    run.setPlatform(platform);
                    run.setSubmited(resultSet.getBoolean("isSubmited"));
                    runs.add(run);
                }
                return runs;
            }
        } catch (SQLException | IllegalArgumentException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Run> readByRunner(int runnerIdentity) throws PersistentException {
        String sql = "SELECT `id`, `categoryId`, `realTime`, `inGameTime`, `date`, `video`, `platform`," + " `isSubmited` FROM `run` WHERE `runnerId` = ? ORDER BY `categoryId`";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, runnerIdentity);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Run> runs = new ArrayList<>();
                Run run = null;
                while (resultSet.next()) {
                    run = new Run();
                    run.setId(resultSet.getInt("id"));
                    CategoryOfRun category = new CategoryOfRun();
                    category.setId(resultSet.getInt("categoryId"));
                    run.setCategory(category);
                    Runner runner = new Runner();
                    runner.setId(runnerIdentity);
                    run.setRunner(runner);
                    int offset = TimeZone.getDefault().getRawOffset();
                    long tableTime;
                    if (resultSet.getTime("realTime") != null) {
                        tableTime = resultSet.getTime("realTime").getTime();
                        Time runTime = new Time(tableTime - offset);
                        run.setRealTime(runTime);
                    }
                    if (resultSet.getTime("inGameTime") != null) {
                        tableTime = resultSet.getTime("inGameTime").getTime();
                        run.setInGameTime(new Time(tableTime - offset));
                    }
                    run.setDate(resultSet.getDate("date"));
                    run.setVideo(resultSet.getString("video"));
                    Platform platform = null;
                    String platStr = resultSet.getString("platform");
                    if (!(platStr == null || platStr.isEmpty())) {
                        platform = Platform.valueOf(platStr);
                    }
                    run.setPlatform(platform);
                    run.setSubmited(resultSet.getBoolean("isSubmited"));
                    runs.add(run);
                }
                return runs;
            }
        } catch (SQLException | IllegalArgumentException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Run> readByCategoryAndRunner(int categoryIdentity, int runnerIdentity) throws PersistentException {
        String sql = "SELECT `id`, `realTime`, `inGameTime`, `date`, `video`, `platform`," + " `isSubmited` FROM `run` WHERE `categoryId` = ? AND `runnerId` = ? ORDER BY `realTime`";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, categoryIdentity);
            statement.setInt(2, runnerIdentity);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Run> runs = new ArrayList<>();
                Run run = null;
                while (resultSet.next()) {
                    run = new Run();
                    run.setId(resultSet.getInt("id"));
                    CategoryOfRun category = new CategoryOfRun();
                    category.setId(categoryIdentity);
                    run.setCategory(category);
                    Runner runner = new Runner();
                    runner.setId(runnerIdentity);
                    run.setRunner(runner);
                    int offset = TimeZone.getDefault().getRawOffset();
                    long tableTime;
                    if (resultSet.getTime("realTime") != null) {
                        tableTime = resultSet.getTime("realTime").getTime();
                        Time runTime = new Time(tableTime - offset);
                        run.setRealTime(runTime);
                    }
                    if (resultSet.getTime("inGameTime") != null) {
                        tableTime = resultSet.getTime("inGameTime").getTime();
                        run.setInGameTime(new Time(tableTime - offset));
                    }
                    run.setDate(resultSet.getDate("date"));
                    run.setVideo(resultSet.getString("video"));
                    Platform platform = null;
                    String platStr = resultSet.getString("platform");
                    if (!(platStr == null || platStr.isEmpty())) {
                        platform = Platform.valueOf(platStr);
                    }
                    run.setPlatform(platform);
                    run.setSubmited(resultSet.getBoolean("isSubmited"));
                    runs.add(run);
                }
                return runs;
            }
        } catch (SQLException | IllegalArgumentException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public int create(Run run) throws PersistentException {
        String sql = "INSERT INTO `run` (`categoryId`, `runnerId`, `realTime`, `inGameTime`, `date`, `video`, `platform`, `isSubmited`)" + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, run.getCategory().getId());
            statement.setInt(2, run.getRunner().getId());
            int offset = TimeZone.getDefault().getRawOffset();
            Time time;
            if (run.getRealTime() != null) {
                time = new Time(run.getRealTime().getTime() + offset);
            } else time = null;
            statement.setTime(3, time);
            if (run.getInGameTime() != null) {
                time = new Time(run.getInGameTime().getTime() + offset);
            } else time = null;
            statement.setTime(4, time);
            statement.setDate(5, run.getDate());
            statement.setString(6, run.getVideo());
            statement.setString(7, run.getPlatform().toString());
            statement.setBoolean(8, run.getSubmited());
            statement.executeUpdate();
            try (ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    LOGGER.error("There is no autoincremented index after trying to add record into table `run`");
                    throw new PersistentException();
                }
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public Run read(Integer id) throws PersistentException {
        String sql = "SELECT `id`,`categoryId`, `runnerId`, `realTime`, `inGameTime`, `date`, `video`, `platform`," + "`isSubmited` FROM `run` WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                Run run = null;
                if (resultSet.next()) {
                    run = new Run();
                    run.setId(id);
                    CategoryOfRun category = new CategoryOfRun();
                    category.setId(resultSet.getInt("categoryId"));
                    run.setCategory(category);
                    Runner runner = new Runner();
                    runner.setId(resultSet.getInt("runnerId"));
                    run.setRunner(runner);
                    int offset = TimeZone.getDefault().getRawOffset();
                    long tableTime;
                    if (resultSet.getTime("realTime") != null) {
                        tableTime = resultSet.getTime("realTime").getTime();
                        Time runTime = new Time(tableTime - offset);
                        run.setRealTime(runTime);
                    }
                    if (resultSet.getTime("inGameTime") != null) {
                        tableTime = resultSet.getTime("inGameTime").getTime();
                        run.setInGameTime(new Time(tableTime - offset));
                    }
                    run.setDate(resultSet.getDate("date"));
                    run.setVideo(resultSet.getString("video"));
                    Platform platform = null;
                    String platStr = resultSet.getString("platform");
                    if (!(platStr == null || platStr.isEmpty())) {
                        platform = Platform.valueOf(platStr);
                    }
                    run.setPlatform(platform);
                    run.setSubmited(resultSet.getBoolean("isSubmited"));
                }
                return run;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void update(Run run) throws PersistentException {
        String sql = "UPDATE `run` SET `categoryId` = ?, `runnerId` = ?, `realTime` = ?," + "`inGameTime` = ?, `date` = ?, `video` = ?, `platform` = ?, `isSubmited` = ? " + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, run.getCategory().getId());
            statement.setInt(2, run.getRunner().getId());
            int offset = TimeZone.getDefault().getRawOffset();
            Time time;
            if (run.getRealTime() != null) {
                 time = new Time(run.getRealTime().getTime() + offset);
            } else time = null;
            statement.setTime(3, time);
            if (run.getInGameTime() != null) {
                time = new Time(run.getInGameTime().getTime() + offset);
            } else time = null;
            statement.setTime(4, time);
            statement.setDate(5, run.getDate());
            statement.setString(6, run.getVideo());
            statement.setString(7, run.getPlatform().toString());
            statement.setBoolean(8, run.getSubmited());
            statement.setInt(9, run.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void delete(Integer id) throws PersistentException {
        String sql = "DELETE FROM `run` WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }
}
