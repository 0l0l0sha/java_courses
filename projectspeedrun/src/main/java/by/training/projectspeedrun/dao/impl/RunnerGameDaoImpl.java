package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.RunnerGameDao;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RunnerGameDaoImpl extends BaseDaoImpl implements RunnerGameDao {
    private static final Logger LOGGER = LogManager.getLogger(RunnerGameDaoImpl.class);

    @Override
    public List<Integer> readByPlayer(int runnerId) throws PersistentException {
        String sql = "SELECT `gameId` FROM `conjuctionRunnerGame` WHERE `runnerId` = ? ";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, runnerId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Integer> gameIds = new ArrayList<>();
                while (resultSet.next()) {
                    gameIds.add(resultSet.getInt("gameId"));
                }
                return gameIds;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public RunnerGame readByPlayerAndGame(int runnerId, int gameId) throws PersistentException {
        String sql = "SELECT `isModerator` FROM `conjuctionRunnerGame` WHERE (`runnerId` = ? && `gameid` = ? )";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, runnerId);
            statement.setInt(2, gameId);
            try (ResultSet resultSet = statement.executeQuery()) {
                RunnerGame runnerGame = null;
                while (resultSet.next()) {
                    runnerGame = new RunnerGame();
                    runnerGame.setGameId(gameId);
                    runnerGame.setRunnerId(runnerId);
                    runnerGame.setModerator(resultSet.getBoolean("isModerator"));
                }
                return runnerGame;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Integer> readModerators(int gameId) throws PersistentException {
        String sql = "SELECT `runnerId` FROM `conjuctionRunnerGame` WHERE (`gameId` = ? && `isModerator` != 0)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, gameId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Integer> runnerIds = new ArrayList<>();
                while (resultSet.next()) {
                    runnerIds.add(resultSet.getInt("runnerId"));
                }
                return runnerIds;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Integer> readModerate(int runnerId) throws PersistentException {
        String sql = "SELECT `gameId` FROM `conjuctionRunnerGame` WHERE (`runnerId` = ? && `isModerator` != 0)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, runnerId);
            try (ResultSet resultSet = statement.executeQuery()) {
                List<Integer> gameIds = new ArrayList<>();
                while (resultSet.next()) {
                    gameIds.add(resultSet.getInt("gameId"));
                }
                return gameIds;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public int QuantityByGame(int gameId) throws PersistentException {
        String sql = "SELECT COUNT(runnerId) AS quantity FROM  conjuctionRunnerGame WHERE gameId = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, gameId);
            try (ResultSet resultSet = statement.executeQuery()) {
                int quantity = 0;
                if (resultSet.next()) {
                    quantity = resultSet.getInt("quantity");
                }
                return quantity;
            }
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public int create(RunnerGame runnerGame) throws PersistentException {
        String sql = "INSERT INTO `conjuctionRunnerGame` (`runnerId`, `gameId`, `isModerator`)"
                + " VALUES (?, ?, ?)";
        try (PreparedStatement statement =  connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ){
            statement.setInt(1, runnerGame.getRunnerId());
            statement.setInt(2, runnerGame.getGameId());
            statement.setBoolean(3, runnerGame.getModerator());
            statement.executeUpdate();
            return 0;
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public RunnerGame read(Integer id) throws PersistentException {
        throw new PersistentException();
    }


    @Override
    public void update(RunnerGame runnerGame) throws PersistentException {
        String sql = "UPDATE `conjuctionRunnerGame` SET `isModerator` = ? WHERE (`runnerId` = ? && `gameid` = ? )";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, runnerGame.getModerator());
            statement.setInt(2, runnerGame.getRunnerId());
            statement.setInt(3, runnerGame.getGameId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new PersistentException(e);
        }
    }


    @Override
    public void delete(Integer id) throws PersistentException {
        throw new PersistentException();
    }


    public void delete(RunnerGame runnerGame) throws PersistentException {
        String sql = "DELETE FROM `conjuctionRunnerGame` WHERE (`runnerId` = ? && `gameid` = ? )";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, runnerGame.getRunnerId());
            statement.setInt(2, runnerGame.getGameId());
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

}
