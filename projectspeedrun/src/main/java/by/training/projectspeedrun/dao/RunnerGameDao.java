package by.training.projectspeedrun.dao;

import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;

import java.util.List;

public interface RunnerGameDao extends Dao<RunnerGame> {

    List<Integer> readByPlayer(int runnerId) throws PersistentException;

    RunnerGame readByPlayerAndGame(int runnerId, int gameId) throws PersistentException;

    List<Integer> readModerators(int gameId) throws PersistentException;

    List<Integer> readModerate(int runnerId) throws PersistentException;

    int QuantityByGame(int gameId) throws PersistentException;

    void delete(RunnerGame runnerGame) throws PersistentException;
}
