package by.training.projectspeedrun.dao.impl;

import by.training.projectspeedrun.dao.RunnerDao;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;



public class RunnerDaoImpl extends BaseDaoImpl implements RunnerDao {

    private static final Logger LOGGER = LogManager.getLogger(RunnerDaoImpl.class);

    @Override
    public Runner read(String login, String pass) throws PersistentException {
        String sql = "SELECT `id`, `email`, `role`, "
        + "`country`, `twitch`, `youtube`, `image` FROM `runner` WHERE `login` = ? AND `pass` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setString(1, login);
            statement.setString(2, pass);
            try ( ResultSet resultSet = statement.executeQuery()) {
                Runner runner = null;
                if (resultSet.next()) {
                    runner = new Runner();
                    runner.setId(resultSet.getInt("id"));
                    runner.setEmail(resultSet.getString("email"));
                    runner.setLogin(login);
                    runner.setPass(pass);
                    runner.setRole(Role.getByIdentity(resultSet.getInt("role")));
                    runner.setCountry(resultSet.getString("country"));
                    runner.setTwitch(resultSet.getString("twitch"));
                    runner.setYoutube(resultSet.getString("youtube"));
                    Blob blob = resultSet.getBlob("image");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                }
                return runner;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public Runner read(String login) throws PersistentException {
        String sql = "SELECT `id`, `email`, `role`, "
                + "`country`, `twitch`, `youtube`, `image` FROM `runner` WHERE `login` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setString(1, login);
            try ( ResultSet resultSet = statement.executeQuery()) {
                Runner runner = null;
                if (resultSet.next()) {
                    runner = new Runner();
                    runner.setId(resultSet.getInt("id"));
                    runner.setEmail(resultSet.getString("email"));
                    runner.setLogin(login);
                    runner.setPass(new String());
                    runner.setRole(Role.getByIdentity(resultSet.getInt("role")));
                    runner.setCountry(resultSet.getString("country"));
                    runner.setTwitch(resultSet.getString("twitch"));
                    runner.setYoutube(resultSet.getString("youtube"));
                    Blob blob = resultSet.getBlob("image");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                }
                return runner;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Runner> read() throws PersistentException {
        String sql = "SELECT `id`, `email`, `login`, `pass`, `role`, "
                + "`country`, `twitch`, `youtube`, `image` FROM `runner` "
                + "ORDER BY `login`";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            try ( ResultSet resultSet = statement.executeQuery()) {
                List<Runner> runners = new ArrayList<>();
                Runner runner = null;
                while (resultSet.next()) {
                    runner = new Runner();
                    runner.setId(resultSet.getInt("id"));
                    runner.setEmail(resultSet.getString("email"));
                    runner.setLogin(resultSet.getString("login"));
                    runner.setPass(resultSet.getString("pass"));
                    runner.setRole(Role.getByIdentity(resultSet.getInt("role")));
                    runner.setCountry(resultSet.getString("country"));
                    runner.setTwitch(resultSet.getString("twitch"));
                    runner.setYoutube(resultSet.getString("youtube"));
                    Blob blob = resultSet.getBlob("image");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                    runners.add(runner);
                }
                return runners;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public List<Runner> readLike(String like) throws PersistentException {
        String sql = "SELECT `id`, `email`, `login`, `pass`, `role`, "
                + "`country`, `twitch`, `youtube`, `image` FROM `runner` "
                + "where `login` LIKE ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setString(1,like);
            try ( ResultSet resultSet = statement.executeQuery()) {
                List<Runner> runners = new ArrayList<>();
                Runner runner;
                while (resultSet.next()) {
                    runner = new Runner();
                    runner.setId(resultSet.getInt("id"));
                    runner.setEmail(resultSet.getString("email"));
                    runner.setLogin(resultSet.getString("login"));
                    runner.setPass(resultSet.getString("pass"));
                    runner.setRole(Role.getByIdentity(resultSet.getInt("role")));
                    runner.setCountry(resultSet.getString("country"));
                    runner.setTwitch(resultSet.getString("twitch"));
                    runner.setYoutube(resultSet.getString("youtube"));
                    Blob blob = resultSet.getBlob("image");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                    runners.add(runner);
                }
                return runners;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }


    @Override
    public int create(Runner runner) throws PersistentException {
        String sql = "INSERT INTO `runner` (`email`, `login`, `pass`, `role`, `country`, `twitch`, `youtube`, `image`)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (PreparedStatement statement =  connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
        ){
            statement.setString(1, runner.getEmail());
            statement.setString(2, runner.getLogin());
            statement.setString(3, runner.getPass());
            statement.setInt(4, runner.getRole().getIdentity());
            statement.setString(5, runner.getCountry());
            statement.setString(6, runner.getTwitch());
            statement.setString(7, runner.getYoutube());
            String encoded = runner.getImage();
            byte[] img = Base64.getDecoder().decode(encoded.getBytes());
            Blob blob = connection.createBlob();
            blob.setBytes(1, img);
            statement.setBlob(8, blob);
            statement.executeUpdate();
            try ( ResultSet resultSet = statement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    LOGGER.error("There is no autoincremented index after trying to add record into table `runner`");
                    throw new PersistentException();
                }
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public Runner read(Integer id) throws PersistentException {
        String sql = "SELECT `email`, `login`, `pass`, `role`, "
                + "`country`, `twitch`, `youtube`, `image` FROM `runner` "
                + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, id);
            try ( ResultSet resultSet = statement.executeQuery()) {
                Runner runner = null;
                if (resultSet.next()) {
                    runner = new Runner();
                    runner.setId(id);
                    runner.setEmail(resultSet.getString("email"));
                    runner.setLogin(resultSet.getString("login"));
                    runner.setPass(resultSet.getString("pass"));
                    runner.setRole(Role.getByIdentity(resultSet.getInt("role")));
                    runner.setCountry(resultSet.getString("country"));
                    runner.setTwitch(resultSet.getString("twitch"));
                    runner.setYoutube(resultSet.getString("youtube"));
                    Blob blob = resultSet.getBlob("image");
                    byte[] imgData = blob.getBytes(1,(int)blob.length());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                }
                return runner;
            }
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void update(Runner runner) throws PersistentException {
        String sql = "UPDATE `runner` SET `email` = ?, `login` = ?, `pass` = ?,"
                + " `role` = ?, `country` = ?, `twitch` = ?, `youtube` = ?, `image` = ?  "
                + "WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setString(1, runner.getEmail());
            statement.setString(2, runner.getLogin());
            statement.setString(3, runner.getPass());
            statement.setInt(4, runner.getRole().getIdentity());
            statement.setString(5, runner.getCountry());
            statement.setString(6, runner.getTwitch());
            statement.setString(7, runner.getYoutube());
            String encoded = runner.getImage();
            byte[] img = Base64.getDecoder().decode(encoded.getBytes());
            Blob blob = connection.createBlob();
            blob.setBytes(1, img);
            statement.setBlob(8, blob);
            statement.setInt(9, runner.getId());
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }

    @Override
    public void delete(Integer id) throws PersistentException {
        String sql = "DELETE FROM `runner` WHERE `id` = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)
        ){
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch(SQLException e) {
            throw new PersistentException(e);
        }
    }
}
