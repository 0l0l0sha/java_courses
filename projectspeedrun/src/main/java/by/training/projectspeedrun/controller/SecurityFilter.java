package by.training.projectspeedrun.controller;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.MainAction;
import by.training.projectspeedrun.action.moderator.ModeratorAction;
import by.training.projectspeedrun.action.self.SelfAction;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.service.GameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class SecurityFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(SecurityFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            Action action = (Action) httpRequest.getAttribute("action");
            Set<Role> allowRoles = action.getAllowRoles();
            String runnerName = "unauthorized runner";
            HttpSession session = httpRequest.getSession(false);
            Runner runner = null;
            if (session != null) {
                runner = (Runner) session.getAttribute("authorizedRunner");
                action.setAuthorizedRunner(runner);
                String errorMessage = (String) session.getAttribute("SecurityFilterMessage");
                if (errorMessage != null) {
                    httpRequest.setAttribute("message", errorMessage);
                    session.removeAttribute("SecurityFilterMessage");
                }
            }
            boolean canExecute = allowRoles.isEmpty();
            if (runner != null) {
                runnerName = "\"" + runner.getLogin() + "\" runner";
                int actionType = 0;
                if (action instanceof ModeratorAction) actionType = 1;
                if (action instanceof SelfAction) actionType = 2;
                switch (actionType) {
                    case 1:
                        canExecute = false;
                        if (session.getAttribute("moderateList") != null && request.getParameter("gameId") != null) {
                            List<Integer> moderateGames = (List<Integer>) session.getAttribute("moderateList");
                            int gameId = Integer.parseInt(request.getParameter("gameId"));
                            if (moderateGames.contains(gameId)) {
                                canExecute = true;
                            }
                        }
                        break;
                    case 2:
                        canExecute = false;
                        if (request.getParameter("runnerId") != null) {
                            int runnerId = Integer.parseInt(request.getParameter("runnerId"));
                            if (runner.getId() == runnerId) {
                                canExecute = true;
                            }
                        }
                        break;
                    default:
                        canExecute = canExecute || allowRoles.contains(runner.getRole());
                }
            }
            if (canExecute) {
                chain.doFilter(request, response);
            } else {
                LOGGER.info(String.format("Trying of %s access to forbidden resource \"%s\"", runnerName, action.getName()));
                if (session != null && action.getClass() != MainAction.class) {
                    ResourceBundle bundle;
                    String bundleBase = "data";
                    if (session.getAttribute("locale") != null) {
                        Locale locale = (Locale) session.getAttribute("locale");
                        int i =0;
                        bundle = ResourceBundle.getBundle(bundleBase, locale, this.getClass().getClassLoader());
                    } else {
                        bundle = ResourceBundle.getBundle(bundleBase);
                    }
                    session.setAttribute("SecurityFilterMessage", bundle.getString("accessDeniedMessage"));
                }
                httpResponse.sendRedirect(httpRequest.getContextPath() + "/login.html");
            }
        } else {
            LOGGER.error("It is impossible to use HTTP filter");
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
