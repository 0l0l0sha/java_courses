package by.training.projectspeedrun.controller;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.GameAction;
import by.training.projectspeedrun.action.GameListAction;
import by.training.projectspeedrun.action.LocaleSetAction;
import by.training.projectspeedrun.action.LoginAction;
import by.training.projectspeedrun.action.RunnerCreateAction;
import by.training.projectspeedrun.action.RunnerListAction;
import by.training.projectspeedrun.action.SearchAction;
import by.training.projectspeedrun.action.SignInAction;
import by.training.projectspeedrun.action.admin.GameCreateAction;
import by.training.projectspeedrun.action.admin.GameDeleteAction;
import by.training.projectspeedrun.action.admin.GameEditAction;
import by.training.projectspeedrun.action.admin.GameNewAction;
import by.training.projectspeedrun.action.admin.GameSaveAction;
import by.training.projectspeedrun.action.admin.ModeratorDeleteAction;
import by.training.projectspeedrun.action.admin.ModeratorSetAction;
import by.training.projectspeedrun.action.admin.RunnerDeleteAction;
import by.training.projectspeedrun.action.authorized.FollowAction;
import by.training.projectspeedrun.action.authorized.LogoutAction;
import by.training.projectspeedrun.action.MainAction;
import by.training.projectspeedrun.action.authorized.ProfileEditAction;
import by.training.projectspeedrun.action.authorized.ProfileSaveAction;
import by.training.projectspeedrun.action.RunAction;
import by.training.projectspeedrun.action.RunnerAction;
import by.training.projectspeedrun.action.authorized.UnFollowAction;
import by.training.projectspeedrun.action.moderator.CategoryCreateAction;
import by.training.projectspeedrun.action.moderator.CategoryDeleteAction;
import by.training.projectspeedrun.action.moderator.CategoryEditAction;
import by.training.projectspeedrun.action.moderator.CategoryNewAction;
import by.training.projectspeedrun.action.moderator.CategorySaveAction;
import by.training.projectspeedrun.action.moderator.RunDeleteAction;
import by.training.projectspeedrun.action.moderator.VerifyAction;
import by.training.projectspeedrun.action.authorized.RunCreateAction;
import by.training.projectspeedrun.action.self.RunEditAction;
import by.training.projectspeedrun.action.authorized.RunNewAction;
import by.training.projectspeedrun.action.self.RunSaveAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ActionFromUriFilter implements Filter {
    private static final Logger LOGGER = LogManager.getLogger(ActionFromUriFilter.class);


    private static Map<String, Class<? extends Action>> actions = new ConcurrentHashMap<>();

    static {
        actions.put("/", MainAction.class);
        actions.put("/index", MainAction.class);
        actions.put("/login", LoginAction.class);
        actions.put("/logout", LogoutAction.class);

        actions.put("/games", GameListAction.class);
        actions.put("/game", GameAction.class);
        actions.put("/game/follow", FollowAction.class);
        actions.put("/game/unfollow", UnFollowAction.class);

        actions.put("/profile/edit", ProfileEditAction.class);
        actions.put("/profile/save", ProfileSaveAction.class);
        actions.put("/signin", SignInAction.class);

        actions.put("/runners", RunnerListAction.class);
        actions.put("/runner", RunnerAction.class);
        actions.put("/runner/delete", RunnerDeleteAction.class);
        actions.put("/runner/create", RunnerCreateAction.class);


        actions.put("/moderator/delete", ModeratorDeleteAction.class);
        actions.put("/moderator/create", ModeratorSetAction.class);

        actions.put("/run", RunAction.class);
        actions.put("/run/verify", VerifyAction.class);
        actions.put("/run/delete", RunDeleteAction.class);
        actions.put("/run/create", RunCreateAction.class);
        actions.put("/run/edit", RunEditAction.class);
        actions.put("/run/save", RunSaveAction.class);
        actions.put("/run/new", RunNewAction.class);

        actions.put("/game/create", GameCreateAction.class);
        actions.put("/game/edit", GameEditAction.class);
        actions.put("/game/delete", GameDeleteAction.class);
        actions.put("/game/save", GameSaveAction.class);
        actions.put("/game/new", GameNewAction.class);

        actions.put("/category/create", CategoryCreateAction.class);
        actions.put("/category/edit", CategoryEditAction.class);
        actions.put("/category/delete", CategoryDeleteAction.class);
        actions.put("/category/save", CategorySaveAction.class);
        actions.put("/category/new", CategoryNewAction.class);

        actions.put("/locale", LocaleSetAction.class);

        actions.put("/search", SearchAction.class);

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            String contextPath = httpRequest.getContextPath();
            String uri = httpRequest.getRequestURI();
            LOGGER.debug(String.format("Starting of processing of request for URI \"%s\"", uri));
            int beginAction = contextPath.length();
            int endAction = uri.lastIndexOf('.');
            String actionName;
            if (endAction >= 0) {
                actionName = uri.substring(beginAction, endAction);
            } else {
                actionName = uri.substring(beginAction);
            }
            Class<? extends Action> actionClass = actions.get(actionName);
            try {
                Action action = actionClass.newInstance();
                action.setName(actionName);
                httpRequest.setAttribute("action", action);
                chain.doFilter(request, response);
            } catch (InstantiationException | IllegalAccessException | NullPointerException e) {
                LOGGER.error("It is impossible to create action handler object", e);
                httpRequest.setAttribute("error", String.format("Запрошенный адрес %s не может быть обработан сервером", uri));
                httpRequest.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
            }
        } else {
            LOGGER.error("It is impossible to use HTTP filter");
            request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}