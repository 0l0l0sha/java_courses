package by.training.projectspeedrun;

import by.training.projectspeedrun.dao.DaoFactory;
import by.training.projectspeedrun.dao.GameDao;
import by.training.projectspeedrun.dao.RunnerDao;
import by.training.projectspeedrun.dao.connectionpool.ConnectionPool;
import by.training.projectspeedrun.dao.impl.DaoFactoryImpl;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.commons.io.IOUtils;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;


public class Mainer {
    public static final String DB_DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";

    public static final String DB_URL = "jdbc:mysql://localhost:3306/speedrundb?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC";

    public static final String DB_USER = "speedrunUser";

    public static final String DB_PASSWORD = "speedrunPassword";

    public static final int DB_POOL_START_SIZE = 10;
    public static final int DB_POOL_MAX_SIZE = 1000;
    public static final int DB_POOL_CHECK_CONNECTION_TIMEOUT = 0;

    public static void main(String arg[]) {
        Locale locale = new Locale("en");
        ResourceBundle bundle = ResourceBundle.getBundle("data", locale);
        String greetings = bundle.getString("greetings");
        Connection connection = null;
        try {
            ConnectionPool.getInstance().init(DB_DRIVER_CLASS, DB_URL, DB_USER, DB_PASSWORD, DB_POOL_START_SIZE, DB_POOL_MAX_SIZE, DB_POOL_CHECK_CONNECTION_TIMEOUT);
            connection = ConnectionPool.getInstance().getConnection();
        } catch (PersistentException e) {
            System.out.println(e.toString());
        }

        DaoFactory factory = new DaoFactoryImpl();
       /* URL url = Mainer.class.getResource("cover-256.jpg");
        File file = new File(url.getPath());
        FileInputStream inputStream = null;
        try {
          inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] imgData = new byte[0];
        try {
            imgData = IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String logo = new String(Base64.getEncoder().encode(imgData));*/
        String fileName = "D:\\!Ilya\\EPAM\\java_courses\\projectspeedrun\\src\\main\\resources\\files\\themes\\wc3\\cover-256.jpg";
        byte[] array = null;
        try {
            array = Files.readAllBytes(Paths.get(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Blob blob = null;
        try {
            blob = new SerialBlob(array);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        byte[] imgData = null;
        try {
            imgData = blob.getBytes(1, (int) blob.length());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String logo = new String(Base64.getEncoder().encode(imgData));

        GameDao gameDao = null;
        List<Game> gameList = null;
        try {
            gameDao = factory.createDao(GameDao.class);
            gameList = gameDao.read();
            for (Game game : gameList) {
                game.setLogo(logo);
                gameDao.update(game);
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }



       /* RunnerDao dao = null;
        List<Runner> runnerList = null;
        try {
            dao = factory.createDao(RunnerDao.class);
            runnerList = dao.read();
            for (Runner runner : runnerList) {
                runner.setImage(logo);
                dao.update(runner);
            }
        } catch (PersistentException e) {
            e.printStackTrace();
        }


        Runner runner1 = new Runner();
        runner1.setTwitch("moi");
        runner1.setYoutube("twoy");
        runner1.setCountry("Belarus");
        runner1.setEmail("sobaka@bsu.by");
        runner1.setLogin("admin");
        runner1.setPass("111");
        //  runner1.setRole(true);
        Runner runner2 = new Runner();
        runner2.setTwitch("lol");
        runner2.setYoutube("kek");
        runner2.setCountry("Belarus");
        runner2.setEmail("kot@bsu.by");
        runner2.setLogin("user");
        runner2.setPass("222");
        // runner2.setRole(false);
        try {
            runner1.setId(dao.create(runner1));
            runner2.setId(dao.create(runner2));
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        try {
            List<Runner> runners = dao.read();
            System.out.println(runners);
            Runner runner3 = dao.read(runner1.getId());
            System.out.println(runner3);
            Runner runner4 = dao.read(runner2.getLogin(), runner2.getPass());
            System.out.println(runner4);
        } catch (PersistentException e) {
            e.printStackTrace();
        }

        runner2.setCountry("Russia");
        try {
            dao.update(runner2);
            System.out.println(dao.read(runner2.getId()));
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        try {
            dao.delete(runner2.getId());
            System.out.println(dao.read());
            //          dao.delete(runner1.getId());
        } catch (PersistentException e) {
            e.printStackTrace();
        }
        factory.close(dao);
        /*try {
            RunnerService service = new ServiceFactoryImpl().getService(RunnerService.class);
            service.save(runner1);
            System.out.println(dao.read());
        } catch (PersistentException e) {
            e.printStackTrace();
        }
*/
    }
}
