package by.training.projectspeedrun.action;

import by.training.projectspeedrun.exeptions.PersistentException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class LocaleSetAction extends Action {

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        HttpSession session = request.getSession();
        if (session.getAttribute("locale") != null) {
            session.removeAttribute("locale");
        }
        Locale locale = new Locale(request.getParameter("locale"));
        session.setAttribute("locale", locale);
        return  new Forward("/games.html");
    }
}
