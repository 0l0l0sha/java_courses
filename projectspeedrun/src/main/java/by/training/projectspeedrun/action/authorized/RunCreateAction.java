package by.training.projectspeedrun.action.authorized;

import by.training.projectspeedrun.action.self.SelfAction;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Platform;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class RunCreateAction extends AuthorizedUserAction {
    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        Runner runner = getAuthorizedRunner();
        GameService gameService = factory.getService(GameService.class);
        String gameTitle = request.getParameter("gameTitle");
        Game game = gameService.findByTitle(gameTitle);
        request.setAttribute("game", game);

        EnumSet<Platform> all = EnumSet.allOf( Platform.class);
        List<Platform> list = new ArrayList<>(all);

        CategoryOfRunService categoryService = factory.getService(CategoryOfRunService.class);
        int categoryId = Integer.parseInt(request.getParameter("categoryId"));
        CategoryOfRun category = categoryService.findByIdentity(categoryId);

        request.setAttribute("platforms",list);
        request.setAttribute("runner",runner);
        request.setAttribute("game", game);
        request.setAttribute("category", category);

        return null;
    }
}
