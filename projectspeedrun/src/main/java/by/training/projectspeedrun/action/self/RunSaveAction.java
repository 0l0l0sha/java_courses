package by.training.projectspeedrun.action.self;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.admin.GameSaveAction;
import by.training.projectspeedrun.action.authorized.AuthorizedUserAction;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;


public class RunSaveAction extends SelfAction {
    private static final Logger LOGGER = LogManager.getLogger(GameSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward = new Action.Forward("/profile/edit.html");
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<Run> validator = ValidatorFactory.createValidator(Run.class);
        try {
            Run newRun = validator.validate(request);
            RunService service = factory.getService(RunService.class);
            int runId = Integer.parseInt(request.getParameter("runId"));
            Run run = service.findByIdentity(runId);
            if (run != null) {
                run.setCategory(newRun.getCategory());
                run.setRunner(newRun.getRunner());
                if (newRun.getRealTime() != null) {
                    run.setRealTime(newRun.getRealTime());
                }
                if (newRun.getInGameTime() != null) {
                    run.setInGameTime(newRun.getInGameTime());
                }
                if (newRun.getPlatform() != null) {
                    run.setPlatform(newRun.getPlatform());
                }
                run.setVideo(newRun.getVideo());
                service.save(run);
                forward.getAttributes().put("message", bundle.getString("runSaveMessage"));
                LOGGER.info(String.format("User \"%s\" changed run with id %d", authorizedRunner.getLogin(), runId));
            } else {
                forward.getAttributes().put("message", bundle.getString("runNotFoundMessage"));
                LOGGER.info(String.format("User \"%s\" tried to change run with id %d but it was not found", authorizedRunner.getLogin(), runId));
            }
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to save profile", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
