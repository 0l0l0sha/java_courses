package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class CategoryDeleteAction extends ModeratorAction {
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {

        ResourceBundle bundle = getResourceBundle(request);
        Forward forward;
        try {
            int categoryId = Integer.parseInt(request.getParameter("categoryId"));
            CategoryOfRunService categoryService = factory.getService(CategoryOfRunService.class);
            CategoryOfRun categoryOfRun = categoryService.findByIdentity(categoryId);
            GameService gameService = factory.getService(GameService.class);
            Game game = gameService.findByIdentity(categoryOfRun.getGame().getId());
            categoryService.delete(categoryOfRun.getId());
            String newUrl = "/game.html?title=" + game.getGameTitle();
            forward = new Action.Forward(newUrl);
            forward.getAttributes().put("identity", categoryOfRun.getId());
            forward.getAttributes().put("message", bundle.getString("categoryDeleteMessage"));
            LOGGER.info(String.format("User \"%s\" deleted category with id %d", getAuthorizedRunner().getLogin(), categoryOfRun.getId()));
        } catch (Exception e) {
            forward = new Action.Forward("/games.html");
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to delete category", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
