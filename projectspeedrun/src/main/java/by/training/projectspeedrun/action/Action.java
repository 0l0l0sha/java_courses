package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;


public abstract class Action {
    private Set<Role> allowRoles = new HashSet<>();
    private Runner authorizedRunner = null;
    private String name;

    public ResourceBundle getResourceBundle(HttpServletRequest request) {

        ResourceBundle bundle;
        HttpSession session = request.getSession();
        String bundleBase = "data";
        if (session.getAttribute("locale") != null) {
            Locale locale = (Locale) session.getAttribute("locale");
            int i =0;
            bundle = ResourceBundle.getBundle(bundleBase, locale, this.getClass().getClassLoader());
        } else {
            bundle = ResourceBundle.getBundle(bundleBase);
        }
        return  bundle;
    }

    protected ServiceFactory factory;

    //   private static List<MenuItem> menu = new ArrayList<>();
    public Set<Role> getAllowRoles() {
        return allowRoles;
    }


    public Runner getAuthorizedRunner() {
        return authorizedRunner;
    }

    public void setAuthorizedRunner(Runner authorizedRunner) {
        this.authorizedRunner = authorizedRunner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFactory(ServiceFactory factory) {
        this.factory = factory;
    }

    abstract public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException;

    public static class Forward {
        private String forward;
        private boolean redirect;
        private Map<String, Object> attributes = new HashMap<>();

        public Forward(String forward, boolean redirect) {
            this.forward = forward;
            this.redirect = redirect;
        }

        public Forward(String forward) {
            this(forward, true);
        }

        public String getForward() {
            return forward;
        }

        public void setForward(String forward) {
            this.forward = forward;
        }

        public boolean isRedirect() {
            return redirect;
        }

        public void setRedirect(boolean redirect) {
            this.redirect = redirect;
        }

        public Map<String, Object> getAttributes() {
            return attributes;
        }
    }
}
