package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

public class GameNewAction extends AdministratorAction {

    private static final Logger LOGGER = LogManager.getLogger(GameSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward;
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<Game> validator = ValidatorFactory.createValidator(Game.class);
        try {
            Game game = validator.validate(request);
            GameService service = factory.getService(GameService.class);
            service.save(game);
            forward = new Forward("/game.html?title="+game.getGameTitle());
            forward.getAttributes().put("message", bundle.getString("gameNewMessage"));
            LOGGER.info(String.format("User \"%s\" created game with id %d", authorizedRunner.getLogin(), game.getId()));
        } catch (IncorrectFormDataException e) {
            forward = new Forward("/game/create.html");
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to create game", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
