package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;
import by.training.projectspeedrun.service.RunnerGameService;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class RunAction extends Action {
    private static final Logger LOGGER = LogManager.getLogger(RunAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        RunService runService = factory.getService(RunService.class);
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            Run run = runService.findByIdentity(id);
            CategoryOfRunService categoryOfRunService = factory.getService(CategoryOfRunService.class);
            CategoryOfRun category = categoryOfRunService.findByIdentity(run.getCategory().getId());
            run.setCategory(category);

            GameService gameService = factory.getService(GameService.class);
            Game game = gameService.findByIdentity(run.getCategory().getGame().getId());

            run.getCategory().setGame(game);

            RunnerService runnerService = factory.getService(RunnerService.class);
            Runner runner = runnerService.findByIdentity(run.getRunner().getId());
            run.setRunner(runner);

            boolean isFollowed = false;
            boolean isAuthorized = false;
            boolean isAdministrator = false;
            if (getAuthorizedRunner() != null) {
                isAdministrator = getAuthorizedRunner().getRole() == Role.ADMINISTRATOR;
                isAuthorized = true;
                RunnerGameService runnerGameService = factory.getService(RunnerGameService.class);
                isFollowed = runnerGameService.findbyPlayerAndGame(getAuthorizedRunner().getId(), game.getId()) == null;
                HttpSession session = request.getSession();
                List<Integer> moderateList = (List<Integer>) session.getAttribute("moderateList");
                request.setAttribute("moderator", moderateList.contains(game.getId()));
            }
            request.setAttribute("isAuthorized", isAuthorized);
            request.setAttribute("isFollowed", isFollowed);
            request.setAttribute("isAdministrator", isAdministrator);

            List<Runner> moderators = runnerService.findModerators(game.getId());
            request.setAttribute("moderators", moderators);

            request.setAttribute("run", run);
            request.setAttribute("game", game);
            request.setAttribute("contextPath", request.getContextPath());
        } catch (NumberFormatException e) {
            if (!request.getQueryString().isEmpty()) {
                LOGGER.warn("Data wasn't found when user tried to acces:" + request.getContextPath(), e);
            } else {
                LOGGER.warn("Incorrect data was found when user tried to acces. Path:" + request.getContextPath() + ", Parameters:" + request.getQueryString(), e);
            }
        }
        return null;
    }
}
