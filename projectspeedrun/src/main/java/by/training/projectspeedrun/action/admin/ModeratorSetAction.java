package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunnerGameService;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class ModeratorSetAction extends AdministratorAction {

    private static final Logger LOGGER = LogManager.getLogger(ModeratorSetAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Action.Forward forward = new Action.Forward("/games.html");
        try {
            String login = request.getParameter("runnerLogin");
            RunnerService service = factory.getService(RunnerService.class);
            Runner runner = service.findByLogin(login);
            if (runner != null) {
                int runnerId = runner.getId();
                int gameId = Integer.parseInt(request.getParameter("gameId"));
                RunnerGameService runnerGameService = factory.getService(RunnerGameService.class);
                RunnerGame runnerGame = runnerGameService.findbyPlayerAndGame(runnerId, gameId);
                if (runnerGame == null) {
                    runnerGame = new RunnerGame();
                    runnerGame.setGameId(gameId);
                    runnerGame.setRunnerId(runnerId);
                }
                runnerGame.setModerator(true);
                runnerGameService.save(runnerGame);
                forward.getAttributes().put("identity", runnerId);
                forward.getAttributes().put("gameId", gameId);
                forward.getAttributes().put("message", bundle.getString("moderatorSetMessage"));
                LOGGER.info(String.format("User \"%s\" gave moderator privilege to runner with id %d in game with id %d", getAuthorizedRunner().getLogin(), runnerId, gameId));
            }else {
                forward.getAttributes().put("message", bundle.getString("runnerNotFoundMessage"));
                LOGGER.info(String.format("User \"%s\" tried to give moderator privilege to runner \"%s\" but he was not found", getAuthorizedRunner().getLogin(), login));
            }
        } catch (Exception e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to give moderator privilege to runner", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
