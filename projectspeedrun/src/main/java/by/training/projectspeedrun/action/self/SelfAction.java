package by.training.projectspeedrun.action.self;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Role;

import java.util.Arrays;

public abstract class SelfAction extends Action {

    public SelfAction() {
        getAllowRoles().addAll(Arrays.asList(Role.values()));
    }
}
