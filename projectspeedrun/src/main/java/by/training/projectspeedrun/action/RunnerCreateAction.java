package by.training.projectspeedrun.action;

import by.training.projectspeedrun.action.authorized.ProfileSaveAction;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunnerService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.ResourceBundle;

public class RunnerCreateAction extends Action {
    private static final Logger LOGGER = LogManager.getLogger(ProfileSaveAction.class);

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Action.Forward forward = new Action.Forward("/profile/edit.html");
        String newPassword = request.getParameter("new-password");
        Validator<Runner> validator = ValidatorFactory.createValidator(Runner.class);
        try {
            Runner runner = validator.validate(request);
            RunnerService service = factory.getService(RunnerService.class);
            runner.setPass(newPassword);
            runner.setRole(Role.RUNNER);
            service.save(runner);
            forward.getAttributes().put("message", bundle.getString("registrateMessage"));
            LOGGER.info(String.format("User \"%s\" registered", runner.getLogin()));
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user tried to register", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
