package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.authorized.ProfileSaveAction;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunnerService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class CategorySaveAction extends ModeratorAction {

    private static final Logger LOGGER = LogManager.getLogger(ProfileSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);

        GameService gameService = factory.getService(GameService.class);
        int gameId = Integer.parseInt(request.getParameter("gameId"));
        Game game = gameService.findByIdentity(gameId);
        Forward forward = new Action.Forward("game.html?title="+game.getGameTitle());
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<CategoryOfRun> validator = ValidatorFactory.createValidator(CategoryOfRun.class);
        try {
            CategoryOfRun newCategoryOfRun = validator.validate(request);
            CategoryOfRunService service = factory.getService(CategoryOfRunService.class);
            int categoryId = Integer.parseInt(request.getParameter("categoryId"));
            CategoryOfRun categoryOfRun = service.findByIdentity(categoryId);
            if (categoryOfRun != null) {
                categoryOfRun.setId(categoryId);
                categoryOfRun.setGame(newCategoryOfRun.getGame());
                categoryOfRun.setCategoryTitle(newCategoryOfRun.getCategoryTitle());
                if (newCategoryOfRun.getRules() != null) {
                    categoryOfRun.setRules(newCategoryOfRun.getRules());
                }
                gameId = categoryOfRun.getGame().getId();
                categoryOfRun.setGame(gameService.findByIdentity(gameId));
                forward = new Forward("/game.html?title=" + categoryOfRun.getGame().getGameTitle() + "&category=" + categoryOfRun.getCategoryTitle());
                service.save(categoryOfRun);
                forward.getAttributes().put("message", bundle.getString("categorySaveMessage"));
                LOGGER.info(String.format("User \"%s\" changed category with id %d", authorizedRunner.getLogin(), categoryId));
            } else {
                forward.getAttributes().put("message", bundle.getString("categoryNotFoundMessage"));
                LOGGER.info(String.format("User \"%s\" tried to change category with id %d but it was not found", authorizedRunner.getLogin(), categoryId));
            }
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to change category", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
