package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.exeptions.PersistentException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public abstract class ModeratorAction extends Action {

    public ModeratorAction() {
        getAllowRoles().addAll(Arrays.asList(Role.values()));
    }
}
