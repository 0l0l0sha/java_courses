package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Role;

abstract public class AdministratorAction extends Action {
    public AdministratorAction() {
        getAllowRoles().add(Role.ADMINISTRATOR);
    }
}
