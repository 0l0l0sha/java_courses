package by.training.projectspeedrun.action.authorized;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutAction extends AuthorizedUserAction {
    private static final Logger LOGGER = LogManager.getLogger(LogoutAction.class);


    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {

        Runner runner = getAuthorizedRunner();
        LOGGER.info(String.format("runner \"%s\" is logged out", runner.getLogin()));
        request.getSession(false).invalidate();
        return new Forward("/");
    }
}
