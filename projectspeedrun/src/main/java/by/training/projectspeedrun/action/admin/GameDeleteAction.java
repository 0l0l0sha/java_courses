package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.moderator.VerifyAction;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class GameDeleteAction extends AdministratorAction {
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {

        ResourceBundle bundle  = getResourceBundle(request);
        Action.Forward forward = new Action.Forward("/games.html");
        try {
            int gameId = Integer.parseInt(request.getParameter("gameId"));
            GameService gameService = factory.getService(GameService.class);
            Game game = gameService.findByIdentity(gameId);
            gameService.delete(game.getId());
            forward.getAttributes().put("identity", game.getId());
            forward.getAttributes().put("message", bundle.getString("gameDeleteMessage"));
            LOGGER.info(String.format("User \"%s\" deleted game with id %d", getAuthorizedRunner().getLogin(), game.getId()));
        } catch (Exception e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to delete runner", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
