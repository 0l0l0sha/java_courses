package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.authorized.ProfileSaveAction;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;


public class CategoryNewAction extends ModeratorAction {
    private static final Logger LOGGER = LogManager.getLogger(ProfileSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward = new Action.Forward("/games.html");
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<CategoryOfRun> validator = ValidatorFactory.createValidator(CategoryOfRun.class);
        try {
            CategoryOfRun categoryOfRun = validator.validate(request);
            CategoryOfRunService service = factory.getService(CategoryOfRunService.class);
            service.save(categoryOfRun);
            GameService gameService = factory.getService(GameService.class);
            int gameId = categoryOfRun.getGame().getId();
            categoryOfRun.setGame(gameService.findByIdentity(gameId));
            forward = new Forward("/game.html?title=" + categoryOfRun.getGame().getGameTitle() + "&category=" + categoryOfRun.getCategoryTitle());
            forward.getAttributes().put("message", bundle.getString("categoryNewMessage"));
            LOGGER.info(String.format("User \"%s\" created category with id %d", authorizedRunner.getLogin(), categoryOfRun.getId()));
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to create category", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
