package by.training.projectspeedrun.action.authorized;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunnerService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ResourceBundle;

public class ProfileSaveAction extends AuthorizedUserAction {
    private static final Logger LOGGER = LogManager.getLogger(ProfileSaveAction.class);

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {

        ResourceBundle bundle = getResourceBundle(request);
        Forward forward = new Forward("/profile/edit.html");
        String oldPassword = request.getParameter("old-password");
        String newPassword = request.getParameter("new-password");
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<Runner> validator = ValidatorFactory.createValidator(Runner.class);
        try {
            Runner newRunner = validator.validate(request);
            if (oldPassword != null && newPassword != null) {
                RunnerService service = factory.getService(RunnerService.class);
                Runner runner = service.findByLoginAndPassword(authorizedRunner.getLogin(), oldPassword);
                if (runner != null) {
                    runner.setId(authorizedRunner.getId());
                    runner.setPass(newPassword);
                    if (newRunner.getImage() != null) {
                        runner.setImage(newRunner.getImage());
                    }
                    if (newRunner.getYoutube() != null) {
                        runner.setYoutube(newRunner.getYoutube());
                    }
                    if (newRunner.getTwitch() != null) {
                        runner.setTwitch(newRunner.getTwitch());
                    }
                    if (newRunner.getCountry() != null) {
                        runner.setCountry(newRunner.getCountry());
                    }
                    runner.setLogin(newRunner.getLogin());
                    runner.setEmail(newRunner.getEmail());
                    service.save(runner);
                    forward.getAttributes().put("message", bundle.getString("profileSaveMessage"));
                    HttpSession session = request.getSession();
                    session.removeAttribute("authorizedRunner");
                    session.setAttribute("authorizedRunner",runner);
                    LOGGER.info(String.format("User \"%s\" changed password", authorizedRunner.getLogin()));
                } else {
                    forward.getAttributes().put("message", bundle.getString("passwordNeopoznan"));
                    LOGGER.info(String.format("User \"%s\" tried to change password and specified the incorrect previous password", authorizedRunner.getLogin()));
                }
            } else {
                forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
                LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to change password", authorizedRunner.getLogin()));
            }
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage") );
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to change profile", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }

}
