package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunnerGameService;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

public class LoginAction extends Action {

    private static final Logger LOGGER = LogManager.getLogger(LoginAction.class);
    private static Map<Role, List<MenuItem>> menu = new ConcurrentHashMap<>();

    static {
        menu.put(Role.RUNNER, new ArrayList<>(Arrays.asList(
                new MenuItem("/games.html", "games"),
                new MenuItem("/games.html?type=1", "followGames"),
                new MenuItem("/runners.html", "runners"),
                new MenuItem("/games.html?type=2", "moderateGames"))));
        menu.put(Role.ADMINISTRATOR, new ArrayList<>(Arrays.asList(
                new MenuItem("/games.html", "games"),
                new MenuItem("/games.html?type=1", "followGames"),
                new MenuItem("/games.html?type=2", "moderateGames"),
                new MenuItem("/runners.html", "runners"),
                new MenuItem("/game/create.html", "createGame"))));
    }


    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        String login = request.getParameter("login");
        String pass = request.getParameter("password");
        if (login != null && pass != null) {
            RunnerService runnerService = factory.getService(RunnerService.class);

            Runner runner = runnerService.findByLoginAndPassword(login, pass);
            if (runner != null) {
                HttpSession session = request.getSession();
                session.setAttribute("authorizedRunner", runner);
                GameService gameService = factory.getService(GameService.class);
                List<Game> gameList = gameService.findModerate(runner.getId());
                List<Integer> gameIdList = new ArrayList<>();
                if (!gameList.isEmpty()) {
                    for (Game game : gameList) {
                        gameIdList.add(game.getId());
                    }
                }
                session.setAttribute("moderateList", gameIdList);
                session.removeAttribute("menu");
                session.setAttribute("menu", menu.get(runner.getRole()));

                LOGGER.info(String.format("runner \"%s\" is logged in from %s (%s:%s)", login, request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));
                return new Forward("/games.html");

            } else {
                //request.setAttribute("message", );
                String message = bundle.getString("loginOrPassNotFoundMessage");
                request.setAttribute("message",message);
                LOGGER.info(String.format("runner \"%s\" unsuccessfully tried to log in from %s (%s:%s)", login, request.getRemoteAddr(), request.getRemoteHost(), request.getRemotePort()));
            }
        }
        return null;
    }
}
