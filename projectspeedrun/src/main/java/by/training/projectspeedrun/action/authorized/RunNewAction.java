package by.training.projectspeedrun.action.authorized;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.admin.GameSaveAction;
import by.training.projectspeedrun.action.self.SelfAction;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class RunNewAction extends AuthorizedUserAction {
    private static final Logger LOGGER = LogManager.getLogger(GameSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward = new Forward("/games.html");
        Runner authorizedRunner = getAuthorizedRunner();
        Validator<Run> validator = ValidatorFactory.createValidator(Run.class);
        try {
            Run run = validator.validate(request);
            run.setRunner(getAuthorizedRunner());
            run.setSubmited(false);
            RunService service = factory.getService(RunService.class);
            service.save(run);
            forward.getAttributes().put("message", bundle.getString("runNewMessage"));
            LOGGER.info(String.format("User \"%s\" created run with id %d", authorizedRunner.getLogin(), run.getId()));
            return  new Forward("/run.html?id="+run.getId());
        } catch (IncorrectFormDataException e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to save profile", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
