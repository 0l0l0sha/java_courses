package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.ResourceBundle;


public class VerifyAction extends ModeratorAction {
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        int runId = Integer.parseInt(request.getParameter("runId"));
        Forward forward = new Forward("/run.html?id=" + runId);
        try {
            RunService runService = factory.getService(RunService.class);
            Run run = runService.findByIdentity(runId);
            run.setSubmited(true);
            java.util.Date date = new Date();
            java.sql.Date verifyDate = new java.sql.Date(date.getTime());
            run.setDate(verifyDate);
            runService.save(run);
            forward.getAttributes().put("identity", run.getId());
            forward.getAttributes().put("message", bundle.getString("verifyMessage"));
            LOGGER.info(String.format("User \"%s\" verified run with id %d", getAuthorizedRunner().getLogin(), run.getId()));
        } catch(Exception e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to verify run", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
