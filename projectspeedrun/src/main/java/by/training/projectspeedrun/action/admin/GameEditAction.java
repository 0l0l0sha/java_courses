package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;


public class GameEditAction extends AdministratorAction {
    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        GameService gameService = factory.getService(GameService.class);
        Game game = null;
        int gameId = -1;
        if (request.getParameter("gameId") != null) {
            gameId = Integer.parseInt(request.getParameter("gameId"));
        } else {
            HttpSession session = request.getSession();
            Map<String, Object> redirectedData = (Map<String, Object>) session.getAttribute("redirectedData");
            gameId = (int) redirectedData.get("gameId");
        }
        game = gameService.findByIdentity(gameId);
        request.setAttribute("game", game);
        return null;
    }
}
