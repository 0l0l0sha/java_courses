package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunnerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RunnerListAction extends Action {

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        RunnerService runnerService = factory.getService(RunnerService.class);
        List<Runner> runners;
        runners = runnerService.findAll();
        request.setAttribute("runners", runners);
        request.setAttribute("uri", request.getContextPath() + "/runner.html");
        return null;
    }

}
