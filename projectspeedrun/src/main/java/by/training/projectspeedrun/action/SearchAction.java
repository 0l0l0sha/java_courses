package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunnerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SearchAction extends Action {
    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        if (request.getParameter("searchInfo") != null) {
            RunnerService runnerService = factory.getService(RunnerService.class);
            List<Runner> runners;
            String like = request.getParameter("searchInfo");
            runners = runnerService.findLike(like);
            request.setAttribute("runners", runners);
            request.setAttribute("ruri", request.getContextPath() + "/runner.html");

            GameService gameService = factory.getService(GameService.class);
            List<Game> games;
            games = gameService.findLike(like);
            request.setAttribute("games", games);
            request.setAttribute("guri", request.getContextPath() + "/game.html");
        }
        return null;
    }
}
