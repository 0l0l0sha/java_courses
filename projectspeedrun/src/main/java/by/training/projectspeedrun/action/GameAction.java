package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;
import by.training.projectspeedrun.service.RunnerGameService;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class GameAction extends Action {
    private static final Logger LOGGER = LogManager.getLogger(GameAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        GameService gameService = factory.getService(GameService.class);
        try {
            String gameTitle = request.getParameter("title");
            Game game = gameService.findByTitle(gameTitle);

            RunnerGameService runnerGameService = factory.getService(RunnerGameService.class);
            int followersQuantity = runnerGameService.QuantityByGame(game.getId());
            game.setFollowersQuantity(followersQuantity);
            request.setAttribute("game", game);
            request.setAttribute("contextPath", request.getContextPath());

            RunnerService runnerService = factory.getService(RunnerService.class);
            boolean isFollowed = false;
            boolean isModerator = false;
            boolean isAuthorized = false;
            boolean isAdmin = false;
            if (getAuthorizedRunner() != null) {
                isAuthorized = true;
                isFollowed = runnerGameService.findbyPlayerAndGame(getAuthorizedRunner().getId(), game.getId()) == null;
                HttpSession session = request.getSession();
                List<Integer> moderateList = (List<Integer>) session.getAttribute("moderateList");
                isModerator = moderateList.contains(game.getId());
                isAdmin = getAuthorizedRunner().getRole() == Role.ADMINISTRATOR;
            }
            request.setAttribute("isAuthorized", isAuthorized);
            request.setAttribute("isFollowed", isFollowed);
            request.setAttribute("isModerator", isModerator);
            request.setAttribute("isAdministrator", isAdmin);

            List<Runner> moderators = runnerService.findModerators(game.getId());
            request.setAttribute("moderators", moderators);

            CategoryOfRunService categoryOfRunService = factory.getService(CategoryOfRunService.class);
            List<CategoryOfRun> categories = categoryOfRunService.findAllInGame(game);
            CategoryOfRun category;
            String categoryTitle = request.getParameter("category");
            if (categoryTitle == null || categoryTitle.isEmpty()) {
                if (!categories.isEmpty()) category = categories.get(0);
                else return null;
            } else {
                category = categoryOfRunService.findByTitleAndGame(categoryTitle, game);
            }
            request.setAttribute("category", category);
            request.setAttribute("categories", categories);

            RunService runService = factory.getService(RunService.class);
            List<Run> runs;
            runs = runService.findByCategory(category.getId());

            List<Run> runsToRemove = new ArrayList<>();
            for (Run run : runs) {
                if (!isModerator && !run.getSubmited()) {
                    runsToRemove.add(run);
                    continue;
                }
                int runnerId = run.getRunner().getId();
                run.setRunner(runnerService.findByIdentity(runnerId));
            }
            runs.removeAll(runsToRemove);
            request.setAttribute("runs", runs);

            Cookie cookie = CookieHandler.getCookie(request, "gameId");
            if (cookie != null) {
                cookie.setValue(String.valueOf(game.getId()));
                response.addCookie(cookie);
            } else {
                Cookie gameIdCookie = new Cookie("gameId", String.valueOf(game.getId()));
                response.addCookie(gameIdCookie);
            }

        } catch (NumberFormatException e) {
            if (!request.getQueryString().isEmpty()) {
                LOGGER.warn("Data wasn't found when user tried to acces:" + request.getContextPath(), e);
            } else {
                LOGGER.warn("Incorrect data was found when user tried to acces. Path:" + request.getContextPath() + ", Parameters:" + request.getQueryString(), e);
            }
        }
        return null;
    }
}
