package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CategoryEditAction extends  ModeratorAction{
    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        GameService gameService = factory.getService(GameService.class);
        Integer gameId = Integer.valueOf(request.getParameter("gameId"));
        Game game = gameService.findByIdentity(gameId);
        request.setAttribute("game", game);
        CategoryOfRunService categoryService = factory.getService(CategoryOfRunService.class);
        int categoryId = Integer.parseInt(request.getParameter("categoryId"));
        CategoryOfRun category = categoryService.findByIdentity(categoryId);
        request.setAttribute("game", game);
        request.setAttribute("category", category);
        return null;
    }
}
