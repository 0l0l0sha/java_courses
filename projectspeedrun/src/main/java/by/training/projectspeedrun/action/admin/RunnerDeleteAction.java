package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.moderator.VerifyAction;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.ResourceBundle;

public class RunnerDeleteAction extends AdministratorAction {
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Action.Forward forward = new Action.Forward("/games.html");
        try {
            int runnerId = Integer.parseInt(request.getParameter("runnerId"));
            RunnerService runnerService = factory.getService(RunnerService.class);
            Runner runner = runnerService.findByIdentity(runnerId);
            runnerService.delete(runner.getId());
            forward.getAttributes().put("identity", runner.getId());
            forward.getAttributes().put("message", bundle.getString("runnerDeleteMessage"));
            LOGGER.info(String.format("User \"%s\" deleted runner with id %d", getAuthorizedRunner().getLogin(), runner.getId()));
        } catch(Exception e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to delete runner", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
