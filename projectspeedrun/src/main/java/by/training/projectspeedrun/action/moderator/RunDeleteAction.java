package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.CookieHandler;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class RunDeleteAction extends ModeratorAction {
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward;
        try {
            int runId = Integer.parseInt(request.getParameter("runId"));
            RunService runService = factory.getService(RunService.class);
            Run run = runService.findByIdentity(runId);
            if (run == null) {
                forward = new Forward("/games.html");
                forward.getAttributes().put("message", bundle.getString("runNotFoundMessage"));
                LOGGER.info(String.format("User \"%s\" tried to delete run with id %d but it was not found", getAuthorizedRunner().getLogin(), runId));
                return forward;
            }

            int gameId = -1;
            Cookie cookie = CookieHandler.getCookie(request, "gameId");
            if (cookie != null) {
                String value = cookie.getValue();
                if (value != null) {
                    gameId = Integer.parseInt(value);
                }
            }
            CategoryOfRunService categoryOfRunService = factory.getService(CategoryOfRunService.class);
            CategoryOfRun category = categoryOfRunService.findByIdentity(run.getCategory().getId());
            GameService gameService = factory.getService(GameService.class);
            Game game = null;
            if (gameId != -1) {
                game = gameService.findByIdentity(gameId);
            } else {
                run.setCategory(category);
                game = gameService.findByIdentity(run.getCategory().getGame().getId());
            }
            runService.delete(run.getId());
            String newUrl = "/game.html?title=" + game.getGameTitle() + "&category=" + category.getCategoryTitle();
            forward = new Forward(newUrl);
            forward.getAttributes().put("identity", run.getId());
            forward.getAttributes().put("message", bundle.getString("runDeleteMessage"));
            LOGGER.info(String.format("User \"%s\" deleted run with id %d", getAuthorizedRunner().getLogin(), run.getId()));
        } catch (Exception e) {
            forward = new Forward("/games.html");
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to delete run", getAuthorizedRunner().getLogin()), e);
        }
        return forward;
    }
}
