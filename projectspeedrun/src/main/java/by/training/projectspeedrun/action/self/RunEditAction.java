package by.training.projectspeedrun.action.self;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.authorized.AuthorizedUserAction;
import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Platform;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;


public class RunEditAction extends SelfAction {
    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        Runner runner = getAuthorizedRunner();
        GameService gameService = factory.getService(GameService.class);
        String gameTitle = request.getParameter("gameTitle");
        Game game = gameService.findByTitle(gameTitle);
        request.setAttribute("game", game);

        EnumSet<Platform> all = EnumSet.allOf( Platform.class);
        List<Platform> list = new ArrayList<>(all);


        RunService runService = factory.getService(RunService.class);
        int runId = Integer.parseInt(request.getParameter("runId"));
        Run run = runService.findByIdentity(runId);

        CategoryOfRunService categoryService = factory.getService(CategoryOfRunService.class);
        int categoryId = Integer.parseInt(request.getParameter("categoryId"));
        CategoryOfRun category = categoryService.findByIdentity(categoryId);


        request.setAttribute("run",run);
        request.setAttribute("platforms",list);
        request.setAttribute("runner",runner);
        request.setAttribute("game", game);
        request.setAttribute("category", category);

        return null;
    }
}
