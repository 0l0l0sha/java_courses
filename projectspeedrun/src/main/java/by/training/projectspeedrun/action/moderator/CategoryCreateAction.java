package by.training.projectspeedrun.action.moderator;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CategoryCreateAction extends ModeratorAction {
    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        GameService gameService = factory.getService(GameService.class);
        Integer gameId = Integer.valueOf(request.getParameter("gameId"));
        Game game = gameService.findByIdentity(gameId);
        request.setAttribute("game", game);
        return null;
    }
}
