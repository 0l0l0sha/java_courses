package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunnerGameService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

public class GameListAction extends Action {
    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        GameService gameService = factory.getService(GameService.class);
        RunnerGameService runnerGameService = factory.getService(RunnerGameService.class);
        List<Game> games = new ArrayList<>();
        int listType = 0;
        if (request.getParameter("type") != null) {
            listType = Integer.parseInt(request.getParameter("type"));
        }
        Runner runner = getAuthorizedRunner();
        switch (listType) {
            case 1:
                if (runner != null) {
                    games = gameService.findByPlayer(runner.getId());
                }
                break;
            case 2:
                if (runner != null) {
                    games = gameService.findModerate(runner.getId());
                }
                break;
            default:
                games = gameService.findAll();
                break;
        }
        request.setAttribute("games", games);
        request.setAttribute("uri", request.getContextPath() + "/game.html");
        return null;
    }
}
