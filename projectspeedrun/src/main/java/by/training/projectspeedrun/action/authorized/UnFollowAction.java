package by.training.projectspeedrun.action.authorized;

import by.training.projectspeedrun.action.Action;
import by.training.projectspeedrun.action.moderator.VerifyAction;
import by.training.projectspeedrun.entity.RunnerGame;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunnerGameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class UnFollowAction extends AuthorizedUserAction{
    private static final Logger LOGGER = LogManager.getLogger(VerifyAction.class);

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {

        ResourceBundle bundle = getResourceBundle(request);
        int gameId = Integer.parseInt(request.getParameter("gameId"));
        GameService gameService = factory.getService(GameService.class);
        Forward forward = new Forward("/game.html?title="+gameService.findByIdentity(gameId).getGameTitle());
        try {
            int runnerId = getAuthorizedRunner().getId();
            RunnerGameService runnerGameService = factory.getService(RunnerGameService.class);
            RunnerGame runnerGame = runnerGameService.findbyPlayerAndGame(runnerId, gameId);
            runnerGameService.delete(runnerGame);
            forward.getAttributes().put("identity", runnerId);
            forward.getAttributes().put("gameId", gameId);
            forward.getAttributes().put("message", bundle.getString("unFollowMessage"));
            LOGGER.info(String.format("User \"%s\" unfollowed game with id %d", getAuthorizedRunner().getLogin(), gameId));

        } catch (Exception e) {
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage") );
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to unfollow game with id %d", getAuthorizedRunner().getLogin(), gameId), e);
        }
        return forward;
    }
}
