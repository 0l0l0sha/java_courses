package by.training.projectspeedrun.action;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.CategoryOfRunService;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.service.RunService;
import by.training.projectspeedrun.service.RunnerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class RunnerAction extends Action{
    private static final Logger LOGGER = LogManager.getLogger(RunnerAction.class);


    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        RunnerService runnerService = factory.getService(RunnerService.class);
        try {
            String login = request.getParameter("login");
            Runner runner = runnerService.findByLogin(login);
            request.setAttribute("runner", runner);
            request.setAttribute("contextPath", request.getContextPath());
            RunService runService = factory.getService(RunService.class);
            List<Run> runs = null;
            runs = runService.findByRunner(runner.getId());
            CategoryOfRunService categoryOfRunService = factory.getService(CategoryOfRunService.class);
            GameService gameService = factory.getService(GameService.class);
            for (Run run : runs) {
                run.setRunner(runner);
                int categoryId = run.getCategory().getId();
                run.setCategory(categoryOfRunService.findByIdentity(categoryId));
                int gameId = run.getCategory().getGame().getId();
                run.getCategory().setGame(gameService.findByIdentity(gameId));
            }
            boolean isAdmin = false;
            if(getAuthorizedRunner()!= null)
            {
                if (getAuthorizedRunner().getRole() == Role.ADMINISTRATOR){
                    isAdmin = true;
                }
            }
            request.setAttribute("isAdministrator", isAdmin);
            request.setAttribute("runs", runs);
        } catch (NumberFormatException e) {
            if (!request.getQueryString().isEmpty()) {
                LOGGER.warn("Data wasn't found when user tried to acces:" + request.getContextPath(), e);
            } else {
                LOGGER.warn("Incorrect data was found when user tried to acces. Path:" + request.getContextPath() + ", Parameters:" + request.getQueryString(), e);
            }
        }
        return null;
    }
}
