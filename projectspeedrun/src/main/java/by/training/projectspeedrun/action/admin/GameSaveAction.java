package by.training.projectspeedrun.action.admin;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import by.training.projectspeedrun.exeptions.PersistentException;
import by.training.projectspeedrun.service.GameService;
import by.training.projectspeedrun.validator.Validator;
import by.training.projectspeedrun.validator.ValidatorFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ResourceBundle;

public class GameSaveAction extends AdministratorAction {

    private static final Logger LOGGER = LogManager.getLogger(GameSaveAction.class);

    @Override
    public Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        ResourceBundle bundle = getResourceBundle(request);
        Forward forward;
        Runner authorizedRunner = getAuthorizedRunner();
        int gameId = Integer.parseInt(request.getParameter("gameId"));
        Validator<Game> validator = ValidatorFactory.createValidator(Game.class);
        try {
            Game newGame = validator.validate(request);
            GameService service = factory.getService(GameService.class);
            Game game = service.findByIdentity(gameId);
            if (game != null) {
                game.setId(gameId);
                game.setGameTitle(newGame.getGameTitle());
                forward = new Forward("/game.html?title="+game.getGameTitle());
                if (newGame.getDescription() != null) {
                    game.setDescription(newGame.getDescription());
                }
                game.setLogo(newGame.getLogo());
                service.save(game);
                forward.getAttributes().put("message", bundle.getString("gameSaveMessage"));
                LOGGER.info(String.format("User \"%s\" changed game with id %d", authorizedRunner.getLogin(), game.getId()));
            } else {
                forward = new Forward("/games.html");
                forward.getAttributes().put("message", bundle.getString("gameNotFoundMessage"));
                LOGGER.info(String.format("User \"%s\" tried to change game with id %d but it was not found", authorizedRunner.getLogin(), gameId));
            }
        } catch (IncorrectFormDataException e) {
            forward = new Forward("/game/edit.html");
            forward.getAttributes().put("gameId", gameId);
            forward.getAttributes().put("message", bundle.getString("incorrectDataMessage"));
            LOGGER.warn(String.format("Incorrect data was found when user \"%s\" tried to save game with id %d", getAuthorizedRunner().getLogin(), gameId), e);
        }
        return forward;
    }
}
