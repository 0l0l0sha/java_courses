package by.training.projectspeedrun.action;

import by.training.projectspeedrun.exeptions.PersistentException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

public class MainAction extends Action {
    private static List<MenuItem> menu = new ArrayList<>();

    static {
        menu.add(new MenuItem("/games.html", "games"));
        menu.add(new MenuItem("/runners.html", "runners"));
    }

    @Override
    public Action.Forward exec(HttpServletRequest request, HttpServletResponse response) throws PersistentException {
        HttpSession session = request.getSession();
        if (session.getAttribute("menu") == null) {
            session.setAttribute("menu", menu);
        }

       /*if (session.getAttribute("locale") == null) {
            session.setAttribute("locale", menu);
        }*/
        return new Forward("/games.html");
    }
}
