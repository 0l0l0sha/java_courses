package by.training.projectspeedrun.validator;

import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;

public class RunnerValidator implements Validator<Runner> {
    @Override
    public Runner validate(HttpServletRequest request) throws IncorrectFormDataException {
        Runner runner = new Runner();
        String parameter = request.getParameter("id");
        if (parameter != null) {
            try {
                runner.setId(Integer.parseInt(parameter));
            } catch (NumberFormatException e) {
                throw new IncorrectFormDataException("id", parameter);
            }
        }

        parameter = request.getParameter("login");
        if (parameter != null && !parameter.isEmpty()) {
            runner.setLogin(parameter);
        } else {
            throw new IncorrectFormDataException("login", parameter);
        }

        parameter = request.getParameter("email");
        if (parameter != null && !parameter.isEmpty()) {
            runner.setEmail(parameter);
        } else {
            throw new IncorrectFormDataException("email", parameter);
        }

        parameter = request.getParameter("country");
        if (parameter != null && !parameter.isEmpty()) {
            try {
                runner.setCountry(parameter);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("country", parameter);
            }
        }

        parameter = request.getParameter("twitch");
        if (parameter != null && !parameter.isEmpty()) {
            try {
                runner.setTwitch(parameter);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("twitch", parameter);
            }
        }

        parameter = request.getParameter("youtube");
        if (parameter != null && !parameter.isEmpty()) {
            try {
                runner.setYoutube(parameter);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("youtube", parameter);
            }
        }

        try {
            Part imageFile = request.getPart("image");
            if (imageFile != null) {
                try {
                    byte[] imgData = IOUtils.toByteArray(imageFile.getInputStream());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    runner.setImage(image);
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    throw new IncorrectFormDataException("image", parameter);
                }
            }
        } catch (IOException | ServletException e) {
            throw new IncorrectFormDataException("image", parameter);
        }


        return runner;
    }
}
