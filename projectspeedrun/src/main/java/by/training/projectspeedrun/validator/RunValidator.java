package by.training.projectspeedrun.validator;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Platform;
import by.training.projectspeedrun.entity.Role;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.sql.Time;
import java.util.TimeZone;

public class RunValidator implements Validator<Run> {
    @Override
    public Run validate(HttpServletRequest request) throws IncorrectFormDataException {
        Run run = new Run();
        String parameter = request.getParameter("id");
        if (parameter != null) {
            try {
                run.setId(Integer.parseInt(parameter));
            } catch (NumberFormatException e) {
                throw new IncorrectFormDataException("id", parameter);
            }
        }

        parameter = request.getParameter("categoryId");
        try {

            CategoryOfRun category = new CategoryOfRun();
            category.setId(Integer.valueOf(parameter));
            run.setCategory(category);
        } catch (NumberFormatException e) {
            throw new IncorrectFormDataException("categoryId", parameter);
        }

        parameter = request.getParameter("runnerId");
        try {
            Runner runner = new Runner();
            runner.setId(Integer.valueOf(parameter));
            run.setRunner(runner);
        } catch (NumberFormatException e) {
            throw new IncorrectFormDataException("runnerId", parameter);
        }

        int offset = TimeZone.getDefault().getRawOffset();
        parameter = request.getParameter("realTime");
        if (parameter != null && !parameter.isEmpty()) {
            try {
                String[] parts= parameter.split("[hms]");
                int h = Integer.parseInt(parts[0]);
                int m = Integer.parseInt(parts[1].trim());
                int s = Integer.parseInt(parts[2].trim());
                run.setRealTime(new Time((((h*60+m)*60)+s)*1000));
            } catch (NumberFormatException e) {
                throw new IncorrectFormDataException("realTime", parameter);
            }
        }

        parameter = request.getParameter("inGameTime");
        if (parameter != null && !parameter.isEmpty()) {
            try { String[] parts= parameter.split("[hms]");
                int h = Integer.parseInt(parts[0]);
                int m = Integer.parseInt(parts[1].trim());
                int s = Integer.parseInt(parts[2].trim());
                run.setRealTime(new Time((((h*60+m)*60)+s)*1000 ));
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("inGameTime", parameter);
            }
        }

        parameter = request.getParameter("date");
        try {
            run.setDate(Date.valueOf(parameter));
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IncorrectFormDataException("date", parameter);
        }

        parameter = request.getParameter("video");
        if (parameter != null && !parameter.isEmpty()) {
            run.setVideo(parameter);
        } else {
            throw new IncorrectFormDataException("Video", parameter);
        }

        parameter = request.getParameter("platform");
        if (parameter != null) {
            try {
                run.setPlatform(Platform.valueOf(parameter));
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("platform", parameter);
            }
        }

        return run;
    }
}
