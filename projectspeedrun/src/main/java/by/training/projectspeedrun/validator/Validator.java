package by.training.projectspeedrun.validator;

import javax.servlet.http.HttpServletRequest;

import by.training.projectspeedrun.exeptions.IncorrectFormDataException;

public interface Validator<Type> {
	Type validate(HttpServletRequest request) throws IncorrectFormDataException;
}