package by.training.projectspeedrun.validator;

import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Platform;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.Base64;

public class GameValidator implements Validator<Game> {
    @Override
    public Game validate(HttpServletRequest request) throws IncorrectFormDataException {
        Game game = new Game();
        String parameter = request.getParameter("id");
        if (parameter != null) {
            try {
                game.setId(Integer.parseInt(parameter));
            } catch (NumberFormatException e) {
                throw new IncorrectFormDataException("id", parameter);
            }
        }

        parameter = request.getParameter("gameTitle");
        try {
            game.setGameTitle(parameter);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IncorrectFormDataException("gameTitle", parameter);
        }

        parameter = request.getParameter("description");
        if (parameter != null) {
            try {
                game.setDescription(parameter);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("description", parameter);
            }
        }

        parameter = request.getParameter("image");
        try {
            Part imageFile = request.getPart("image");
            if (imageFile != null) {
                try {
                    byte[] imgData = IOUtils.toByteArray(imageFile.getInputStream());
                    String image = new String(Base64.getEncoder().encode(imgData));
                    game.setLogo(image);
                } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                    throw new IncorrectFormDataException("logo", parameter);
                }
            }
        } catch (IOException | ServletException e) {
            throw new IncorrectFormDataException("logo", parameter);
        }
        return game;
    }
}
