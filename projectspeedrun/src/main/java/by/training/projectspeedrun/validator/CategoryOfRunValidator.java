package by.training.projectspeedrun.validator;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.exeptions.IncorrectFormDataException;

import javax.servlet.http.HttpServletRequest;

public class CategoryOfRunValidator implements Validator<CategoryOfRun> {
    @Override
    public CategoryOfRun validate(HttpServletRequest request) throws IncorrectFormDataException {
        CategoryOfRun category = new CategoryOfRun();
        String parameter = request.getParameter("id");
        if (parameter != null) {
            try {
                category.setId(Integer.parseInt(parameter));
            } catch (NumberFormatException e) {
                throw new IncorrectFormDataException("id", parameter);
            }
        }

        parameter = request.getParameter("gameId");
        try {
            Game game = new Game();
            game.setId(Integer.parseInt(parameter));
            category.setGame(game);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IncorrectFormDataException("gameTitle", parameter);
        }

        parameter = request.getParameter("categoryTitle");
        try {
            category.setCategoryTitle(parameter);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IncorrectFormDataException("categoryTitle", parameter);
        }


        parameter = request.getParameter("rules");
        if (parameter != null) {
            try {
                category.setRules(parameter);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IncorrectFormDataException("rules", parameter);
            }
        }
        return category;
    }
}
