package by.training.projectspeedrun.validator;

import by.training.projectspeedrun.entity.CategoryOfRun;
import by.training.projectspeedrun.entity.Game;
import by.training.projectspeedrun.entity.Run;
import by.training.projectspeedrun.entity.Runner;

import java.util.HashMap;
import java.util.Map;

public class ValidatorFactory {
	private static Map<Class<?>, Class<? extends Validator<?>>> validators = new HashMap<>();

	static {
		validators.put(Runner.class, RunnerValidator.class);
		validators.put(Run.class, RunValidator.class);
		validators.put(Game.class, GameValidator.class);
		validators.put(CategoryOfRun.class, CategoryOfRunValidator.class);
	}

	@SuppressWarnings("unchecked")
	public static <Type> Validator<Type> createValidator(Class<Type> entity) {
		try {
			return (Validator<Type>)validators.get(entity).newInstance();
		} catch(InstantiationException | IllegalAccessException e) {
			return null;
		}
	}
}
