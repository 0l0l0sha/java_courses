<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 16.07.2019
  Time: 3:44
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>


<%--TODO подписать колонки тут и в ранере--%>

<u:html title="${game.gameTitle}" message="${message}">
    <h1 align="center">${game.gameTitle}</h1>
    <hr>
    <div class="container-fluid">
        <c class="row">
            <br>
            <div class="col-xs-3" style="height: 100%">
                <img alt="No image" id="photo" src="data:image/jpeg;base64,${game.logo}">
                <div><p>${game.gameTitle}</p></div>
                <p class="small text-muted">${game.followersQuantity} followers</p>
                <br>

                <c:if test="${isAuthorized}">
                    <c:choose>
                        <c:when test="${isFollowed}">
                            <c:url value="/game/follow.html" var="followUrl"/>
                            <form action="${followUrl}" method="post">
                                <INPUT type="hidden" name="gameId" value="${game.id}">
                                <BUTTON type="submit">Подписаться</BUTTON>
                            </FORM>
                        </c:when>
                        <c:otherwise>
                            <c:url value="/game/unfollow.html" var="unFollowUrl"/>
                            <form action="${unFollowUrl}" method="post">
                                <INPUT type="hidden" name="gameId" value="${game.id}">
                                <BUTTON type="submit">Отписаться</BUTTON>
                            </FORM>
                        </c:otherwise>
                    </c:choose>
                    <br>
                    <c:if test="${not empty categories}">
                        <c:url value="/run/create.html" var="runCreateUrl"/>
                        <form action="${runCreateUrl}" method="post">
                            <INPUT type="hidden" name="gameTitle" value="${game.gameTitle}">
                            <INPUT type="hidden" name="categoryId" value="${category.id}">
                            <BUTTON type="submit">Создать забег</BUTTON>
                        </FORM>
                    </c:if>
                    <br>
                    <c:if test="${isAdministrator}">
                        <c:url value="/game/edit.html" var="gameEditUrl"/>
                        <form action="${gameEditUrl}" method="post">
                            <INPUT type="hidden" name="gameId" value="${game.id}">
                            <BUTTON type="submit">Edit game</BUTTON>
                        </FORM>
                        <LABEL for="moderDel">Delete Moder:</LABEL>
                        <c:url value="/moderator/delete.html" var="moderDeleteUrl"/>
                        <form action="${moderDeleteUrl}" method="post">
                            <INPUT type="hidden" name="gameId" value="${game.id}">
                            <INPUT type="text" id="moderDel" name="runnerLogin">
                            <BUTTON type="submit">Забрать модерку</BUTTON>
                        </FORM>
                        <LABEL for="moderSet">Set Moder:</LABEL>
                        <c:url value="/moderator/create.html" var="moderCreateUrl"/>
                        <form action="${moderCreateUrl}" method="post">
                            <INPUT type="hidden" name="gameId" value="${game.id}">
                            <INPUT type="text" id="moderSet" name="runnerLogin">
                            <BUTTON type="submit">Сделать модератором</BUTTON>
                        </FORM>
                    </c:if>
                </c:if>
                <c:url value="/game.html?title=${game.gameTitle}" var="leaderBordUrl"/>
                <form action="${leaderBordUrl}" method="post">
                    <BUTTON type="submit">Доска лидеров</BUTTON>
                </FORM>
                <br>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#gameModal">Description
                </button>
                <div class="modal fade" id="gameModal" tabindex="-1" role="dialog" aria-labelledby="gameModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p>${game.description}</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <p>Модераторы:</p>
                <c:forEach items="${moderators}" var="moderator" varStatus="status">
                    <div class="col-xs-12">
                        <a href="${contextPath}/runner.html?login=${moderator.login}">
                            <div>${moderator.login}</div>
                        </a>
                    </div>
                </c:forEach>
            </div>


            <c:if test="${isModerator}">
            <div class="col-xs-3">
                <c:url value="/category/create.html" var="categoryCreateUrl"/>
                <div style="overflow: hidden;"></div>
                <form action="${categoryCreateUrl}" method="post">
                    <INPUT type="hidden" name="gameId" value="${game.id}">
                    <BUTTON type="submit">Create category</BUTTON>
                </FORM>
            </div>
            <c:if test="${not empty categories}">
            <div class="col-xs-3-push-3">
                <c:url value="/category/edit.html" var="categoryEditUrl"/>
                <div style="overflow: hidden;">
                    <form action="${categoryEditUrl}" method="post">
                        <INPUT type="hidden" name="gameId" value="${game.id}">
                        <INPUT type="hidden" name="categoryId" value="${category.id}">
                        <BUTTON type="submit">Edit category</BUTTON>
                    </FORM>
                </div>
            </div>
            </c:if>
            </c:if>

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#categoryModal">Rules
            </button>
            <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p>${category.rules}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-9">
                <c:forEach items="${categories}" var="categoryFromList" varStatus="status">
                    <div class="btn-group" role="group" aria-label="Basic example" class='border-width: 2px;'>
                        <a href="${contextPath}/game.html?title=${game.gameTitle}&category=${categoryFromList.categoryTitle}">
                            <c:choose>
                                <c:when test="${category.categoryTitle == categoryFromList.categoryTitle}">
                                    <button type="button" style="background-color: darkgreen"
                                    >${categoryFromList.categoryTitle}</button>
                                </c:when>
                                <c:otherwise>
                                    <button type="button"
                                    >${categoryFromList.categoryTitle}</button>
                                </c:otherwise>
                            </c:choose>
                        </a>
                    </div>
                </c:forEach>
                <br>
            </div>
            <br>

            <div class="col-xs-9">
                <br>
                <div class="col-xs-2">
                    <p style="font-size: 18px; text-align: left;">Runner</p>
                </div>
                <div class="col-xs-2">
                    <p style="font-size: 18px; text-align: right;">Real Time</p>
                </div>
                <div class="col-xs-4">
                    <div class="col-xs-7">
                        <p style="font-size: 18px; text-align: center;">In Game Time</p>
                    </div>
                    <div class="col-xs-5">
                        <p style="font-size: 18px; text-align: left;">Platform</p>
                    </div>
                </div>
                <div class="col-xs-2">
                    <p style="font-size: 18px; text-align: left;">Date</p>
                </div>
                <div class="col-xs-2">
                    <p style="font-size: 18px; text-align: left;">Video</p>
                </div>

                <c:forEach items="${runs}" var="run" varStatus="status">
                    <c:choose>
                        <c:when test="${!run.submited}">
                            <div class="col-xs-12" style="background-color: rgba(212,203,205, 0.8)">
                                <br>
                                <div class="col-xs-3">
                                    <a href="${contextPath}/runner.html?login=${run.runner.login}">
                                        <div>${run.runner.login}</div>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.realTime}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.inGameTime}</div>
                                </div>
                                <div class="col-xs-1">
                                    <div>${run.platform}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.date}</div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="${contextPath}/run.html?id=${run.id}">
                                        <c:if test="${not empty run.video}">
                                            <div>Video exist</div>
                                        </c:if>
                                    </a>
                                </div>
                                <br>
                                <hr>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="col-xs-12">
                                <br>
                                <div class="col-xs-3">
                                    <a href="${contextPath}/runner.html?login=${run.runner.login}">
                                        <div>${run.runner.login}</div>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.realTime}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.inGameTime}</div>
                                </div>
                                <div class="col-xs-1">
                                    <div>${run.platform}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.date}</div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="${contextPath}/run.html?id=${run.id}">
                                        <c:if test="${not empty run.video}">
                                            <div>Video exist</div>
                                        </c:if>
                                    </a>
                                </div>
                                <br>
                                <hr>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </div>
    </div>
</u:html>