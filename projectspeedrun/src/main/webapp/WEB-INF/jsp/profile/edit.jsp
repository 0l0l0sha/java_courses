<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Профиль пользователя «${authorizedRunner.login}»" message="${message}"
        validator="validator-of-edit-profile-form.js">
    <H2>Профиль пользователя «${authorizedRunner.login}»</H2>
    <c:url value="/profile/save.html" var="profileSaveUrl"/>
    <FORM action="${profileSaveUrl}" method="post" enctype="multipart/form-data" onsubmit="return validateEditProfile(this)">
        <LABEL for="login">Имя пользователя:</LABEL>
        <INPUT type="text" id="login" name="login" value="${authorizedRunner.login}">
        <LABEL for="old-password">Старый пароль:</LABEL>
        <INPUT type="password" id="old-password" name="old-password">
        <LABEL for="new-password-1">Новый пароль:</LABEL>
        <INPUT type="password" id="new-password-1" name="new-password">
        <LABEL for="new-password-2">Новый пароль ещё раз:</LABEL>
        <INPUT type="password" id="new-password-2">

        <LABEL for="email">e-mail:</LABEL>
        <INPUT type="text" id="email" name="email" value="${authorizedRunner.email}">
        <LABEL for="youtube">YouTube:</LABEL>
        <c:choose>
            <c:when test="${not empty authorizedRunner.youtube}">
                <INPUT type="url" id="youtube" name="youtube" value="${authorizedRunner.youtube}">
            </c:when>
            <c:otherwise>
                <INPUT type="url" id="youtube" name="youtube">
            </c:otherwise>
        </c:choose>

        <LABEL for="twitch">Twitch:</LABEL>
        <c:choose>
            <c:when test="${not empty authorizedRunner.twitch}">
                <INPUT type="url" id="twitch" name="twitch" value="${authorizedRunner.twitch}">
            </c:when>
            <c:otherwise>
                <INPUT type="url" id="twitch" name="twitch">
            </c:otherwise>
        </c:choose>

        <LABEL for="country">Country:</LABEL>
        <c:choose>
            <c:when test="${not empty authorizedRunner.country}">
                <INPUT type="text" id="country" name="country" value="${authorizedRunner.country}">
            </c:when>
            <c:otherwise>
                <INPUT type="text" id="country" name="country">
            </c:otherwise>
        </c:choose>

        <LABEL for="image">Image.jpg:</LABEL>
        <c:choose>
            <c:when test="${not empty authorizedRunner.image}">
                <input id ="image" name="image" type="file" accept="image/jpeg, image/jpg" value="${authorizedRunner.image}">
            </c:when>
            <c:otherwise>
                <input id ="image" name="image" type="file" accept="image/jpeg, image/jpg">
            </c:otherwise>
        </c:choose>

        <BUTTON type="submit">Подтвердить</BUTTON>

    </FORM>
</u:html>
