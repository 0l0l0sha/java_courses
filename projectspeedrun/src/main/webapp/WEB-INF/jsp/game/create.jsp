<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 27.10.2019
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Создать Игру" message="${message}"
        validator="validator-of-edit-game-form.js">
    <H2>Создать Игру</H2>
    <c:url value="/game/new.html" var="gameSaveUrl"/>
    <FORM action="${gameSaveUrl}" method="post" enctype="multipart/form-data" onsubmit="return validateEditGame(this)">

        <LABEL for="gameTitle">Game Title:</LABEL>
        <INPUT required type="text" id="gameTitle" name="gameTitle">


        <LABEL for="description">Description (only 500 symbols):</LABEL>
        <INPUT type="text" id="description" name="description">


        <LABEL for="image">Logo.jpg:</LABEL>
        <input required id="image" name="logo" type="file" accept="image/jpeg, image/jpg">


        <BUTTON type="submit">Подтвердить</BUTTON>

    </FORM>
</u:html>
