<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 25.10.2019
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Изменить ${game.gameTitle}»" message="${message}"
        validator="validator-of-edit-game-form.js">
    <H2>Изменить ${game.gameTitle}</H2>
    <c:url value="/game/save.html" var="gameSaveUrl"/>
    <FORM action="${gameSaveUrl}" method="post"
          enctype="multipart/form-data" onsubmit="return validateEditGame(this)">
        <LABEL for="gameTitle">Game Title:</LABEL>
        <INPUT required type="text" id="gameTitle" name="gameTitle" value="${game.gameTitle}">


        <LABEL for="description">Description (only 500 symbols):</LABEL>
        <c:choose>
            <c:when test="${not empty game.description}">
                <INPUT type="text" id="description" name="description" value="${game.description}">
            </c:when>
            <c:otherwise>
                <INPUT type="text" id="description" name="description">
            </c:otherwise>
        </c:choose>

        <LABEL for="image">Logo.jpg:</LABEL>
        <c:choose>
            <c:when test="${not empty game.logo}">
                <input required id="image" name="image" type="file" accept="image/jpeg, image/jpg" value="${game.logo}">

            </c:when>
            <c:otherwise>
                <input required id="image" name="image" type="file" accept="image/jpeg, image/jpg">
            </c:otherwise>
        </c:choose>

        <BUTTON type="submit">Подтвердить</BUTTON>

        <INPUT type="hidden" name="gameId" value="${game.id}">
    </FORM>

    <BUTTON type="button" onclick="submitFormById('form-delete')">Удалить</BUTTON>
    <c:url value="/game/delete.html" var="gameDeleteUrl"/>
    <FORM action="${gameDeleteUrl}" method="post" id="form-delete"
          onsubmit="return confirmation(this, 'Вы уверены, что хотите удалить игру?')">
        <INPUT type="hidden" name="gameId" value="${game.id}">
    </FORM>
</u:html>
