<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 31.10.2019
  Time: 7:19
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html title="Список игр" message="${message}">
    <div class="col-xs-2"></div>
    <h1>Game's list</h1>
    <hr>
    <div class="container-fluid">
    <div class="row">
        <div style="overflow: hidden; padding-bottom: 5%;">
            <c:forEach items="${games}" var="game" varStatus="status">
                <div class="col-xs-3" style = "margin-bottom: 5%;">
                    <a href="${guri}?title=${game.gameTitle}">
                        <img  alt="No image" src="data:image/jpeg;base64,${game.logo}">
                        <p>${game.gameTitle}</p>
                    </a>
                    <p class="small text-muted">${game.followersQuantity} followers</p>
                </div>
            </c:forEach>
            <c:forEach items="${runners}" var="runner" varStatus="status">
                <div class="col-xs-3" style = "margin-bottom: 5%;">
                    <a href="${ruri}?login=${runner.login}">
                        <img alt="No image"  src="data:image/jpeg;base64,${runner.image}">
                        <p>${runner.login}</p>
                    </a>
                </div>
            </c:forEach>
        </div>

    </div>
</u:html>
