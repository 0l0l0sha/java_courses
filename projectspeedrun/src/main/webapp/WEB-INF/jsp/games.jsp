<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html title="Список игр" message="${message}">
    <div class="col-xs-2"></div>
    <h1>Game's list</h1>
    <hr>
    <div class="container-fluid">
    <div class="row">
        <div style="overflow: hidden; padding-bottom: 5%;">
            <c:forEach items="${games}" var="game" varStatus="status">
                <div class="col-xs-3" style = "margin-bottom: 5%;">
                    <a href="${uri}?title=${game.gameTitle}">
                        <img alt="No image" id="photo" src="data:image/jpeg;base64,${game.logo}">
                        <p>${game.gameTitle}</p>
                    </a>
                    <p class="small text-muted">${game.followersQuantity} followers</p>
                </div>
            </c:forEach>
        </div>

    </div>
</u:html>


