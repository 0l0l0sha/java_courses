<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 04.10.2019
  Time: 21:05
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>

<u:html title="${runner.login}" message="${message}">
    <h1 align="center">${runner.login}</h1>
    <div class="container-fluid">
        <div class="row">
            <br>
            <div class="col-xs-3">
                <img alt="No image" id="photo" src="data:image/jpeg;base64,${runner.image}">
                <p>${runner.country}</p>
                <A href="${runner.twitch}">Twitch</A>
                <A href="${runner.youtube}">YouTube</A>
                <br>
                <c:if test="${runner.id == authorizedRunner.id}">
                    <c:url value="/profile/edit.html" var="editProfile"/>
                    <A href="${editProfile}">изменить профиль</A>
                </c:if>
                <br>
            </div>

            <div class="col-xs-9">
                <c:forEach items="${runs}" var="run" varStatus="status">
                    <c:choose>
                        <c:when test="${!run.submited}">
                            <div class="col-xs-12" style="background-color: rgba(212,203,205, 0.8)">
                                <br>
                                <div class="col-xs-3">
                                    <a href="${contextPath}/game.html?title=${run.category.game.gameTitle}&category=${run.category.categoryTitle}">
                                        <div>${run.category.game.gameTitle} - ${run.category.categoryTitle}</div>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.realTime}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.inGameTime}</div>
                                </div>
                                <div class="col-xs-1">
                                    <div>${run.platform}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.date}</div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="${contextPath}/run.html?id=${run.id}">
                                        <c:if test="${not empty run.video}">
                                            <div>Video exist</div>
                                        </c:if>
                                    </a>
                                </div>
                                <br>
                                <hr>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="col-xs-12">
                                <br>
                                <div class="col-xs-3">
                                    <a href="${contextPath}/game.html?title=${run.category.game.gameTitle}&category=${run.category.categoryTitle}">
                                        <div>${run.category.game.gameTitle} - ${run.category.categoryTitle}</div>
                                    </a>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.realTime}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.inGameTime}</div>
                                </div>
                                <div class="col-xs-1">
                                    <div>${run.platform}</div>
                                </div>
                                <div class="col-xs-2">
                                    <div>${run.date}</div>
                                </div>
                                <div class="col-xs-2">
                                    <a href="${contextPath}/run.html?id=${run.id}">
                                        <c:if test="${not empty run.video}">
                                            <div>Video exist</div>
                                        </c:if>
                                    </a>
                                </div>
                                <br>
                                <hr>
                            </div>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>

                <c:if test="${isAdministrator}">
                    <BUTTON type="button" onclick="submitFormById('form-delete')">Удалить</BUTTON>
                    <c:url value="/runner/delete.html" var="runDeleteUrl"/>
                    <FORM action="${runDeleteUrl}" method="post" id="form-delete"
                          onsubmit="return confirmation(this, 'Вы уверены, что хотите удалить аккаунт?')">
                        <INPUT type="hidden" name="runnerId" value="${runner.id}">
                    </FORM>
                </c:if>
            </div>
        </div>
    </div>
</u:html>
