<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 25.10.2019
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Изменить забег «${category.categoryTitle} by ${runner.login} - ${game.gameTitle}»" message="${message}"
        validator="validator-of-edit-run-form.js">
    <H2>Изменить забег «${category.categoryTitle} by ${runner.login} - ${game.gameTitle}»</H2>
    <c:url value="/run/save.html" var="runSaveUrl"/>
    <FORM action="${runSaveUrl}" method="post" onsubmit="return validateEditRun(this)">
        <LABEL for="login">Имя пользователя:</LABEL>
        <INPUT type="text" id="login" name="login" value="${runner.login}" disabled>

        <LABEL for="date">Date:</LABEL>
        <c:choose>
            <c:when test="${not empty run.date}">
                <INPUT type="date" name="date" value="${run.date}" disabled>
                <INPUT type="hidden"  name="date" value="${run.date}">
            </c:when>
            <c:otherwise>
                <INPUT required type="date" id="date" name="date">
            </c:otherwise>
        </c:choose>


        <LABEL for="realTime">Real Time:</LABEL>
        <c:choose>
            <c:when test="${not empty run.realTime}">
                <INPUT type="text" id="realTime" name="realTime" value="${run.realTime}">
            </c:when>
            <c:otherwise>
                <INPUT type="text" id="realTime" name="realTime">
            </c:otherwise>
        </c:choose>

        <LABEL for="inGameTime">in Game Time:</LABEL>
        <c:choose>
            <c:when test="${not empty run.inGameTime}">
                <INPUT type="text" id="inGameTime" name="inGameTime" value="${run.inGameTime}">
            </c:when>
            <c:otherwise>
                <INPUT type="text" id="inGameTime" name="inGameTime">
            </c:otherwise>
        </c:choose>

        <LABEL for="platform">Platform:</LABEL>
        <select id="platform" name="platform" required>
            <c:choose>
                <c:when test="${not empty run.platform}">
                    <c:forEach items="${platforms}" var="platform" varStatus="status">
                        <c:choose>
                            <c:when test="${platform == run.platform}">
                                <option selected value="${platform}">${platform}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${platform}">${platform}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${platforms}" var="platform" varStatus="status">
                        <option value="${platform}">${platform}</option>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </select>


        <LABEL for="video">Video:</LABEL>
        <c:choose>
            <c:when test="${not empty run.video}">
                <INPUT required type="url" id="video" name="video" value="${run.video}">
            </c:when>
            <c:otherwise>
                <INPUT required type="url" id="video" name="video">
            </c:otherwise>
        </c:choose>


        <INPUT type="hidden" name="runnerId" value="${runner.id}">
        <INPUT type="hidden" name="categoryId" value="${category.id}">
        <INPUT type="hidden" name="runId" value="${run.id}">
        <BUTTON type="submit">Подтвердить</BUTTON>


    </FORM>
</u:html>