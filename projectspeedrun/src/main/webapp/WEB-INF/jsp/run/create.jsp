<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 27.10.2019
  Time: 19:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Создать Забег «${category.categoryTitle} by ${runner.login} - ${game.gameTitle}»" message="${message}"
        validator="validator-of-edit-run-form.js">
    <H2>Создать Забег «${category.categoryTitle} by ${runner.login} - ${game.gameTitle}»</H2>
    <c:url value="/run/new.html" var="runSaveUrl"/>
    <FORM action="${runSaveUrl}" method="post" onsubmit="return validateEditRun(this)">
        <LABEL for="login">Имя пользователя:</LABEL>
        <INPUT type="text" id="login" name="login" value="${runner.login}" disabled>

        <LABEL for="date">Date:</LABEL>
        <INPUT required type="date" id="date" name="date" min="2005-01-01">


        <LABEL for="realTime">Real Time:</LABEL>
        <INPUT type="text" id="realTime" name="realTime">


        <LABEL for="inGameTime">in Game Time:</LABEL>
        <INPUT type="text" id="inGameTime" name="inGameTime">


        <LABEL for="platform">Platform:</LABEL>
        <select id="platform" name="platform" required>
            <c:forEach items="${platforms}" var="platform" varStatus="status">
                <option value="${platform}">${platform}</option>
            </c:forEach>
        </select>


        <LABEL for="video">Video URL:</LABEL>
        <INPUT required type="url" id="video" name="video">


        <INPUT type="hidden" name="runnerId" value="${runner.id}">
        <INPUT type="hidden" name="categoryId" value="${category.id}">
        <BUTTON type="submit">Подтвердить</BUTTON>

    </FORM>
</u:html>
