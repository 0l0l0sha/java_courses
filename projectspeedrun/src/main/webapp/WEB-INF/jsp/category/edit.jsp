<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 25.10.2019
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Изменить Категорию в ${game.gameTitle}" message="${message}"
        validator="validator-of-edit-category-form.js">
    <H2>Изменить Категорию в ${game.gameTitle}</H2>
    <c:url value="/category/save.html" var="categorySaveUrl"/>
    <FORM action="${categorySaveUrl}" method="post"
          enctype="multipart/form-data" onsubmit="return validateEditCategory(this)">

        <LABEL for="categoryTitle">Category Title:</LABEL>
        <INPUT required type="text" id="categoryTitle" name="categoryTitle" value="${category.categoryTitle}">

        <LABEL for="rules">Rules:</LABEL>
        <c:choose>
            <c:when test="${not empty category.rules}">
                <INPUT type="text" id="rules" name="rules" value="${category.rules}">
            </c:when>
            <c:otherwise>
                <INPUT type="text" id="rules" name="rules">
            </c:otherwise>
        </c:choose>


        <BUTTON type="submit">Подтвердить</BUTTON>

        <INPUT type="hidden" name="gameId" value="${game.id}">
        <INPUT type="hidden" name="categoryId" value="${category.id}">
    </FORM>

    <BUTTON type="button" onclick="submitFormById('form-delete')">Удалить</BUTTON>
    <c:url value="/category/delete.html" var="categoryDeleteUrl"/>
    <FORM action="${categoryDeleteUrl}" method="post" id="form-delete"
          onsubmit="return confirmation(this, 'Вы уверены, что хотите удалить  категорию?')">
        <INPUT type="hidden" name="categoryId" value="${category.id}">
        <INPUT type="hidden" name="gameId" value="${game.id}">
    </FORM>
</u:html>
