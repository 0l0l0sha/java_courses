<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 27.10.2019
  Time: 20:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Создать Категорию в ${game.gameTitle}" message="${message}"
        validator="validator-of-edit-category-form.js">
    <H2>Создать Категорию в ${game.gameTitle}</H2>
    <c:url value="/category/new.html" var="categorySaveUrl"/>
    <FORM action="${categorySaveUrl}" method="post" enctype="multipart/form-data" onsubmit="return validateEditCategory(this)">

        <LABEL for="categoryTitle">Category Title:</LABEL>
        <INPUT required type="text" id="categoryTitle" name="categoryTitle">


        <LABEL for="rules">Rules:</LABEL>
        <INPUT type="text" id="rules" name="rules">


        <BUTTON type="submit">Подтвердить</BUTTON>

        <INPUT type="hidden" name="gameId" value="${game.id}">

    </FORM>
</u:html>
