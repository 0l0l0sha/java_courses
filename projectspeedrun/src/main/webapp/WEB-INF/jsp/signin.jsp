<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 27.10.2019
  Time: 19:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>
<u:html title="Регистрация" message="${message}"
        validator="validator-of-edit-profile-form.js">
    <H2>Регистрация</H2>
    <c:url value="/runner/create.html" var="newRunnerUrl"/>
    <FORM action="${newRunnerUrl}" method="post" enctype="multipart/form-data"
          onsubmit="return validateEditProfile(this)">
        <LABEL for="login">Имя пользователя:</LABEL>
        <INPUT required type="text" id="login" name="login">

        <LABEL for="new-password-1">Новый пароль:</LABEL>
        <INPUT type="password" id="new-password-1" name="new-password">
        <LABEL for="new-password-2">Новый пароль ещё раз:</LABEL>
        <INPUT type="password" id="new-password-2">

        <LABEL for="email">e-mail:</LABEL>
        <INPUT required type="text" id="email" name="email">

        <LABEL for="youtube">YouTube:</LABEL>
        <INPUT type="url" id="youtube" name="youtube">

        <LABEL for="twitch">Twitch:</LABEL>
        <INPUT type="url" id="twitch" name="twitch">

        <LABEL for="country">Country:</LABEL>
        <INPUT type="text" id="country" name="country">

        <LABEL for="image">Image.jpg :</LABEL>
        <input id="image" name="image" type="file" accept="image/jpeg, image/jpg">

        <BUTTON type="submit">Подтвердить</BUTTON>
    </FORM>
</u:html>