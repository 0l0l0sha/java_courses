<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 15.10.2019
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="u" %>


<u:html title="${run.category.categoryTitle} in ${run.realTime} by ${run.runner.login} - ${game.gameTitle}"
        message="${message}">
    <h1 align="center">${game.gameTitle}</h1>
    <hr>
    <div class="container-fluid">
        <c class="row">
            <div class="col-xs-12">
                <div class="col-xs-3">
                    <img id="photo" src="data:image/jpeg;base64,${game.logo}">
                    <div>${game.gameTitle}</div>
                    <p class="small text-muted">${game.followersQuantity} followers</p>
                    <br>
                    <c:if test="${isAuthorized}">
                        <c:choose>
                            <c:when test="${isFollowed}">
                                <c:url value="/game/follow.html" var="followUrl"/>
                                <form action="${followUrl}" method="post">
                                    <INPUT type="hidden" name="gameId" value="${game.id}">
                                    <BUTTON type="submit">Подписаться</BUTTON>
                                </FORM>
                            </c:when>
                            <c:otherwise>
                                <c:url value="/game/unfollow.html" var="unFollowUrl"/>
                                <form action="${unFollowUrl}" method="post">
                                    <INPUT type="hidden" name="gameId" value="${game.id}">
                                    <BUTTON type="submit">Отписаться</BUTTON>
                                </FORM>
                            </c:otherwise>
                        </c:choose>
                        <br>
                        <c:if test="${isAdministrator}">
                            <c:url value="/game/edit.html" var="gameEditUrl"/>
                            <form action="${gameEditUrl}" method="post">
                                <INPUT type="hidden" name="gameId" value="${game.id}">
                                <BUTTON type="submit">Edit game</BUTTON>
                            </FORM>
                        </c:if>

                    </c:if>
                    <br>
                    <c:url value="/game.html?title=${game.gameTitle}" var="leaderBordUrl"/>
                    <form action="${leaderBordUrl}" method="post">
                        <BUTTON type="submit">Доска лидеров</BUTTON>
                    </FORM>
                    <br>

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#gameModal">Description
                    </button>
                    <div class="modal fade" id="gameModal" tabindex="-1" role="dialog" aria-labelledby="gameModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>${game.description}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <p>Модераторы:</p>
                    <c:forEach items="${moderators}" var="moderator" varStatus="status">
                        <div class="col-xs-12">
                            <a href="${contextPath}/runner.html?login=${moderator.login}">
                                <div>${moderator.login}</div>
                            </a>
                        </div>
                    </c:forEach>
                </div>
                <div class="col-xs-9">
                    <a href="${run.video}">
                        <div>${run.video}</div>
                    </a>
                    <br>
                    <div class="col-xs-12">
                            ${category.categoryTitle} in ${run.realTime} by <a href="${contextPath}/runner.html?login=${run.runner.login}"><div>${run.runner.login}</div></a>- ${game.gameTitle}
                    </div>

                    <div class="col-xs-2">
                        <div>${run.realTime}</div>
                    </div>
                    <div class="col-xs-2">
                        <div>${run.inGameTime}</div>
                    </div>
                    <div class="col-xs-2">
                        <div>${run.platform}</div>
                    </div>
                    <div class="col-xs-2">
                        <div>${run.date}</div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-xs-3 col-xs-offset-3">
                                <c:if test="${run.runner.id == authorizedRunner.id}">
                                    <c:url value="/run/edit.html" var="editRunUrl"/>
                                    <FORM action="${editRunUrl}" method="post">
                                        <INPUT type="hidden" name="gameTitle" value="${game.gameTitle}">
                                        <INPUT type="hidden" name="runId" value="${run.id}">
                                        <INPUT type="hidden" name="categoryId" value="${run.category.id}">
                                        <INPUT type="hidden" name="runnerId" value="${run.runner.id}">
                                        <BUTTON type="submit">Edit run</BUTTON>
                                    </FORM>
                                </c:if>
                            </div>
                            <c:if test="${moderator}">
                                <div class="col-xs-3">
                                    <BUTTON type="button" onclick="submitFormById('form-delete')">Удалить</BUTTON>
                                    <c:url value="/run/delete.html" var="runDeleteUrl"/>
                                    <FORM action="${runDeleteUrl}" method="post" id="form-delete"
                                          onsubmit="return confirmation(this, 'Вы уверены, что хотите удалить забег?')">
                                        <INPUT type="hidden" name="runId" value="${run.id}">
                                        <INPUT type="hidden" name="gameId" value="${game.id}">
                                    </FORM>
                                </div>
                                <c:if test="${!run.submited}">
                                    <div class="col-xs-3">
                                        <c:url value="/run/verify.html" var="followUrl"/>
                                        <form action="${followUrl}" method="post">
                                            <INPUT type="hidden" name="runId" value="${run.id}">
                                            <INPUT type="hidden" name="gameId" value="${game.id}">
                                            <BUTTON type="submit">Верефицировать</BUTTON>
                                        </FORM>
                                    </div>
                                </c:if>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </c>
    </div>
</u:html>
