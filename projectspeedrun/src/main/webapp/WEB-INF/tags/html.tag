<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="message" required="false" rtexprvalue="true" type="java.lang.String"%>
<%@attribute name="validator" required="false" rtexprvalue="true" type="java.lang.String"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="u"%>



<HTML>
<HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <TITLE>SpeedRuin - ${title}</TITLE>
   <c:url value="/main1.css" var="cssUrl"/>
    <LINK rel="stylesheet" type="text/css" href="${cssUrl}">
    <c:url value="/js" var="javascriptUrl"/>
    <SCRIPT type="text/javascript" src="${javascriptUrl}/main.js"></SCRIPT>
    <c:if test="${not empty message}">
        <SCRIPT type="text/javascript">
            startMessage = ${message};
        </SCRIPT>
    </c:if>
    <c:if test="${not empty validator}">
        <SCRIPT type="text/javascript" src="${javascriptUrl}/main-validator.js"></SCRIPT>
        <SCRIPT type="text/javascript" src="${javascriptUrl}/${validator}"></SCRIPT>
    </c:if>
</HEAD>
<BODY>
<u:menu/>
<DIV id="page" class = 'col-xs-8 col-xs-offset-2'>
    <jsp:doBody/>
</DIV>
</BODY>
</HTML>