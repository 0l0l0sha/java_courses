<%@tag language="java" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<DIV id="header">
    <H1>SpeedRuin</H1>
    <UL class="right">
        <LI class="item">
            <c:url value="/img/arrow.gif" var="arrowUrl"/>
            <A href="#" class="drop-button"> Locale <IMG class="arrow" src="${arrowUrl}"></A>
            <OL class="drop">
                <c:url value="/locale.html?locale=ru" var="localeRu"/>
                <LI><A href="${localeRu}">Ru</A></LI>
                <c:url value="/locale.html?locale=en" var="localeEn"/>
                <LI><A href="${localeEn}">En</A></LI>
                <c:url value="/locale.html?locale=uk" var="localeBy"/>
                <LI><A href="${localeBy}">By</A></LI>
            </OL>
        </LI>
        <LI class="item" style="margin-top: 24px">
            <c:url value="/search.html" var="searchUrl"/>
            <form action="${searchUrl}" method="post">
                <INPUT type="search" name="searchInfo" placeholder="Поиск по сайту">
                <BUTTON type="submit">Найти</BUTTON>
            </form>

        </LI>
            <c:forEach items="${menu}" var="item">
            <c:url value="${item.url}" var="itemUrl"/>
            <LI class="item"><A href="${itemUrl}">${item.name}</A></LI>
        </c:forEach>
        <c:choose>
            <c:when test="${not empty authorizedRunner}">
                <LI class="item">
                    <c:url value="/img/arrow.gif" var="arrowUrl"/>
                    <A href="#" class="drop-button">${authorizedRunner.login} <IMG class="arrow" src="${arrowUrl}"></A>
                    <OL class="drop">
                        <c:url value="/runner.html?login=${authorizedRunner.login}" var="runnerProfile"/>
                        <LI><A href="${runnerProfile}">профиль</A></LI>
                        <c:url value="/logout.html" var="logoutUrl"/>
                        <LI><A href="${logoutUrl}">выход</A></LI>
                    </OL>
                </LI>
            </c:when>
            <c:otherwise>
                <LI class="item">
                        <c:url value="signin.html" var="signInUrl"/>
                <LI><A href="${signInUrl}">Sign In</A></LI>
                </LI>
                <LI class="item">
                    <c:url value="login.html" var="logInUrl"/>
                    <LI><A href="${logInUrl}">Log In</A></LI>
                </LI>
            </c:otherwise>
        </c:choose>
    </UL>
</DIV>
