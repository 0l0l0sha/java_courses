function validateEditRun(form) {
    return validateForm(form, [{
        id: "realTime",
        message: "Поле «Time» должно быть в формате **h **m **s где h меньше `20`, `m` и `s` меньше 60",
        checker: checkTime
    }, {
        id: "inGameTime",
        message: "Поле «Time» должно быть в формате **h **m **s где h меньше `20`, `m` и `s` меньше 60",
        checker: checkTime
    }, {
        id: "date",
        message: "Забег не должен быть старше 2005 года и не из будущего",
        checker: checkDate
    }
    ]);
}

function checkTime(value) {
    return checkRegexp(value, "^[01][0-9]h [0-5][0-9]m [0-5][0-9]s$");
}


function checkDate(value) {
    var parts = value.split("[-.]");
    var now = new Date();
    var part;
    if (parts[0].length > parts[2].length)
        part = parts[0]
    else
        part = parts[2]
    return (parseInt(part) > 2005 && parseInt(part) < parseInt(now.getFullYear().toString()));
}

