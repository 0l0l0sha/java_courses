function validateEditProfile(form) {
    return validateForm(form, [{
        id: "login",
        message: "Поле «Имя пользователя» не заполнено или содержит больше 45 символов",
        checker: checkTitle

    }, {
        id: "country",
        message: "В Поле «Страна» не больше 20 символов",
        checker: checkCountry

    }, {
        id: "image",
        message: "Поле «image» должно быть изображением с расширением .jpg/.jpeg, не более 16мб",
        checker: checkLogoExtens
    }, {
        id: "email",
        message: "Поле «email» должно быть задано в формате email",
        checker: checkEmail
    }, {
        id: "twitch",
        message: "Поле «twitch» должно быть url на twitch",
        checker: checkTwitch
    }, {
        id: "youtube",
        message: "Поле «youtube» должно быть url на ваш канал на youtube",
        checker: checkYoutube
    }]) && validateNewPassword(form) && checkFirstFileSize(form);
}

function checkLogoExtens(value) {
    if (checkNotEmpty(value)) {
        var _validFileExtensions = [".jpg", ".jpeg"];
        /*var ext = value.value.match(/\.([^\.]+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'jpeg':
                break;
            default:
                return false;
        }*/
        var sFileName = value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() === sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                return false;
            }
        }
    }
    return true;

}



function checkEmail(value) {
    return checkRegexp(value, "^(.+)(@)(.+)(\.)(.+)$");
}

function checkTwitch(value) {
    return checkRegexp(value, "^(https:\/\/)?(www\.)?twitch\.tv\/.*$");
}

function checkYoutube(value) {
    return checkRegexp(value, "^(https:\/\/)?(www\.)?youtube\.com\/channel\/.*$");
}

function validateNewPassword(givenForm) {
    if (givenForm["new-password-1"].value !== givenForm["new-password-2"].value) {
        errorMessage(givenForm["new-password-1"], "Значения в полях<BR>«Новый пароль»<BR>и<BR>«Новый пароль ещё раз»<BR>не совпадают");
        return false;
    }
    return true;
}


function checkCountry(value) {
    if (checkNotEmpty(value))
        return checkLength(value, 20);
    return true;
}