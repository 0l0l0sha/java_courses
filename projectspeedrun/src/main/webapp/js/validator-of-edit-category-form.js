function validateEditCategory(form) {
	return validateForm(form, [{
		id: "categoryTitle",
		message: "Поле «Название» не заполнено или содержит более 45 символов",
		checker: checkTitle
	}, {
		id: "rules",
		message: "Поле «Правила» должно иметь не больше 65.000 символов",
		checker: checkRulesLength
	} ]);
}

function checkRulesLength(value) {
	if(checkNotEmpty(value))
		return checkLength(value,65000);
	return true;
}
