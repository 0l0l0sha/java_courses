function validateEditGame(form) {
    return validateForm(form, [{
        id: "gameTitle",
        message: "Поле «Название» не заполнено или содержит более 45 символов",
        checker: checkTitle
    }, {
        id: "description",
        message: "Поле «Описание» должно иметь не больше 500 символов",
        checker: checkDescriptionLength
    }, {
        id: "image",
        message: "Поле «Лого» должно быть изображением с расширением .jpg/.jpeg",
        checker: checkLogoExtens
    }])&& checkFirstFileSize(form);

}


function checkDescriptionLength(value) {
    if (checkNotEmpty(value))
        return checkLength(value, 500);
    return true;
}

function checkLogoExtens(value) {

    if (checkNotEmpty(value)) {
        var _validFileExtensions = [".jpg", ".jpeg"];
        /*var ext = value.value.match(/\.([^\.]+)$/)[1];
        switch (ext) {
            case 'jpg':
            case 'jpeg':
                break;
            default:
                return false;
        }*/
        var sFileName = value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() === sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                return false;
            }
        }
        return true;

    }
    return false;
}