function checkEmptyOrPositiveInteger(value) {
    return !checkNotEmpty(value) || checkPositiveInteger(value);
}

function checkPositiveInteger(value) {
    return checkInteger(value) && value > 0;
}

function checkInteger(value) {
    return checkNumber(value) && value === Math.round(value);
}

function checkNumber(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
}

function checkRegexp(value, regexp) {
    return new RegExp(regexp).test(value.toString());
}

function checkLength(value, maxLength) {
    return value.length <= maxLength;
}

function checkNotEmpty(value) {
    return value.length !== 0;
}

function checkTitle(value) {
    if (checkNotEmpty(value))
        return checkLength(value, 45);
    return false;
}

function validateForm(form, data) {
    for (i in data) {
        if (!data[i].checker(form[data[i].id].value)) {
            errorMessage(form[data[i].id], data[i].message);
            return false;
        }
    }
    return true;
}

function errorMessage(element, message) {
    show(message, function () {
        element.focus()
    });
}

function checkFirstFileSize(form) {
    var size = form["image"].files[0].size;
    if (size > 16 * 1024 * 1024) {
        errorMessage(form["image"],"Поле «Image/Logo» должно быть не более 16мб");
        return false;
    }
    return true
}
