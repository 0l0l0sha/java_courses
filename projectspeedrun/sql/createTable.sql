USE `speedrundb`;


CREATE TABLE IF NOT EXISTS `speedrundb`.`runner` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(320) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `pass` VARCHAR(50) NOT NULL,
  `role` TINYINT NULL DEFAULT 0,
  `country` VARCHAR(50) NULL,
  `twitch` VARCHAR(320) NULL,
  `youtube` VARCHAR(320) NULL,
  `image` mediumblob NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `speedrundb`.`game` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gameTitle` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NULL,
  `logo` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
ENGINE = InnoDB DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `speedrundb`.`conjuctionRunnerGame` (
  `runnerId` INT NOT NULL,
  `gameId` INT NOT NULL,
  `isModerator` TINYINT NOT NULL,
  INDEX `userID_idx` (`runnerID` ASC) VISIBLE,
  INDEX `gameID_idx` (`gameID` ASC) VISIBLE,
  CONSTRAINT `conjuctionRunnerId`
    FOREIGN KEY (`runnerId`)
    REFERENCES `speedrundb`.`runner` (`id`)
    ON DELETE CASCADE
    ON UPDATE no action,
  CONSTRAINT `conjuctionGameId`
    FOREIGN KEY (`gameId`)
    REFERENCES `speedrundb`.`game` (`id`)
    ON DELETE CASCADE
    ON UPDATE no action)
ENGINE = InnoDB DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `speedrundb`.`categoryOfRun` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gameId` INT NOT NULL,
  `categoryTitle` VARCHAR(45) NOT NULL,
  `rules` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `gameId_idx` (`gameId` ASC) VISIBLE,
  CONSTRAINT `categoryGameId`
    FOREIGN KEY (`gameId`)
    REFERENCES `speedrundb`.`game` (`id`)
    ON DELETE CASCADE
    ON UPDATE no action)
ENGINE = InnoDB DEFAULT CHARACTER SET utf8;

CREATE TABLE IF NOT EXISTS `speedrundb`.`run` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categoryId` INT NOT NULL,
  `runnerId` INT NOT NULL,
  `realTime` TIME NULL,
  `inGameTime` TIME NULL,
  `date` DATE NOT NULL,
  `video` text NULL,
  `platform` VARCHAR(10) NULL,
  `isSubmited` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `categoryId_idx` (`categoryId` ASC) VISIBLE,
  INDEX `userId_idx` (`runnerId` ASC) VISIBLE,
  CONSTRAINT `runCategoryId`
    FOREIGN KEY (`categoryId`)
    REFERENCES `speedrundb`.`categoryOfRun` (`id`)
    ON DELETE CASCADE
    ON UPDATE no action,
  CONSTRAINT `runRunnerId`
    FOREIGN KEY (`runnerId`)
    REFERENCES `speedrundb`.`runner` (`id`)
    ON DELETE CASCADE
    ON UPDATE no action)
ENGINE = InnoDB DEFAULT CHARACTER SET utf8;
