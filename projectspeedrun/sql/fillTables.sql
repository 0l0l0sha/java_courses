USE `speedrundb`;

insert into runner (email,login,pass,role,country,twitch,youtube,image) values ("sobaka@gmail.com","admin","111",1,"Belarus","lol","kek","lel");

insert into game (gameTitle,description,logo) values ("Warcraft III: Reign of Chaos", "lolechka","/projectspeedrun/themes/wc3/cover-256.png");
insert into game (gameTitle,description,logo) values ("Warcraft 222: Reign of Chaos", "lolechka","/projectspeedrun/themes/wc3/cover-256.png");
insert into categoryOfRun (gameId,categoryTitle) values (2, "Undead Campain");
insert into categoryOfRun (gameId,categoryTitle) values (2, "Human Campain");

insert into runner (email,login, pass, image) values ('e2','JuriRosenkilde', "222", "lel");
insert into runner (email,login, pass,image) values ('e3','username1826', "333", "lel");
insert into runner (email,login, pass,image) values ('e4','Sajiki', "444", "lel");
insert into runner (email,login, pass,image) values ('e5','CovertMuffin', "555", "lel");
insert into runner (email,login, pass,image) values ('e6','MrMister', "666", "lel");
insert into runner (email,login, pass,image) values ('e7','blasterpoard', "777", "lel");

insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 2, '00:39:32', '2017-07-16','v1',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 3, '00:49:18', '2019-06-18','v9',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 3, '00:43:03', '2019-07-19','v2',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 4, '00:44:22', '2017-07-02','v5',1);
insert into run (categoryId,runnerId, realTime, date, video, platform, isSubmited) values (1, 4, '00:43:34', '2017-07-19','v3','PC',0);
insert into run (categoryId,runnerId, realTime, date, video, platform, isSubmited) values (1, 5, '00:44:11', '2019-04-14','v4','PC',0);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 4, '00:47:16', '2016-11-12','v8',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 6, '00:46:17', '2017-06-05','v6',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (1, 4, '00:46:39', '2016-11-21','v7',1);




insert into run (categoryId,runnerId, realTime, inGameTime, date, video, platform, isSubmited) values (2, 4,'00:45:47', '00:43:47', '2019-07-11','v12', 'PC', 1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (2, 7, '00:41:01', '2018-05-23','v10',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (2, 5, '00:45:10', '2018-06-12','v11',1);
insert into run (categoryId,runnerId, realTime, date, video, isSubmited) values (2, 5, '00:46:16', '2018-06-08','v13',1);

insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (2, 2, 0);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (3, 2, 1);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (4, 2, 0);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (5, 2, 1);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (6, 2, 0);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (7, 2, 1);

insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (5, 1, 1);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (6, 1, 0);
insert into conjuctionRunnerGame (runnerId, gameId, isModerator) values (7, 1, 1);
