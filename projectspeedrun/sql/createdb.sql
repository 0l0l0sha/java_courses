CREATE DATABASE `speedrundb` DEFAULT CHARACTER SET UTF8MB4;


GRANT SELECT,INSERT,UPDATE,DELETE
ON `speedrundb`.*
TO 'speedrunUser'@'localhost';

GRANT SELECT,INSERT,UPDATE,DELETE
ON `speedrundb`.*
TO 'speedrunUser'@'%';