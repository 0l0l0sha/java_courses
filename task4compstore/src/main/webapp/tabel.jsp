<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>Result of parsing</title>
    </head>
    <body>
        <form action="index.jsp" method="post" align="center">
            <input type="submit" value="Change parser" name="CP"/>
        </form>
        <h1 align="center">${parserType} parsing results</h1>
        <table border="1" width="80%" align="center">

            <tr align=\"center\">
            <td>Name</td>
            <td>Type</td>
            <td>Origin</td>
            <td>Price</td>
            <td>Powerusage</td>
            <td>Ports</td>
            <td>Coolers</td>
            <td>Peripheral</td>
            <td>Critical</td>
            <td>Date</td>
            <td>Index</td>
            </tr>

            <c:forEach items="${devices}" var="device" varStatus="status">
                <tr align=\"center\">
                    <td align=\"left\"> <c:out value="${device.name}"/></td>
                    <td><c:out value="${device.gameTitle}"/></td>
                </tr>
            </c:forEach>

        </table>
    </body>
</html>