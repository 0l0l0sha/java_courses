<html>
<body>
<h2>Hello World!</h2>
<form action="Tabel" method="post" enctype="multipart/form-data">
    <h2>${errorMessage}</h2>
    <div>
        Select XML file:
        <br>
        <input name="uploadXMLFile" type="file">
        <br>
    </div>
    <br>
    Select XML parser:
    <p><select name="parser" required>
        <option selected value="DOM">DOM</option>
        <option value="SAX">SAX</option>
        <option value="StAX">StAX</option>
    </select></p>
    <p><input type="submit" value="Parse"></p>
</form>
</body>
</html>
