package by.training.task4compstore.servlet;

import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.entity.ParserValue;
import by.training.task4compstore.parser.DeviceXMLparser;
import by.training.task4compstore.validator.ValidatorXML;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;


@MultipartConfig
public class Controller extends HttpServlet {

    private final static String XSD_PATH = "data.xsd";
    private static final Logger LOGGER = LogManager.getLogger();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Hello World!</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Hello World!</h1>");
        out.println("</body>");
        out.println("</html>");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean valid = false;
        PrintWriter out = resp.getWriter();
        Part xmlFile = req.getPart("uploadXMLFile");
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream inputStreamXSD = classloader.getResourceAsStream(XSD_PATH);
            valid = ValidatorXML.validateAgainstXSD(xmlFile.getInputStream(), inputStreamXSD);
        } catch (FileNotFoundException e) {
            out.println("Your XSD not in resources");
        }
        if (valid) {
            //TODO можно попробовать через Part
            String select = req.getParameter("parser");
            ParserValue parserValue = ParserValue.valueOf(select);
            Devices devices = DeviceXMLparser.parseXML(parserValue, xmlFile.getInputStream());
            req.setAttribute("devices", devices.getDevices());
            req.getRequestDispatcher("/tabel.jsp").forward(req, resp);
        } else {
            out.println("You forgot to upload XML or It's not Valid");
        }
    }
}
