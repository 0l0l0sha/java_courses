package by.training.task4compstore.parser;

import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.entity.ParserValue;
import by.training.task4compstore.parser.dom.DevicesDOMbuilder;
import by.training.task4compstore.parser.sax.DevicesSAXbuilder;
import by.training.task4compstore.parser.stax.DevicesStAXbuilder;

import java.io.InputStream;

public class DeviceXMLparser {
    public static Devices parseXML(ParserValue parserValue, InputStream fileInputStream) {
        DevicesBuilder devicesBuilder;
        switch (parserValue) {
            case DOM:
                devicesBuilder = new DevicesDOMbuilder();
                return devicesBuilder.buildDevices(fileInputStream);
            case SAX:
                devicesBuilder = new DevicesSAXbuilder();
                return devicesBuilder.buildDevices(fileInputStream);
            case StAX:
                devicesBuilder = new DevicesStAXbuilder();
                return devicesBuilder.buildDevices(fileInputStream);
            default:
                return new Devices();
        }
    }
}
