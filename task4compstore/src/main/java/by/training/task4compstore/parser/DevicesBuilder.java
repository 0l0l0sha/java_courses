package by.training.task4compstore.parser;

import by.training.task4compstore.entity.Devices;

import java.io.InputStream;

public abstract class  DevicesBuilder {
    public abstract Devices buildDevices(InputStream fileInputStream);
}
