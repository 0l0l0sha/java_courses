package by.training.task4compstore.parser.dom;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


import by.training.task4compstore.entity.Cooler;
import by.training.task4compstore.entity.Coolers;
import by.training.task4compstore.entity.Device;
import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.entity.Devicetype;
import by.training.task4compstore.entity.Port;
import by.training.task4compstore.entity.Ports;
import by.training.task4compstore.entity.Portvalue;
import by.training.task4compstore.parser.DevicesBuilder;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DevicesDOMbuilder extends DevicesBuilder {

    private DocumentBuilder docBuilder;

    public DevicesDOMbuilder() {
        // создание DOM-анализатора
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            docBuilder = factory.newDocumentBuilder();

        } catch (ParserConfigurationException e) {

            System.err.println("Ошибка конфигурации парсера: " + e);

        }
    }


    public Devices buildDevices(InputStream fileInputStream) {
        Devices devices = new Devices();
        Document doc = null;
        try {
// parsing XML-документа и создание древовидной структуры
            doc = docBuilder.parse(fileInputStream);
            Element root = doc.getDocumentElement();
// получение списка дочерних элементов <student>
            NodeList devicesList = root.getElementsByTagName("device");
            for (int i = 0; i < devicesList.getLength(); i++) {
                Element deviceElement = (Element) devicesList.item(i);
                Device device = buildDevice(deviceElement);
                devices.addDevice(device);
            }
        } catch (IOException e) {

            System.err.println("File error or I/O error: " + e);

        } catch (SAXException e) {

            System.err.println("Parsing failure: " + e);

        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return devices;
    }

    private Device buildDevice(Element deviceElement) {
        Device device = new Device();
// заполнение объекта student

        device.setOrigin(deviceElement.getAttribute("origin"));
        device.setCode(deviceElement.getAttribute("code"));
        Devicetype devicetype = Devicetype.valueOf(deviceElement.getAttribute("type").toUpperCase());
        device.setType(devicetype);
        String perCheck = deviceElement.getAttribute("peripheral");
        Boolean peripheral = false;
        if (perCheck != null) {
            peripheral = Boolean.parseBoolean(perCheck);
        }
        device.setPeripheral(peripheral);
        Boolean critical = Boolean.parseBoolean(deviceElement.getAttribute("critical"));
        device.setCritical(critical);
        XMLGregorianCalendar incomingDate = XMLGregorianCalendarImpl.parse(deviceElement.getAttribute("incomingdate"));
        device.setIncomingdate(incomingDate);

        device.setName(getElementTextContent(deviceElement, "name"));
        BigDecimal price = new BigDecimal(getElementTextContent(deviceElement, "price"));
        device.setPrice(price);
        double powerusage = Double.parseDouble(getElementTextContent(deviceElement, "powerusage"));
        device.setPowerusage(powerusage);

        Ports ports = new Ports();
        Element portsElement = (Element) deviceElement.getElementsByTagName("ports").item(0);
        for (int i = 0; i < portsElement.getElementsByTagName("port").getLength(); i++) {
            Element portElement = (Element) portsElement.getElementsByTagName("port").item(i);
            Port port = new Port();
            Portvalue portvalue = Portvalue.valueOf(portElement.getAttribute("value").toUpperCase());
            port.setValue(portvalue);
            int count = Integer.parseInt(portElement.getAttribute("count"));
            port.setCount(count);
            ports.addPort(port);
        }
        device.setPorts(ports);

        Coolers coolers = new Coolers();
        Element coolersElement = (Element) deviceElement.getElementsByTagName("coolers").item(0);
        NodeList list = coolersElement.getElementsByTagName("cooler");
        if (list.getLength() > 0) {
            Element coolerElement = (Element) list.item(0);
            Cooler cooler = new Cooler();
            cooler.setCount(Integer.parseInt(coolerElement.getAttribute("count")));
            coolers.setCooler(cooler);
        }
        list = coolersElement.getElementsByTagName("noncooler");
        if (list.getLength() > 0) {
            Element nonCoolerElement = (Element) list.item(0);
            coolers.setNoncooler("");
        }
        device.setCoolers(coolers);
        return device;
    }

    // получение текстового содержимого тега
    private static String getElementTextContent(Element element, String elementName) {
        NodeList nList = element.getElementsByTagName(elementName);
        Node node = nList.item(0);
        String text = node.getTextContent();
        return text;
    }
}


