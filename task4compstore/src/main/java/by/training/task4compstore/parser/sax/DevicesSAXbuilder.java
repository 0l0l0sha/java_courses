package by.training.task4compstore.parser.sax;

import java.io.IOException;
import java.io.InputStream;

import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.parser.DevicesBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class DevicesSAXbuilder extends DevicesBuilder {
    private DevicesHandler sh;
    private XMLReader reader;

    public DevicesSAXbuilder() {
// создание SAX-анализатора
        sh = new DevicesHandler();
        try {
// создание объекта-обработчика
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(sh);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        }
    }

    public Devices buildDevices(InputStream fileInputStream) {
        try {
// разбор XML-документа
            InputSource inputSource = new InputSource(fileInputStream);
            reader.parse(inputSource);
        } catch (SAXException e) {
            System.err.print("ошибка SAX парсера: " + e);
        } catch (IOException e) {
            System.err.print("ошибка I/О потока: " + e);
        }
        return sh.getDevices();
    }
}


