package by.training.task4compstore.parser.stax;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.training.task4compstore.entity.Cooler;
import by.training.task4compstore.entity.Coolers;
import by.training.task4compstore.entity.Device;
import by.training.task4compstore.entity.DeviceEnum;
import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.entity.Devicetype;
import by.training.task4compstore.entity.Port;
import by.training.task4compstore.entity.Ports;
import by.training.task4compstore.entity.Portvalue;
import by.training.task4compstore.parser.DevicesBuilder;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

public class DevicesStAXbuilder extends DevicesBuilder {
    private Devices devices = new Devices();
    private XMLInputFactory inputFactory;

    public DevicesStAXbuilder() {
        inputFactory = XMLInputFactory.newInstance();
    }

    public Devices buildDevices(InputStream fileInputStream) {
        XMLStreamReader reader = null;
        String name;
        try {
            reader = inputFactory.createXMLStreamReader(fileInputStream);
// StAX parsing
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.DEVICE) {
                        Device device = buildDevice(reader);
                        devices.addDevice(device);
                    }
                }
            }
        } catch (XMLStreamException ex) {
            System.err.println("StAX parsing error! " + ex.getMessage());
        } finally {
            try {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                System.err.println("Impossible close file " + fileInputStream + " : " + e);
            }
        }
        return devices;
    }

    private Device buildDevice(XMLStreamReader reader) throws XMLStreamException {
        Device device = new Device();
        device.setOrigin(reader.getAttributeValue(null, DeviceEnum.ORIGIN.getValue()));
        //TODO проверки на null
        device.setCode(reader.getAttributeValue(null, DeviceEnum.CODE.getValue()));
        device.setType(Devicetype.valueOf(reader.getAttributeValue(null, DeviceEnum.TYPE.getValue()).toUpperCase()));
        device.setPeripheral(Boolean.parseBoolean(reader.getAttributeValue(null, DeviceEnum.PERIPHERAL.getValue())));
        device.setCritical(Boolean.parseBoolean(reader.getAttributeValue(null, DeviceEnum.CRITICAL.getValue())));
        device.setIncomingdate(XMLGregorianCalendarImpl.parse(reader.getAttributeValue(null, DeviceEnum.INCOMINGDATE.getValue())));
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DeviceEnum.valueOf(name.toUpperCase())) {
                        case NAME:
                            device.setName(getXMLText(reader));
                            break;
                        case PRICE:
                            name = getXMLText(reader);
                            device.setPrice(new BigDecimal(name));
                            break;
                        case POWERUSAGE:
                            name = getXMLText(reader);
                            device.setPowerusage(Double.parseDouble(name));
                            break;
                        case PORTS:
                            device.setPorts(getXMLPorts(reader));
                            break;
                        case COOLERS:
                            device.setCoolers(getXMLCoolers(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.DEVICE) {
                        return device;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Device");
    }

    private Ports getXMLPorts(XMLStreamReader reader) throws XMLStreamException {
        Ports ports = new Ports();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.PORT) {
                        ports.addPort(getXMLPort(reader));
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.PORTS) {
                        return ports;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Ports");
    }

    private Port getXMLPort(XMLStreamReader reader) {
        Port port = new Port();
        port.setValue(Portvalue.valueOf(reader.getAttributeValue(null, DeviceEnum.VALUE.getValue()).toUpperCase()));
        port.setCount(Integer.valueOf(reader.getAttributeValue(null, DeviceEnum.COUNT.getValue())));
        return port;
    }

    private Coolers getXMLCoolers(XMLStreamReader reader) throws XMLStreamException {
        Coolers coolers = new Coolers();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DeviceEnum.valueOf(name.toUpperCase())) {
                        case COOLER:
                            coolers.setCooler(getXMLCooler(reader));
                            break;
                        case NONCOOLER:
                            coolers.setNoncooler("");
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.COOLERS) {
                        return coolers;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag Coolers");
    }

    private Cooler getXMLCooler(XMLStreamReader reader) {
        Cooler cooler = new Cooler();
        cooler.setCount(Integer.valueOf(reader.getAttributeValue(null, DeviceEnum.COUNT.getValue())));
        return cooler;
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}

