package by.training.task4compstore.parser.sax;


import java.math.BigDecimal;
import java.util.EnumSet;

import by.training.task4compstore.entity.Cooler;
import by.training.task4compstore.entity.Coolers;
import by.training.task4compstore.entity.Device;
import by.training.task4compstore.entity.DeviceEnum;
import by.training.task4compstore.entity.Devices;
import by.training.task4compstore.entity.Devicetype;
import by.training.task4compstore.entity.Port;
import by.training.task4compstore.entity.Ports;
import by.training.task4compstore.entity.Portvalue;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.datatype.XMLGregorianCalendar;

public class DevicesHandler extends DefaultHandler {
    private Devices devices;
    private Device current = null;
    private DeviceEnum currentEnum = null;
    private EnumSet<DeviceEnum> withText;

    public DevicesHandler() {
        devices = new Devices();
        withText = EnumSet.range(DeviceEnum.NAME, DeviceEnum.POWERUSAGE);
    }

    public Devices getDevices() {
        return devices;
    }

    public void startElement(String uri, String localName, String qName, Attributes attrs) {
        switch (localName) {
            case "device":
                current = new Device();
                current.setOrigin(attrs.getValue("origin"));
                current.setCode(attrs.getValue("code"));
                Devicetype devicetype = Devicetype.valueOf(attrs.getValue("type").toUpperCase());
                current.setType(devicetype);
                String perCheck = attrs.getValue("peripheral");
                Boolean peripheral = false;
                if (perCheck != null) {
                    peripheral = Boolean.parseBoolean(perCheck);
                }
                current.setPeripheral(peripheral);
                Boolean critical = Boolean.parseBoolean(attrs.getValue("critical"));
                current.setCritical(critical);
                XMLGregorianCalendar incomingDate = XMLGregorianCalendarImpl.parse(attrs.getValue("incomingdate"));
                current.setIncomingdate(incomingDate);
                break;
            case "ports":
                current.setPorts(new Ports());
                break;
            case "coolers":
                current.setCoolers(new Coolers());
                break;
            case "port":
                Port port = new Port();
                port.setCount(Integer.parseInt(attrs.getValue("count")));
                port.setValue(Portvalue.valueOf(attrs.getValue("value").toUpperCase()));
                current.getPorts().addPort(port);
                break;
            case "cooler":
                Cooler cooler = new Cooler();
                cooler.setCount(Integer.parseInt(attrs.getValue("count")));
                current.getCoolers().setCooler(cooler);
                break;
            case "noncooler":
                current.getCoolers().setNoncooler("");
                break;
            default:
                DeviceEnum temp = DeviceEnum.valueOf(localName.toUpperCase());
                if (withText.contains(temp)) {
                    currentEnum = temp;
                }
                break;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if ("device".equals(localName)) {
            devices.addDevice(current);
        }
    }

    public void characters(char[] ch, int start, int length) {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case NAME:
                    current.setName(s);
                    break;
                case PRICE:
                    current.setPrice(new BigDecimal(s));
                    break;
                case POWERUSAGE:
                    current.setPowerusage(Double.parseDouble(s));
                    break;
                default:
                    throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
            }
        }
        currentEnum = null;
    }
}

