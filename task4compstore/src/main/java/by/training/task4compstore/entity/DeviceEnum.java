package by.training.task4compstore.entity;

public enum DeviceEnum {

    DEVICES("devices"),
    ORIGIN("origin"),
    CODE("code"),
    TYPE("type"),
    PERIPHERAL("peripheral"),
    CRITICAL("critical"),
    INCOMINGDATE("incomingdate"),
    VALUE("value"),
    COUNT("count"),
    DEVICE("device"),
    NAME("name"),
    PRICE("price"),
    POWERUSAGE("powerusage"),
    PORT("port"),
    COOLER("cooler"),
    NONCOOLER("noncooler"),
    PORTS("ports"),
    COOLERS("coolers");

    private String value;

    private DeviceEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}