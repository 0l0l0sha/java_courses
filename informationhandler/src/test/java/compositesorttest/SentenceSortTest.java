package compositesorttest;

import by.training.informationhandler.composite.Paragraph;
import by.training.informationhandler.composite.Sentence;
import by.training.informationhandler.composite.Symbol;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.ParagraphParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SentenceSortTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str;
        ParagraphParser paragraphParser = new ParagraphParser();
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        paragraphParser.setNextParser(sentenceParser);
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        str = "Testing can be easy. You need only 1 thing -\n good structure in ur code!!!";
        TextComposite paragraph = paragraphParser.build(str);
        str = "You need only 1 thing -\n good structure in ur code!!! Testing can be easy.";
        TextComposite checkParagraph = paragraphParser.build(str);
        Symbol s1 = new Symbol('e');
        Symbol s2 = new Symbol('y');
        Symbol s3 = new Symbol('a');
        Symbol s4 = new Symbol('.');
        Symbol s5 = new Symbol('!');
        return new Object[][]{
                {paragraph, s1, paragraph},
                {paragraph, s2, paragraph},
                {paragraph, s3, checkParagraph},
                {paragraph, s4, checkParagraph},
                {paragraph, s5, paragraph},};
    }

    @Test(description = "Sorting sentences by quantity of required symbol", dataProvider = "correctTest")
    public void senSortTest(TextComposite actual, Symbol s, TextComposite expected) {
        Sentence.setSortSymbol(s);
        actual.sort();
        Assert.assertEquals(actual, expected);
    }
}
