package compositesorttest;

import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.ParagraphParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.TextParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ParagraphSortTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str;
        TextParser textParser = new TextParser();
        ParagraphParser paragraphParser = new ParagraphParser();
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        textParser.setNextParser(paragraphParser);
        paragraphParser.setNextParser(sentenceParser);
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);

        String src = "    It has survived not only five centuries, but also the leap into electronic "
                + "typesetting, remaining essentially unchanged. Kek. cHECK. It was popularised in the with the "
                + "release of Letraset sheets containing Lorem Ipsum passages, and more recently with "
                + "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. "
                + "    It is a long established fact that a reader will be distracted by the readable "
                + "content of a page when looking at its layout. The point of using Ipsum is that it has a "
                + "more-or-less normal distribution of letters, as opposed to using 'Content here, content "
                + "here', making it look like readable English. sENTENCE. "
                + "    It is a established fact that a reader will be of a page when looking at its "
                + "layout. LOL."
                + "    Bye. ";
        TextComposite text = textParser.build(src);
        src =   "    Bye. "
                + "    It is a established fact that a reader will be of a page when looking at its "
                + "layout. LOL. "
                + "    It is a long established fact that a reader will be distracted by the readable "
                + "content of a page when looking at its layout. The point of using Ipsum is that it has a "
                + "more-or-less normal distribution of letters, as opposed to using 'Content here, content "
                + "here', making it look like readable English. sENTENCE. "
                + "    It has survived not only five centuries, but also the leap into electronic "
                + "typesetting, remaining essentially unchanged. Kek. cHECK. It was popularised in the with the "
                + "release of Letraset sheets containing Lorem Ipsum passages, and more recently with "
                + "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. ";
        TextComposite checText = textParser.build(src);
        return new Object[][]{
                {text,checText}
        };
    }
    @Test(description = "Sorting paragraphs by quantity of sentences", dataProvider = "correctTest")
    public void parSortTest(TextComposite actual, TextComposite expected) {
        actual.sort();
        Assert.assertEquals(actual, expected);
    }
}
