package compositesorttest;

import by.training.informationhandler.composite.Paragraph;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.ParagraphParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WordSortTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str;
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        str = "U need only 1 thing - good structure in ur code!!!";
        TextComposite sen = sentenceParser.build(str);
        str = "U 1 in ur good - only need code!!! thing structure";
        TextComposite checkSen = sentenceParser.build(str);
        return new Object[][]{
                {sen,checkSen}
        };
    }

    @Test(description = "Sorting lexemes by length of it's words (ignore pure marks like '-' ", dataProvider = "correctTest")
    public void createParagraphTest(TextComposite actual, TextComposite expected) {
        actual.sort();
        Assert.assertEquals(actual, expected);
    }
}
