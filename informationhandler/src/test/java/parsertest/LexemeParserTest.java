package parsertest;

import by.training.informationhandler.composite.Lexeme;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LexemeParserTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str = "";
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        wordParser.setNextParser(symbolParser);
        Lexeme lex1 = new Lexeme();
        str = "kek";
        TextComposite word;
        TextComposite mark;
        word = wordParser.build(str);
        lex1.add(word);
        Lexeme lex2 = new Lexeme();
        str = "lol";
        word = wordParser.build(str);
        str = "!";
        mark = wordParser.build(str);
        lex2.add(word);
        lex2.add(mark);
        Lexeme lex3 = new Lexeme();
        str = "something";
        word = wordParser.build(str);
        str = "...";
        mark = wordParser.build(str);
        lex3.add(word);
        lex3.add(mark);
        Lexeme lex4 = new Lexeme();
        str = "-";
        mark = wordParser.build(str);
        lex4.add(mark);
        return new Object[][]{
                {"kek",lex1},
                {"lol!",lex2},
                {"something...", lex3},
                {"-", lex4},
        };
    }

    @Test(description = "Lexeme Parse on word and mark", dataProvider = "correctTest")
    public void createLexemeTest(String str, TextComposite expected) {
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        TextComposite actual = lexemeParser.build(str);
        Assert.assertEquals(actual, expected);
    }
}
