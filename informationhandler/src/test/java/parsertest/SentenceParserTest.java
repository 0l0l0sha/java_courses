package parsertest;

import by.training.informationhandler.composite.Sentence;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SentenceParserTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str;
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        Sentence sen1 = new Sentence();
        TextComposite lex;
        str = "kek";
        lex = lexemeParser.build(str);
        sen1.add(lex);
        str = "lol,";
        lex = lexemeParser.build(str);
        sen1.add(lex);
        str = "something";
        lex = lexemeParser.build(str);
        sen1.add(lex);
        str = "-";
        lex = lexemeParser.build(str);
        sen1.add(lex);
        str = "everything...";
        lex = lexemeParser.build(str);
        sen1.add(lex);
        return new Object[][]{
                {"kek lol, something - everything...",sen1},
                {"kek lol,   something - everything...",sen1},
                {"kek lol,\n something - everything...",sen1},
                {"kek lol, something -\n everything...",sen1},
                {"\tkek lol,\n something -\teverything...",sen1},
        };
    }

    @Test(description = "Sentence Parse on lexemes", dataProvider = "correctTest")
    public void createSentenceTest(String str, TextComposite expected) {
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        TextComposite actual = sentenceParser.build(str);
        Assert.assertEquals(actual, expected);
    }
}
