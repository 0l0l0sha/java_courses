package parsertest;


import by.training.informationhandler.composite.Text;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.ParagraphParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.TextParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TextParserTest {
        @DataProvider(name = "correctTest")
        public static Object[][] correctData() {
            String str;
            ParagraphParser paragraphParser = new ParagraphParser();
            SentenceParser sentenceParser = new SentenceParser();
            LexemeParser lexemeParser = new LexemeParser();
            WordParser wordParser = new WordParser();
            SymbolParser symbolParser = new SymbolParser();
            paragraphParser.setNextParser(sentenceParser);
            sentenceParser.setNextParser(lexemeParser);
            lexemeParser.setNextParser(wordParser);
            wordParser.setNextParser(symbolParser);
            Text text = new Text();
            TextComposite par;
            str = "It has survived not only five centuries, but also the leap into electronic "
                    + "typesetting, remaining essentially unchanged. It was popularised in the with the "
                    + "release of Letraset sheets containing Lorem Ipsum passages, and more recently with "
                    + "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
            par = paragraphParser.build(str);
            text.add(par);
            str = "It is a long established fact that a reader will be distracted by the readable "
                    + "content of a page when looking at its layout. The point of using Ipsum is that it has a "
                    + "more-or-less normal distribution of letters, as opposed to using 'Content here, content "
                    + "here', making it look like readable English.";
            par = paragraphParser.build(str);
            text.add(par);
            str = "It is a established fact that a reader will be of a page when looking at its "
                    + "layout.";
            par = paragraphParser.build(str);
            text.add(par);
            str = "Bye.";
            par = paragraphParser.build(str);
            text.add(par);
            String src = "    It has survived not only five centuries, but also the leap into electronic "
                    + "typesetting, remaining essentially unchanged. It was popularised in the with the "
                    + "release of Letraset sheets containing Lorem Ipsum passages, and more recently with "
                    + "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. "
                    + "    It is a long established fact that a reader will be distracted by the readable "
                    + "content of a page when looking at its layout. The point of using Ipsum is that it has a "
                    + "more-or-less normal distribution of letters, as opposed to using 'Content here, content "
                    + "here', making it look like readable English. "
                    + "    It is a established fact that a reader will be of a page when looking at its "
                    + "layout. "
                    + "    Bye. ";
            return new Object[][]{
                    {src,text}
            };
        }

        @Test(description = "Check on correct text building from string using parser",  dataProvider = "correctTest")
        public void createTextTest(String str, TextComposite expected) {
            TextParser textParser = new TextParser();
            ParagraphParser paragraphParser = new ParagraphParser();
            SentenceParser sentenceParser = new SentenceParser();
            LexemeParser lexemeParser = new LexemeParser();
            WordParser wordParser = new WordParser();
            SymbolParser symbolParser = new SymbolParser();
            textParser.setNextParser(paragraphParser);
            paragraphParser.setNextParser(sentenceParser);
            sentenceParser.setNextParser(lexemeParser);
            lexemeParser.setNextParser(wordParser);
            wordParser.setNextParser(symbolParser);
            TextComposite actual = textParser.build(str);
            Assert.assertEquals(actual, expected);
        }
}
