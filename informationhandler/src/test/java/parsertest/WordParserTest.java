package parsertest;

import by.training.informationhandler.composite.Mark;
import by.training.informationhandler.composite.Symbol;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.composite.Word;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class WordParserTest {

    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Word kek = new Word();
        Symbol k = new Symbol('k');
        Symbol e = new Symbol('e');
        Symbol k2 = new Symbol('k');
        kek.add(k);
        kek.add(e);
        kek.add(k2);
        Mark excMark = new Mark();
        Symbol exc = new Symbol('!');
        excMark.add(exc);
        Mark ellipsis = new Mark();
        Symbol dot1 = new Symbol('.');
        Symbol dot2 = new Symbol('.');
        Symbol dot3 = new Symbol('.');
        ellipsis.add(dot1);
        ellipsis.add(dot2);
        ellipsis.add(dot3);
        return new Object[][]{
                {"kek",kek},
                {"!",excMark},
                {"...", ellipsis}
        };
    }

    @Test(description = "Word Parse on symbols", dataProvider = "correctTest")
    public void createWordTest(String str, TextComposite expected) {
        WordParser parser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        parser.setNextParser(symbolParser);
        TextComposite actual = parser.build(str);
        Assert.assertEquals(actual, expected);
    }
}
