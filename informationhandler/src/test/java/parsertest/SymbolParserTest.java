package parsertest;

import by.training.informationhandler.composite.Symbol;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.SymbolParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SymbolParserTest {

    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Symbol l = new Symbol('l');
        Symbol five = new Symbol('5');
        Symbol dot = new Symbol('.');
        return new Object[][]{
                {"l",l},
                {"5",five},
                {".", dot}
        };
    }

    @Test(description = "Creating symbol wrapper to required symbol", dataProvider = "correctTest")
    public void createSymbolTest(String str, Symbol expected) {
        SymbolParser parser = new SymbolParser();
        TextComposite actual = parser.build(str);
        Assert.assertEquals(actual, expected);
    }
}
