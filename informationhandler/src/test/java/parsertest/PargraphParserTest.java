package parsertest;

import by.training.informationhandler.composite.Paragraph;
import by.training.informationhandler.composite.Sentence;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.parser.LexemeParser;
import by.training.informationhandler.parser.ParagraphParser;
import by.training.informationhandler.parser.SentenceParser;
import by.training.informationhandler.parser.SymbolParser;
import by.training.informationhandler.parser.WordParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PargraphParserTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        String str;
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        Paragraph paragraph = new Paragraph();
        TextComposite sen;
        str = "Testing can be easy.";
        sen = sentenceParser.build(str);
        paragraph.add(sen);
        str = "U need only 1 thing - good structure in ur code!!!";
        sen = sentenceParser.build(str);
        paragraph.add(sen);
        return new Object[][]{
                {"Testing can be easy. U need only 1 thing -\n good structure in ur code!!!",paragraph}
        };
    }

    @Test(description = "Paragraph Parse on sentences", dataProvider = "correctTest")
    public void createParagraphTest(String str, TextComposite expected) {
        ParagraphParser paragraphParser = new ParagraphParser();
        SentenceParser sentenceParser = new SentenceParser();
        LexemeParser lexemeParser = new LexemeParser();
        WordParser wordParser = new WordParser();
        SymbolParser symbolParser = new SymbolParser();
        paragraphParser.setNextParser(sentenceParser);
        sentenceParser.setNextParser(lexemeParser);
        lexemeParser.setNextParser(wordParser);
        wordParser.setNextParser(symbolParser);
        TextComposite actual = paragraphParser.build(str);
        Assert.assertEquals(actual, expected);
    }
}
