package by.training.informationhandler.composite;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract class described Any of text composites except for leaf (symbol)
 */
public abstract class Node implements TextComposite {
    /**
     * List of child composites.
     */
    protected List<TextComposite> list = new LinkedList<>();

    /**
     * Collections sort.
     */
    @Override
    public void sort() {
        Collections.sort(list);
    }

    /**
     * add new item in a list.
     * @param item child TextComposite.
     */
    @Override
    public void add(TextComposite item) {
        list.add(item);
    }

    /**
     * Get param quantity (need for sentence sort).
     * @param symbol for search
     * @return int quantity of it in this TextComposite
     */
    @Override
    public
    int getSymbolQuantity(Symbol symbol) {
        int summ = 0;
        for (TextComposite item: list) {
            summ+= item.getSymbolQuantity(symbol);
        }
        return summ;
    }

    /**
     * Text Composite matching
     * @param o object to matching
     * @return true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return list != null ? list.equals(node.list) : node.list == null;
    }

    /**
     * @return int hash code.
     */
    @Override
    public int hashCode() {
        return list != null ? list.hashCode() : 0;
    }

    /**
     * @return length of that composite
     */
    public int getLength(){
        return list.size();
    }
}
