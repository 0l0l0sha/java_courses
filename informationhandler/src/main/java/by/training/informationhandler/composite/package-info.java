/**
 * Text Composite objects which are nodes and leaf (symbol)
 * Provides tree based structure to store text and sort it's parts.
 *
 * @since 1.0
 * @author Ololosha
 * @version 1.1
 */
package by.training.informationhandler.composite;