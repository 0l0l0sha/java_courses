package by.training.informationhandler.composite;

/**
 * Class described
 * how to compare words
 * and how to make String interpretation.
 */
public class Word extends Node
{
    /**
     * Method for comparing Lexemes based on their length.
     * @param o - object to compare
     * @return - smth >0 if this word bigger,
     *                <0 if o bigger,
     *                0 if Length is equal or o isn't a word
     */
    @Override
    public int compareTo(TextComposite o)
    {
        if (o instanceof Word){
            return this.list.size() - ((Word) o).list.size();
        }
        return 0;
    }

    /**
     * @return String interpretation of this word.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (TextComposite item: list) {
            result.append(item.toString());
        }
        return result.toString();
    }
}
