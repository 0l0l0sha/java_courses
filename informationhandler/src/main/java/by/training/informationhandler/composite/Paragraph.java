package by.training.informationhandler.composite;


/**
 * Class described
 * how to compare paragraphs
 * and how to make String interpretation.
 */
public class Paragraph extends Node
{
    /**
     * Method for comparing Paragraph based on their length (sentence quantity).
     * @param o - object to compare
     * @return - smth >0 if this lexeme bigger,
     *                <0 if o bigger,
     *                0 if Length is equal or o isn't a Paragraph
     */
    @Override
    public int compareTo(TextComposite o)
    {
        if(o instanceof Paragraph) {
            return this.list.size() - ((Paragraph)o).list.size();
        }
        return 0;
    }

    /**
     * @return String interpretation of this Paragraph
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (TextComposite item: list) {
            result.append(item.toString());
            result.append(" ");
        }
        return result.toString();
    }

}
