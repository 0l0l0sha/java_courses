package by.training.informationhandler.composite;


/**
 * Class described
 * how to compare lexemes
 * how to take its length
 * and how to make String interpretation.
 */
public class Lexeme extends Node {
    /**
     * Method for comparing Lexemes based on their length.
     * @see Lexeme#getLength()
     * @param o - object to compare
     * @return - smth >0 if this lexeme bigger,
     *                <0 if o bigger,
     *                0 if Length is equal or o isn't a Lexeme
     */
    @Override
    public int compareTo(final TextComposite o) {
        if (o instanceof Lexeme) {
            return this.getLength() - o.getLength();
        }
        return 0;
    }

    /**
     * @return String interpretation of this Lexeme
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (TextComposite item : list) {
            result.append(item.toString());
        }
        return result.toString();
    }

    /**
     * @return Length of the First Item in Lexeme
     * (Word's if it is, or Mark's (0) if it isn't)
     * @see Mark#getLength()
     */
    @Override
    public int getLength() {
        return list.get(0).getLength();
    }
}
