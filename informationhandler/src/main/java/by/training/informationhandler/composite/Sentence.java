package by.training.informationhandler.composite;

/**
 * Class described
 * how to compare sentences
 * how to take its length
 * how to sort Lexemes in sentence
 * and how to make String interpretation.
 */
public class Sentence extends Node {
    /**
     * Symbol by which sentences sorting in paragraph.
     */
    private static Symbol sortSymbol;

    /**
     * Method for comparing Sentences based on the number of the given symbol.
     * @see Sentence#getSymbolQuantity(Symbol)
     * @param o - object to compare
     * @return - smth >0 if this sentence have more of that symbol than o,
     *                <0 if less,
     *                0 if quantity is equal or o isn't a sentence
     */
    @Override
    public int compareTo(TextComposite o) {
        if (o instanceof Sentence) {
            return this.getSymbolQuantity(sortSymbol)
                    - o.getSymbolQuantity(sortSymbol);
        }
        return 0;
    }

    /**
     * Selection sort of lexemes in sentence based on their word length.
     * Ignores Lexemes with word length 0
     * (consisting only of a punctuation mark).
     */
    @Override
    public void sort() {
        int min;
        for (int index = 0; index < list.size() - 1; index++) {
            if (list.get(index).getLength() != 0) {
                min = index;
                TextComposite minItem = list.get(min);
                for (int scan = index + 1; scan < list.size(); scan++) {
                    TextComposite temp = list.get(scan);
                    if (temp.getLength() != 0 && temp.compareTo(minItem) < 0) {
                        min = scan;
                        minItem = list.get(min);
                    }
                }
                // Swap the values
                if (min != index) {
                    TextComposite temp = list.get(min);
                    list.remove(min);
                    list.add(min, list.get(index));
                    list.remove(index);
                    list.add(index, temp);
                }
            }
        }
    }

    /**
     * @return String interpretation of this Sentence
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (TextComposite item : list) {
            result.append(item.toString());
            result.append(" ");
        }
        return result.toString();
    }

    /**
     * @return Symbol for sorting.
     * @see Sentence#sortSymbol
     */
    public static Symbol getSortSymbol() {
        return sortSymbol;
    }

    /**
     * set sortSymbol.
     * @param sortSymbol - new Symbol for sorting.
     * @see Sentence#sortSymbol
     */
    public static void setSortSymbol(final Symbol sortSymbol) {
        Sentence.sortSymbol = sortSymbol;
    }
}
