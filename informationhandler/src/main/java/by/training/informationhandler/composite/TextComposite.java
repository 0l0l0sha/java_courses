package by.training.informationhandler.composite;

/**
 * Text composite interface with declaration of needed methods
 */
public interface TextComposite extends Comparable<TextComposite> {
    void sort();

    void add(TextComposite items);

    int getSymbolQuantity(Symbol symbol);

    int getLength();
}
