package by.training.informationhandler.composite;

/**
 * Class described leaf of Text Composite - Symbol.
 */
public class Symbol implements TextComposite {
    /**
     * Actual symbol.
     */
    private char symbol;

    /**
     * Constructor with param.
     * @param symbol - char, actual symbol.
     */
    public Symbol(final char symbol) {
        this.symbol = symbol;
    }

    /**
     * @return actual symbol.
     */
    public char getSymbol() {
        return symbol;
    }

    /**
     * text composite stub.
     */
    public void sort() {
    }

    /**
     * text composite stub.
     */
    @Override
    public void add(final TextComposite items) {

    }

    /**
     * check on equals (ignoring case) with param.
     * @param symbol to compare
     * @return 1 if equal, 0 if not
     */
    @Override
    public int getSymbolQuantity(final Symbol symbol) {
        String src = String.valueOf(this.symbol);
        String item = String.valueOf(symbol.symbol);
        if (src.equalsIgnoreCase(item)) {
            return 1;
        }
        return 0;
    }

    /**
     * @return 1 - for sum in nearest composite node.
     */
    @Override
    public int getLength() {
        return 1;
    }

    /**
     * text composite stub.
     */
    @Override
    public int compareTo(final TextComposite o) {
        return 0;
    }

    /**
     * @return String interpretation of this Symbol.
     */
    @Override
    public String toString() {
        return String.valueOf(symbol);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Symbol symbol1 = (Symbol) o;

        return symbol == symbol1.symbol;
    }

    @Override
    public int hashCode() {
        return (int) symbol;
    }
}
