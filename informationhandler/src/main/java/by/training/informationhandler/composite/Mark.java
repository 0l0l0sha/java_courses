package by.training.informationhandler.composite;

/**
 * Class described how to make String interpretation.
 * It's have stubs for comparing and Length.
 */
public class Mark extends Node {
    /**
     * Stub for comparing method.
     * @param o - object to compare
     * @return 0 - because it is stub
     */
    @Override
    public int compareTo(TextComposite o) {
        return 0;
    }

    /**
     * @return String interpretation of this Mark
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (TextComposite item: list) {
            result.append(item.toString());
        }
        return result.toString();
    }

    /**
     * Method Length is need to check length of word in lexeme.
     * If there isn't a word in a lexeme, it's calling marks method.
     * So this method returns 0.
     * @return 0 - the actual word Length.
     */
    @Override
    public int getLength()
    {
        return 0;
    }
}
