package by.training.informationhandler.composite;



public class Text extends Node
{
    /**
     * Stub for comparing method.
     * @param o - object to compare
     * @return 0 - because it is stub
     */
    @Override
    public int compareTo(TextComposite o)
    {
        return 0;
    }

    /**
     * @return String interpretation of this text.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        int i = 0;
        for (TextComposite item: list) {
            result.append(item.toString());
            i++;
            if(i<list.size())
            {result.append("\n\n\t");}
        }
        return result.toString();
    }
}
