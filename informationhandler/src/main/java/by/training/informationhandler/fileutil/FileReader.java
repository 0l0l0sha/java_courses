package by.training.informationhandler.fileutil;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileReader {

    /**
     * Our logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();


    /**
     * method that takes information from file in UTF-8 coding
     * and converts it in a right form
     * @param filePath of file with text
     * @return Text in correct formed String.
     */
    public static String readFileToString(String filePath) {
        File myfile = new File(filePath);
        List<String> lines;
        try {
            lines = FileUtils.readLines(myfile,
                    StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            return "";
        }
        LOGGER.debug("There are " + lines.size() + " lines in the file\n");
        LOGGER.debug("The second line is: " + lines.get(1));
        StringBuilder temp = new StringBuilder();
        for (String line:lines) {
            temp.append(line);
            temp.append(" ");
        }
        String regex = "[\\n(\\x0B)\\f\\r]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(temp.toString());
        return  m.replaceAll("    ");
    }
}
