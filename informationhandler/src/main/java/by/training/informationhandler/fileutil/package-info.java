/**
 * Simple utils for reading from files in the correct form
 *
 * @since 1.0
 * @author Ololosha
 * @version 1.1
 */
package by.training.informationhandler.fileutil;