package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Symbol;
import by.training.informationhandler.composite.TextComposite;

import java.util.ArrayList;
import java.util.List;


/**
 * Class described how to create new Symbol
 * (last part of Chain)
 */
public class SymbolParser extends Parser
{
    /**
     * @see SymbolParser
     * @param str with symbol to create
     * @return new Symbol.
     */
    @Override
    public TextComposite createTextElem(String str)
    {
        return new Symbol(str.charAt(0));
    }

    /**
     * stub
     * @param str for parse
     * @return empty list
     */
    @Override
    public List<String> parse(String str)
    {
        return new ArrayList<>();
    }
}
