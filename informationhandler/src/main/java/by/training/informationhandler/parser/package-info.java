/**
 * Different levels of parsers for correct form String with a text.
 *
 * @since 1.0
 * @author Ololosha
 * @version 1.1
 */
package by.training.informationhandler.parser;