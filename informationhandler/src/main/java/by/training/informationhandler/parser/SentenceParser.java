package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Sentence;
import by.training.informationhandler.composite.TextComposite;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Class described how to parse Sentence into lexemes.
 */
public class SentenceParser extends  Parser
{
    /**
     * creating new empty sentence
     * @param str
     * @return that sentence
     */
    @Override
    public TextComposite createTextElem(String str)
    {
        return new Sentence();
    }

    /**
     * @see SentenceParser
     * @param str string to parse
     * @return List of lexemes in String form
     */
    @Override
    public List<String> parse(String str)
    {
        String regex = "[\\n(\\x0B)\\f\\r]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        str = m.replaceAll("");
        p = Pattern.compile("\\t");
        m = p.matcher(str);
        str = m.replaceAll(" ");
        return Arrays.asList(str.split(" "));
    }
}
