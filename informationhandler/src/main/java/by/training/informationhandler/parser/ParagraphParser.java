package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Paragraph;
import by.training.informationhandler.composite.TextComposite;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class described how to parse Paragraph into Sentences.
 */
public class ParagraphParser extends Parser
{
    /**
     * creating new empty paragraph
     * @param str
     * @return that paragraph
     */
    @Override
    public TextComposite createTextElem(String str)
    {
        return new Paragraph();
    }


    /**
     * @see ParagraphParser
     * @param str string to parse
     * @return List of sentences in String form
     */
    @Override
    public List<String> parse(String str)
    {
        List<String> result = new ArrayList<>();
        String regex = "([^!\\?\\.]+[!\\?\\.]+)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        while (m.find())
        {
            result.add(m.group().trim());
        }
        return result;
    }
}
