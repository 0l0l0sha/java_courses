package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Lexeme;
import by.training.informationhandler.composite.TextComposite;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class described how to parse Lexeme to word and mark.
 */
public class LexemeParser extends Parser {
    /**
     * creating new empty Lexeme
     * @param str
     * @return that lexeme
     */
    @Override
    public TextComposite createTextElem(String str) {
        return new Lexeme();
    }

    /**
     * @see LexemeParser
     * @param str string to parse
     * @return List, contains word in 0 position (if it is)
     * and Mark in 1 (if there is word and mark)
     * or mark in 0 (if there is only mark) in String form
     */
    @Override
    public List<String> parse(String str) {
        String regex1 = "[^\\p{Punct}]+";
        String regex2 = "[\\p{Punct}]+";
        Pattern p1 = Pattern.compile(regex1);
        Matcher m1 = p1.matcher(str);
        Pattern p2 = Pattern.compile(regex2);
        Matcher m2 = p2.matcher(str);
        List<String>result = new ArrayList<>();
        if (m1.find()) {
            result.add(m1.group());
        }
        if (m2.find()) {
            result.add(m2.group());
        }
        return result;
    }
}
