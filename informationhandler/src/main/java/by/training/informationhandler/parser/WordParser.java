package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Mark;
import by.training.informationhandler.composite.TextComposite;
import by.training.informationhandler.composite.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class described how to parse word or mark into symbols
 * and create rite one.
 */
public class WordParser extends Parser {

    /**
     * creating new empty word or mark based on the string contents
     * @param str fot parse
     * @return that word or mark
     */
    @Override
    public TextComposite createTextElem(String str) {
        String regex1 = "[^\\p{Punct}]+";
        Pattern p1 = Pattern.compile(regex1);
        Matcher m1 = p1.matcher(str);
        if (m1.find()) {
            return new Word();
        }
        return new Mark();
    }

    /**
     * @see WordParser
     * @param str string to parse
     * @return List of Symbols in String form
     */
    @Override
    public List<String> parse(String str) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            result.add("" + str.charAt(i));
        }
        return result;
    }
}
