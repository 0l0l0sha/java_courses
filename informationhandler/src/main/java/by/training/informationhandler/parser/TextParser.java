package by.training.informationhandler.parser;

import by.training.informationhandler.composite.Text;
import by.training.informationhandler.composite.TextComposite;

import java.util.Arrays;
import java.util.List;


/**
 * Class described how to parse Sentence into paragraphs.
 */
public class TextParser extends Parser {
    /**
     * creating new empty text
     * @param str
     * @return that text
     */
    @Override
    public TextComposite createTextElem(String str) {
        return new Text();
    }

    /**
     * @see TextParser
     * @param str string to parse
     * @return List of Paragraphs in String form
     */
    @Override
    public List<String> parse(String str) {
        String regex = "[ ]{4}";
        String[] res = str.split(regex);
        return Arrays.asList(res);
    }
}
