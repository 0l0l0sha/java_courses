package by.training.informationhandler.parser;

import by.training.informationhandler.composite.TextComposite;
import java.util.List;

/**
 * abstract parser that described creation and parse
 * and has realized main methods build and set Next Parser
 */
public abstract class Parser
{
    private Parser nextParser;

    public Parser getNextParser() {
        return nextParser;
    }

    /**
     * Set next Parser
     * @param nextParser new next Parser
     */
    public void setNextParser(Parser nextParser) {
        this.nextParser = nextParser;
    }

    /**
     * main method
     * describe how to build a composite from taking string
     * @param str String to parse into composites.
     * @return full composite
     */
    public TextComposite build(String str)
    {
        str = str.trim();
        TextComposite root = createTextElem(str);
        if (nextParser != null)
        {
            List<String> list = this.parse(str);
            for (String childStr : list)
            {
                if (!childStr.isEmpty()) {
                    TextComposite item = nextParser.build(childStr);
                    root.add(item);
                }
            }
        }
        return root;
    }

    /**
     * creating method
     * @param str for parse
     * @return result of composite constructor
     */
    public abstract TextComposite createTextElem(String str);

    /**
     * @param str for parse
     * @return list of substrings
     */
    public abstract List<String> parse(String str);
}
