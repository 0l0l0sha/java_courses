package by.trainig.safematrix.mainapp;

import by.trainig.safematrix.celleditor.CellEditor;
import by.trainig.safematrix.writer.Writer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {
    private static final Logger LOGGER = LogManager.getLogger();


    public static void main(String args[]) {
        Writer.setPrintStream(System.out);
        while (true) {
            System.out.println("Choose destiny of this matrix");
            System.out.println("1 to change");
            System.out.println("2 to see it");
            System.out.println("3 to destroy everything");
            Scanner in = new Scanner(System.in);
            int choise;
            try {
                choise = in.nextInt();
            } catch (InputMismatchException e) {
                choise = 0;
            }
            if (choise == 3) {
                break;
            }
            switch (choise) {
                case 1:
                    try {
                        CellEditor.start();
                    } catch (RuntimeException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        Writer.writeMatrix();
                    } catch (RuntimeException e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                default:
                    System.out.println("You are not omnipotent\nChoose from the presented opportunities");
                    break;
            }
        }
        LOGGER.debug("stop");
    }
}
