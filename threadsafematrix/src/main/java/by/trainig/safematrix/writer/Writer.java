package by.trainig.safematrix.writer;

import by.trainig.safematrix.matrix.Matrix;

import java.io.PrintStream;

public class Writer {

    private static PrintStream printStream = System.out;

    public static void setPrintStream (PrintStream newStream){
        printStream = newStream;
    }

    public static void writeMatrix() {
        String result = Matrix.getInstance().toString();
        printStream.println(result);
    }
}
