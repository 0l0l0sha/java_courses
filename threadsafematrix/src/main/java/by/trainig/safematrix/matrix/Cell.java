package by.trainig.safematrix.matrix;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;


public class Cell {
    private int value;
    private boolean flag = false;
    //   private ReentrantLock locker = new ReentrantLock();
    private Semaphore semaphore = new Semaphore(1);

    private static final Logger LOGGER = LogManager.getLogger();

    public Cell(int value) {
        this.value = value;
    }


    public int getValue() {
        return value;
    }

    public boolean setValue(int value) {
        if (semaphore.tryAcquire() && !flag) {
            this.value = value;
            this.flag = true;
            semaphore.release();
            return true;
        }
        semaphore.release();
        return false;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
