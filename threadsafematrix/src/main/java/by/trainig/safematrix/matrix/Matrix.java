package by.trainig.safematrix.matrix;

import by.trainig.safematrix.serveces.reader.Reader;
import by.trainig.safematrix.serveces.validator.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Matrix {

    public static final String FILE_PATH = "data/matrix.txt";

    private static Matrix matrix;

    private static final Logger LOGGER = LogManager.getLogger();

    private static ReentrantLock locker = new ReentrantLock();

    private int size = 10;

    private Cell[][] cellArray;

    private Matrix() {
        List<String> list = Reader.readFileToString(FILE_PATH);
        int[][] args = Validator.validateMatrix(list);
        this.cellArray = new Cell[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                cellArray[i][j] = new Cell(args[i][j]);
            }
        }
    }

    public static Matrix getInstance() {
        if (matrix == null){
            locker.lock();
            if (matrix == null)
            {
                matrix = new Matrix();
            }
            locker.unlock();
        }
        return matrix;
    }

    public int getSIZE() {
        return size;
    }

    public boolean setValueCell(int value, int row, int column) {
        Cell cell = cellArray[row][column];
        LOGGER.debug("Cell " + row + " " + column);
        boolean lol = cell.setValue(value);
        return lol;
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result.append(cellArray[i][j].toString());
                result.append(' ');
            }
            result.append('\n');
        }
        return result.toString();
    }
}
