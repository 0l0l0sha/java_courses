package by.trainig.safematrix.celleditor;

import by.trainig.safematrix.celleditor.thread.CellEditorThread;
import by.trainig.safematrix.serveces.reader.Reader;
import by.trainig.safematrix.serveces.validator.Validator;

import java.util.List;


public class CellEditor {
    public static final String FILE_PATH = "data/threads.txt";

    public static void start() {
        List<String> list = Reader.readFileToString(FILE_PATH);
        int[] args = Validator.validateThreads(list);
        for (int i = 0; i < args.length; i++) {
            CellEditorThread p = new CellEditorThread(args[i]);
            Thread t = new Thread(p);
            t.start();
        }
    }
}
