package by.trainig.safematrix.celleditor.thread;

import by.trainig.safematrix.matrix.Matrix;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class CellEditorThread implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger();

    private int value;

    public CellEditorThread(int value) {
        this.value = value;
    }

    public void run() {
        Matrix matrix = Matrix.getInstance();
        Random random = new Random();
        int size = matrix.getSIZE();
        int index = random.nextInt(size);
        int attempts = 0;
        while (attempts < size) {
            boolean result = matrix.setValueCell(value, index, index);
            LOGGER.debug("Cell " + index + " " + index + "; Result: " + result + " (value " + value + ")");
            index++;
            if (index == size) {
                index = 0;
            }
            attempts++;
        }
        LOGGER.debug("\n" + matrix.toString());
    }
}
