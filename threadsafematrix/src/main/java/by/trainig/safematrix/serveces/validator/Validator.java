package by.trainig.safematrix.serveces.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Validator {

    private static final Logger LOGGER = LogManager.getLogger();

    public static int[][] validateMatrix(List<String> data) {
        int size = data.size();
        int result[][] = new int[size][size];
        int i = 0;
        for (String str : data) {
            String[] args = str.split(" ");
            int j = 0;
            for (String arg : args) {
                if (j >= size) {
                    throw new IllegalArgumentException("row and column quantity must be equal");
                }
                try {
                    int check = Integer.parseInt(arg);
                    result[i][j] = check;
                } catch (NumberFormatException e) {
                    LOGGER.error(e.toString(), e);
                    throw new IllegalArgumentException("incorrect matrix file", e);
                }
                ++j;
            }
            ++i;
        }
        return result;
    }

    public static int[] validateThreads(List<String> data) {
        int size = data.size();
        int result[] = new int[size];
        int i = 0;
        for (String arg : data) {
            try {
                int check = Integer.parseInt(arg);
                result[i] = check;
            } catch (NumberFormatException e) {
                LOGGER.error(e.toString(), e);
                throw new IllegalArgumentException("incorrect threads file", e);
            }
            ++i;
        }
        return result;
    }

}
