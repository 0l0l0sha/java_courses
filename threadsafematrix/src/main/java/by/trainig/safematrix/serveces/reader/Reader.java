package by.trainig.safematrix.serveces.reader;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Reader {

    /**
     * Our logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    public static List<String> readFileToString(String filePath) {
        File myfile = new File(filePath);
        List<String> lines = new ArrayList<>();
        try {
            lines = FileUtils.readLines(myfile,
                    StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            throw new IllegalArgumentException("incorrect file" + filePath,e) ;
        }
        return lines;
    }
}

