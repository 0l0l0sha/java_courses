package by.training.itcompany.team;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.handler.EmployeeSalaryCalculator;
import by.training.itcompany.repository.comparator.LevelComparator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class described team in IT-company.
 */
public class Team {

    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * List of team members.
     */
    private List<Pair> members = new ArrayList<>();

    //todo javadoc
    public double getTeamPrice() {
        double teamPrice = 0;
        for (Pair pair : members) {
            TargetTeamMember target = pair.getTargetMember();
            Employee employee = pair.getEmployee();
            teamPrice += target.getHours() * EmployeeSalaryCalculator.salary(employee);
        }
        return teamPrice;
    }


    /**
     * Set List of team members.
     *
     * @param members List of {@link Pair}
     */
    public void setMembers(final List<Pair> members) {
        this.members = members;
    }

    //todo javadoc
    public void addMember(final Employee member, TargetTeamMember teamMember) {
        this.members.add(new Pair(teamMember, member));
        member.setBusy(true);
    }

    public int getTeamSize() {
        return members.size();
    }

    public void setTeamLeader() {
        Employee employeeMax = members.get(0).getEmployee();
        Comparator<Employee> cmpLevel = new LevelComparator();
        for (Pair pair : members) {
            Employee employee = pair.getEmployee();
            TargetTeamMember job = pair.getTargetMember();
            if (job.getSpeciality() == Speciality.DEV && cmpLevel.compare(employee, employeeMax) > 0) {
                employeeMax = employee;
            }
        }
        if (employeeMax instanceof Developer) {
            ((Developer) employeeMax).setTeamLead(true);
            return;
        }
        LOGGER.error("No Developers in that team" + this.toString());
    }

    @Override
    public String toString() {
        return "Team{" +
                "members=" + members +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Team team = (Team) o;

        boolean flag = false;
        for (Pair member : members) {
            flag = false;
            for (Pair memberO : team.members) {
                if (memberO.equals(member)) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                LOGGER.error(member);
                break;
            }
        }
        return flag;
    }

    @Override
    public int hashCode() {
        return members != null ? members.hashCode() : 0;
    }
}
