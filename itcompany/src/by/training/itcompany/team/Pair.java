package by.training.itcompany.team;

import by.training.itcompany.entity.Employee;

public class Pair {
    private TargetTeamMember targetMember;
    private Employee employee;

    public Pair(TargetTeamMember targetMember, Employee employee) {
        this.targetMember = targetMember;
        this.employee = employee;
    }

    public TargetTeamMember getTargetMember() {
        return targetMember;
    }

    public void setTargetMember(TargetTeamMember targetMember) {
        this.targetMember = targetMember;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "job = " + targetMember +
                ", employee = " + employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair pair = (Pair) o;

        if (targetMember != null ? !targetMember.equals(pair.targetMember) : pair.targetMember != null) return false;
        return employee != null ? employee.equals(pair.employee) : pair.employee == null;
    }

    @Override
    public int hashCode() {
        int result = targetMember != null ? targetMember.hashCode() : 0;
        result = 31 * result + (employee != null ? employee.hashCode() : 0);
        return result;
    }
}
