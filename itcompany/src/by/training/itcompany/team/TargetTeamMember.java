package by.training.itcompany.team;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;

//todo javadoc
public class TargetTeamMember {

    //todo javadoc
    private Level level;
    //todo javadoc
    private Speciality speciality;
    //todo javadoc
    private int hours;
    //todo javadoc
    private boolean flag = false;
//todo javadoc
    public TargetTeamMember(int hours, Speciality speciality, Level level) {
        this.level = level;
        this.speciality = speciality;
        this.hours = hours;
    }
    //todo javadoc
    public int getHours() {
        return hours;
    }
    //todo javadoc
    public void setHours(int hours) {
        this.hours = hours;
    }

    //todo javadoc
    public Level getLevel() {
        return level;
    }
    //todo javadoc
    public void setLevel(Level level) {
        this.level = level;
    }
    //todo javadoc
    public Speciality getSpeciality() {
        return speciality;
    }
    //todo javadoc
    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }
    //todo javadoc
    public boolean isFlag() {
        return flag;
    }
    //todo javadoc
    public void setFlag(boolean flag) {
        this.flag = flag;
    }


    @Override
    public String toString() {
        return "TargetTeamMember{" +
                "level=" + level +
                ", speciality=" + speciality +
                ", hours=" + hours +
                ", flag=" + flag +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetTeamMember that = (TargetTeamMember) o;

        if (hours != that.hours) return false;
        if (flag != that.flag) return false;
        if (level != that.level) return false;
        return speciality == that.speciality;
    }

    @Override
    public int hashCode() {
        int result = level != null ? level.hashCode() : 0;
        result = 31 * result + (speciality != null ? speciality.hashCode() : 0);
        result = 31 * result + hours;
        result = 31 * result + (flag ? 1 : 0);
        return result;
    }
}
