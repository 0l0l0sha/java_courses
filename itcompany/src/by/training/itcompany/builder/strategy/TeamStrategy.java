package by.training.itcompany.builder.strategy;

import by.training.itcompany.builder.TeamBuilder;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.factory.EmployeeFactory;
import by.training.itcompany.factory.EmployeeFactoryJava;
import by.training.itcompany.factory.EmployeeFactoryNet;
import by.training.itcompany.factory.TargetTeamMemberFactory;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.team.TargetTeamMember;
import by.training.itcompany.team.Team;
import by.training.itcompany.util.Reader;
import by.training.itcompany.validator.TargetTeamValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class TeamStrategy {

    private static final Logger LOGGER = LogManager.getLogger();

    public static Optional<Team> fileTeamCreate(EmployeeRepository repository, String targetFilePath, String employeeFilePath) {
        List<String> targetMembersData = Reader.readFileToString(targetFilePath);
        if (targetMembersData.isEmpty()) {
            LOGGER.error("Invalid target team File Path or empty file");
            return Optional.empty();
        }
        Optional<List<String[]>> oTargetData = TargetTeamValidator.validate(targetMembersData);
        if (!oTargetData.isPresent()) {
            LOGGER.error("Invalid target team Data");
            return Optional.empty();
        }
        List<String[]> targetData = oTargetData.get();
        Technology technology = Technology.valueOf(targetData.remove(0)[0]);
        List<TargetTeamMember> targetMembers = createTargetList(targetData);
        EmployeeFactory factory;
        switch (technology) {
            case JAVA:
                factory = new EmployeeFactoryJava();
                break;
            case NET:
                factory = new EmployeeFactoryNet();
                break;
            default:
                factory = new EmployeeFactoryJava();
        }
        List<String> employeeData = Reader.readFileToString(employeeFilePath);
        if (targetMembersData.isEmpty()) {
            LOGGER.error("Invalid employee File Path or empty file");
            return Optional.empty();
        }
        Optional<Team> oTeam = TeamBuilder.listStringBuild(employeeData, factory, repository, targetMembers);
        if (oTeam.isPresent()) {
            return oTeam;
        }
        return Optional.empty();
    }

    public static Optional<Team> reposytoryTeamCreate(EmployeeRepository repository, String targetFilePath) {
        List<String> targetMembersData = Reader.readFileToString(targetFilePath);
        if (targetMembersData.isEmpty()) {
            LOGGER.error("Invalid target team File Path or empty file");
            return Optional.empty();
        }
        Optional<List<String[]>> oTargetData = TargetTeamValidator.validate(targetMembersData);
        if (!oTargetData.isPresent()) {
            LOGGER.error("Invalid target team Data");
            return Optional.empty();
        }
        List<String[]> targetData = oTargetData.get();
        Technology technology = Technology.valueOf(targetData.remove(0)[0]);
        List<TargetTeamMember> targetMembers = createTargetList(targetData);
        Optional<Team> oTeam = TeamBuilder.repositoryBuild(technology, repository, targetMembers);
        if (oTeam.isPresent()) {
            return oTeam;
        }
        return Optional.empty();
    }

    private static List<TargetTeamMember> createTargetList(List<String[]> data) {
        List<TargetTeamMember> result = new ArrayList<>();
        for (String[] targetParam : data) {
            int targetHour = Integer.parseInt(targetParam[0]);
            Speciality targetSpec = Speciality.valueOf(targetParam[1]);
            Level targetLevel = Level.valueOf(targetParam[2]);
            result.add(TargetTeamMemberFactory.getTargetTeamMember(targetHour, targetSpec, targetLevel));
        }
        return result;
    }
}
