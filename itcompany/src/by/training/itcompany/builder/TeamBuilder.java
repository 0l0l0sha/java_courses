package by.training.itcompany.builder;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.team.TargetTeamMember;
import by.training.itcompany.team.Team;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialist;
import by.training.itcompany.factory.EmployeeFactory;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByNameAndLevel;
import by.training.itcompany.repository.specification.EmployeeSpecificationByBusinessAndTech;
import by.training.itcompany.validator.EmployeeValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

//todo javadoc
public final class TeamBuilder {
    //todo javadoc
    private static final Logger LOGGER = LogManager.getLogger();

    //todo javadoc
    private TeamBuilder() {
    }

    //todo javadoc
    public static Optional<Team> listStringBuild(final List<String> employeeData,final EmployeeFactory factory,
                                                 EmployeeRepository repository,  List<TargetTeamMember> targetMembers) {
        Technology technology = factory.getTechnology();
        Team team = new Team();
        boolean teamCheck = false;
        for (String data : employeeData) {
            if (!data.isEmpty()) {
                Optional<String[]> oParams = EmployeeValidator.validate(data);
                if (oParams.isPresent()) {
                    String[] params = oParams.get();
                    if (technology == Technology.valueOf(params[0])) {
                        Speciality seekerSpec = Speciality.valueOf(params[1]);
                        Level seekerLevel = Level.valueOf(params[2]);
                        String seekerName = params[3];
                        EmployeeSpecification specification = new EmployeeSpecificationByNameAndLevel(seekerName, seekerLevel);
                        List<Employee> existEmloyee = repository.query(specification);
                        Employee seeker = null;
                        if (existEmloyee.isEmpty()) {
                            switch (seekerSpec) {
                                case DEV:
                                    seeker = factory.getDeveloper(seekerName, seekerLevel);
                                    break;
                                case QA:
                                    seeker = factory.getQualitySpecialist(seekerName, seekerLevel);
                                    break;
                            }
                            repository.addEmployee(seeker);
                        } else {
                            seeker = existEmloyee.get(0);
                        }
                        Optional<TargetTeamMember> targetMember = checkTarget(seeker, targetMembers);
                        if (targetMember.isPresent()) {
                            team.addMember(seeker, targetMember.get());
                            teamCheck = team.getTeamSize() == targetMembers.size();
                        }

                    }
                } else {
                    LOGGER.error("Invalid Employee Data in " + employeeData.indexOf(data) + " line");
                }
            }
        }
        if (teamCheck) {
            team.setTeamLeader();
            return Optional.of(team);
        }
        LOGGER.error("Not enough seekers for that team");
        return Optional.empty();
    }

    //todo javadoc
    public static Optional<Team> repositoryBuild(final Technology technology, EmployeeRepository repository, List<TargetTeamMember> targetMembers) {
        Team team = new Team();
        boolean teamCheck = false;
        List<Employee> freeList = repository.query(new EmployeeSpecificationByBusinessAndTech(false, technology));
        for (Employee freeOne : freeList) {
            Optional<TargetTeamMember> targetMember = checkTarget(freeOne, targetMembers);
            if (targetMember.isPresent()) {
                team.addMember(freeOne, targetMember.get());
                teamCheck = team.getTeamSize() == targetMembers.size();
            }
            if (teamCheck) {
                team.setTeamLeader();
                return Optional.of(team);
            }
        }
        LOGGER.error("Not enough seekers for that team");
        return Optional.empty();
    }

    private static Optional<TargetTeamMember> checkTarget(final Employee seeker, List<TargetTeamMember> targetMembers) {
        for (TargetTeamMember targetMember : targetMembers) {
            if (!targetMember.isFlag()) {
                boolean checkSpec = false;
                switch (targetMember.getSpeciality()) {
                    case DEV:
                        checkSpec = seeker instanceof Developer;
                        break;
                    case QA:
                        checkSpec = seeker instanceof QualitySpecialist;
                        break;
                }
                if (checkSpec
                        && targetMember.getLevel() == seeker.getLevel()
                        && !seeker.isBusy()) {
                    targetMember.setFlag(true);
                    LOGGER.debug("Seeker fit Target. Target: " + targetMember + "; Seeker:" + seeker);
                    return Optional.of(targetMember);
                }
            }
        }
        LOGGER.debug("Seeker not fit Target. Target: " + targetMembers + "; Seeker:" + seeker);
        return Optional.empty();
    }

}

