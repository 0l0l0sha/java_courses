package by.training.itcompany.repository;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.repository.specification.EmployeeSpecification;

import java.util.List;
import java.util.Optional;


//todo javadoc
public interface EmployeeRepository {
    //todo javadoc
    int addEmployee(Employee employee);

    //todo javadoc
    Optional<Employee> removeEmployee(Employee employee);

    //todo javadoc
    void updateEmployee(Employee employee);

    //todo javadoc
    List<Employee> query(EmployeeSpecification specification);
}
