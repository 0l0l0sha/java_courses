package by.training.itcompany.repository;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByID;
import by.training.itcompany.repository.specification.Sorted;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


//todo javadoc

public class EmployeeRepositoryImpl implements EmployeeRepository {
    //todo javadoc
    private List<Employee> employeeList = new ArrayList<>();
    //todo javadoc
    @Override
    public int addEmployee(Employee employee) {
        int lastIndex = employeeList.size() - 1;
        int newID;
        if (lastIndex >= 0) {
            newID = employeeList.get(lastIndex).getId() + 1;
        } else {
            newID = 1;
        }
        employee.setId(newID);
        employeeList.add(employee);
        return newID;
    }
    //todo javadoc
    @Override
    public Optional<Employee> removeEmployee(Employee employee) {
        int index = employeeList.indexOf(employee);
        if (index != -1) {
            return Optional.ofNullable(employeeList.remove(index));
        }
        return Optional.empty();
    }
    //todo javadoc
    @Override
    public void updateEmployee(Employee employee) {
        List<Employee> list = query(new EmployeeSpecificationByID(employee.getId()));
        if (list.isEmpty()) {
            //todo log
            return;
        }
        // объект единственный т.к. одинаковых айди быть не должно
        Employee oldEmployee = list.get(0);
        int index = employeeList.indexOf(oldEmployee);
        employeeList.set(index, employee);
    }
    //todo javadoc
    @Override
    public List<Employee> query(EmployeeSpecification specification) {
        List<Employee> result = new ArrayList<>();
        for (Employee employee : employeeList) {
            if (specification.specified(employee)) {
                result.add(employee);
            }
        }
        if (specification instanceof Sorted) {
            result.sort(((Sorted) specification).getComparator());
        }
        return result;
    }

}
