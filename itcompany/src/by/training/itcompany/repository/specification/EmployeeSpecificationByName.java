package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;

public class EmployeeSpecificationByName implements EmployeeSpecification {
    private String desiredEmployeeName;

    public EmployeeSpecificationByName(String desiredEmployeeName) {
        super();
        this.desiredEmployeeName = desiredEmployeeName.toUpperCase();
    }
    @Override
    public boolean specified(Employee employee) {
        String checkName = employee.getName();
        return checkName.equals(desiredEmployeeName);
    }
}
