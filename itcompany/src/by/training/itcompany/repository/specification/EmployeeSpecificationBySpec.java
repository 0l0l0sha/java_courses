package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Speciality;

public class EmployeeSpecificationBySpec implements EmployeeSpecification {
    private Speciality desiredEmployeeSpec;

    public EmployeeSpecificationBySpec(Speciality desiredEmployeeSpec) {
        super();
        this.desiredEmployeeSpec = desiredEmployeeSpec;
    }
    @Override
    public boolean specified(Employee employee) {

        return employee.getSpeciality() == desiredEmployeeSpec;
    }
}
