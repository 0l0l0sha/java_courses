package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Java;
import by.training.itcompany.entity.Net;
import by.training.itcompany.entity.Technology;

public class EmployeeSpecificationByBusinessAndTech implements EmployeeSpecification{
    private boolean desiredEmployeeBusyness;
    private Technology desiredEmployeeTech;

    public EmployeeSpecificationByBusinessAndTech(boolean desiredEmployeeBusiness, Technology desiredEmployeeTech) {
        super();
        this.desiredEmployeeBusyness = desiredEmployeeBusiness;
        this.desiredEmployeeTech = desiredEmployeeTech;
    }
    @Override
    public boolean specified(Employee employee) {
        boolean targetTech = false;
        switch (desiredEmployeeTech){
            case JAVA:
                targetTech = employee instanceof Java;
                break;
            case NET:
                targetTech = employee instanceof Net;
                break;
        }
        return  targetTech && employee.isBusy() == desiredEmployeeBusyness;
    }
}
