package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;

import java.util.Comparator;

public interface Sorted {
    Comparator<Employee> getComparator();
}
