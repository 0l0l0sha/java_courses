package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.repository.comparator.NameComparator;
import by.training.itcompany.repository.comparator.SalaryComparator;

import java.util.Comparator;

public class EmployeeSpecificationSortBySalaryAndName implements EmployeeSpecification, Sorted{
    @Override
    public boolean specified(Employee employee) {
        return true;
    }

    @Override
    public Comparator<Employee> getComparator() {
        return new SalaryComparator().thenComparing(new NameComparator());
    }
}
