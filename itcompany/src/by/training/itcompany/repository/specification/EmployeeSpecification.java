package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;

public interface EmployeeSpecification {
    boolean specified(Employee employee);
}
