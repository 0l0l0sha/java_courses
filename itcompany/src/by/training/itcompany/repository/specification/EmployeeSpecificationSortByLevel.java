package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.repository.comparator.LevelComparator;
import by.training.itcompany.repository.comparator.NameComparator;

import java.util.Comparator;

public class EmployeeSpecificationSortByLevel implements EmployeeSpecification, Sorted {
    @Override
    public boolean specified(Employee employee) {
        return true;
    }

    @Override
    public Comparator<Employee> getComparator() {
        return new LevelComparator();
    }
}
