package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;

public class EmployeeSpecificationByLevel implements EmployeeSpecification {
    private Level desiredLevel;

    public EmployeeSpecificationByLevel(Level desiredLevel) {
        super();
        this.desiredLevel = desiredLevel;
    }
    @Override
    public boolean specified(Employee employee) {
        return employee.getLevel() == desiredLevel;
    }
}
