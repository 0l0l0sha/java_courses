package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.repository.comparator.*;

import java.util.Comparator;

public class EmployeeSpecificationSortBySpec implements EmployeeSpecification, Sorted{
    @Override
    public boolean specified(Employee employee) {
        return true;
    }

    @Override
    public Comparator<Employee> getComparator() {
        return new SpecComparator();
    }
}
