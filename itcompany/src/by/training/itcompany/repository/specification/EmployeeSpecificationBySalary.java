package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.handler.EmployeeSalaryCalculator;

public class EmployeeSpecificationBySalary implements EmployeeSpecification {
    private double minSalary;
    private double maxSalary;

    public EmployeeSpecificationBySalary(double minSalary, double maxSalary) {
        if (minSalary > maxSalary) {
            double temp = minSalary;
            minSalary = maxSalary;
            maxSalary = temp;
        }
        this.minSalary = minSalary;
        this.maxSalary = maxSalary;
    }


    @Override
    public boolean specified(Employee employee) {
        double salary = EmployeeSalaryCalculator.salary(employee);
        return (Double.compare(minSalary, salary) <= 0
                && Double.compare(salary, maxSalary) <= 0);
    }
}
