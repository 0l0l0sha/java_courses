package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;

public class EmployeeSpecificationByID implements EmployeeSpecification {
    private int desiredEmployeeID;

    public EmployeeSpecificationByID(int desiredEmployeeID) {
        super();
        this.desiredEmployeeID = desiredEmployeeID;
    }
    @Override
    public boolean specified(Employee employee) {
        return employee.getId() == desiredEmployeeID;
    }
}
