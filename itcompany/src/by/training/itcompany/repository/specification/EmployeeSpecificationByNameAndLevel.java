package by.training.itcompany.repository.specification;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;

public class EmployeeSpecificationByNameAndLevel implements EmployeeSpecification {
    private String desiredName;
    private Level desiredLevel;

    public EmployeeSpecificationByNameAndLevel(String desiredName, Level desiredLevel) {
        super();
        this.desiredName = desiredName.toUpperCase();
        this.desiredLevel = desiredLevel;
    }

    @Override
    public boolean specified(Employee employee) {
        return employee.getLevel() == desiredLevel && employee.getName().equals(desiredName);
    }
}
