package by.training.itcompany.repository.comparator;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.handler.EmployeeSalaryCalculator;
import java.util.Comparator;

//todo javadoc
public class SalaryComparator implements Comparator<Employee> {
    //todo javadoc
    @Override
    public int compare(Employee employee1, Employee employee2) {
        double salary1 = EmployeeSalaryCalculator.salary(employee1);
        double salary2 = EmployeeSalaryCalculator.salary(employee2);
        return Double.compare(salary1,salary2);
    }
}
