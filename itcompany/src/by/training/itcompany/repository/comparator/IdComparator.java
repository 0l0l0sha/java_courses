package by.training.itcompany.repository.comparator;

import by.training.itcompany.entity.Employee;
import java.util.Comparator;

//todo javadoc
public class IdComparator implements Comparator<Employee> {
    //todo javadoc
        @Override
        public int compare(Employee employee1, Employee employee2) {
            return Integer.compare(employee1.getId(), employee2.getId());
        }
    }
