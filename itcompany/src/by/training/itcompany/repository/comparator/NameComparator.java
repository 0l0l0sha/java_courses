package by.training.itcompany.repository.comparator;

import by.training.itcompany.entity.Employee;

import java.util.Comparator;

//todo javadoc
public class NameComparator implements Comparator<Employee> {
    //todo javadoc
    @Override
    public int compare(Employee employee1, Employee employee2) {
        return (employee1.getName().compareTo(employee2.getName()));
    }
}
