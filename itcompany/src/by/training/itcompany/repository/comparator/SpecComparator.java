package by.training.itcompany.repository.comparator;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.handler.EmployeeSalaryCalculator;

import java.util.Comparator;

//todo javadoc
public class SpecComparator implements Comparator<Employee> {
    //todo javadoc
    @Override
    public int compare(Employee employee1, Employee employee2) {
        return employee1.getClass().getSimpleName().compareTo(employee2.getClass().getSimpleName());
    }
}
