package by.training.itcompany.repository.comparator;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;

import java.util.Comparator;

public class LevelComparator  implements Comparator<Employee> {

    @Override
    public int compare(Employee employee1, Employee employee2) {
        int lev1 = levelToInt(employee1.getLevel());
        int lev2 = levelToInt(employee2.getLevel());
        return Integer.compare(lev1,lev2);
    }

    private int levelToInt(Level level)
    {
        switch (level){
            case FIRST:
                return 1;
            case SECOND:
                return 2;
            case THIRD:
                return 3;
            case FOURTH:
                return 4;
        }
        //todo log c неправильной ошибкой?
        return 0;
    }
}
