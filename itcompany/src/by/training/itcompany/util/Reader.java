package by.training.itcompany.util;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

//todo javadoc
public class Reader {

    /**
     * Our logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    //todo javadoc
    public static final String FILE_PATH = "data/input.txt";

    //todo javadoc
    public static List<String> readFileToString(String filePath) {
        File myfile = new File(filePath);
        List<String> lines = new ArrayList<>();
        try {
            lines = FileUtils.readLines(myfile,
                    StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            return new ArrayList<>();
        }
        LOGGER.debug("There are " + lines.size() + " lines in the file\n");
        LOGGER.debug("The second line is: " + lines.get(1));
        return lines;
    }
}
