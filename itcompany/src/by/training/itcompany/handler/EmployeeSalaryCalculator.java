package by.training.itcompany.handler;

import by.training.itcompany.entity.Employee;

//todo javadoc
public final class EmployeeSalaryCalculator {
    //todo javadoc
    private EmployeeSalaryCalculator() {
    }

    //todo javadoc

    public static double salary(final Employee employee) {
        switch (employee.getLevel()) {
            case FIRST:
                return employee.getRate();
            case SECOND:
                return employee.getRate() * 2;
            case THIRD:
                return employee.getRate() * 3;
            case FOURTH:
                return employee.getRate() * 4;
            default:

                return 0;
        }
    }
}
