package by.training.itcompany.handler;

import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.entity.Employee;

//todo javadoc
public class EmployeeCopy {
    //todo javadoc
    public static void copy(Employee source, Employee dest) {
        dest.setName(source.getName());
        dest.setLevel(source.getLevel());
        if (source instanceof Developer && dest instanceof Developer) {
            ((Developer) dest).setTeamLead(((Developer) source).isTeamLead());
        }
    }
}
