package by.training.itcompany.factory;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.team.TargetTeamMember;

public class TargetTeamMemberFactory {
    public static TargetTeamMember getTargetTeamMember(int hours, Speciality speciality, Level level){
        return new TargetTeamMember(hours,speciality,level);
    }
}
