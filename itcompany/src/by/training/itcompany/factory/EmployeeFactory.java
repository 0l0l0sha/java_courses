package by.training.itcompany.factory;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialist;

//todo javadoc

public interface EmployeeFactory {
    //todo javadoc
    QualitySpecialist getQualitySpecialist(String name, Level level);

    //todo javadoc
    Developer getDeveloper(String name, Level level);

    //todo javadoc
    Technology getTechnology();
}
