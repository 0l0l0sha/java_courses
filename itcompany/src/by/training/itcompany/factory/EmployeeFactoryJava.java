package by.training.itcompany.factory;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialist;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;

//todo javadoc
public class EmployeeFactoryJava implements EmployeeFactory {
    //todo javadoc
    @Override
    public QualitySpecialist getQualitySpecialist(String name, Level level) {
        return new QualitySpecialistJava(name, level);
    }
    //todo javadoc
    @Override
    public Developer getDeveloper(String name, Level level) {
        return new DeveloperJava(name, level);
    }
    //todo javadoc
    @Override
    public Technology getTechnology() {
        return Technology.JAVA;
    }
}
