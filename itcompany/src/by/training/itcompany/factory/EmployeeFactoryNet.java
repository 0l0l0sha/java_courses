package by.training.itcompany.factory;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.Developer;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialist;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;

//todo javadoc
public class EmployeeFactoryNet implements EmployeeFactory {
    //todo javadoc
    @Override
    public QualitySpecialist getQualitySpecialist(String name, Level level) {
        return new QualitySpecialistNet(name, level);
    }
    //todo javadoc
    @Override
    public Developer getDeveloper(String name, Level level) {
        return new DeveloperNet(name, level);
    }
    //todo javadoc
    @Override
    public Technology getTechnology() {
        return Technology.NET;
    }
}
