package by.training.itcompany.main;
/** Runner class, runs everything.
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public final class Runner
{
    /** private constructor instead of default.
     */
    protected static final Logger logger = LogManager.getLogger();
    private Runner() { }

    /** main method with args.
     * @param args console args, no effects in our case
     */
    public static void main(final String[] args) {
        logger.debug("Hello World!");
    }
}
