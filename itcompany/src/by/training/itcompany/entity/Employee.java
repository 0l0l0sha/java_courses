package by.training.itcompany.entity;


/**
 * Class described Employee in IT-company.
 */
public abstract class Employee {
    /**
     * Employee's id.
     */
    private int id = 0;
    /**
     * Employee's name.
     */
    private String name;
    /**
     * Employee's skill level.
     */
    private Level level;

    /**
     * Employee's busyness.
     */
    private boolean busy = false;


    private Speciality speciality;

    /**
     * Employee's constructor.
     *
     * @param name  of Employee.
     * @param level of Employee's skill.
     */
    public Employee(final String name, final Level level) {
        this.name = name.toUpperCase();
        this.level = level;
    }

    /**
     * Employee's copy constructor.
     *
     * @param employee copy example.
     */
    public Employee(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName().toUpperCase();
        this.level = employee.getLevel();
        this.busy = employee.busy;
    }

    /**
     * @param id newID of Employee
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return int Employees's id
     */
    public int getId() {
        return id;
    }

    /**
     * @return String Employee's name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name is a new name of Employee
     */
    public void setName(final String name) {
        this.name = name.toUpperCase();
    }

    /**
     * @return {@link Level} Employee's skill level.
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Set's Employee's skill Level according to param.
     *
     * @param level {@link Level}
     */
    public void setLevel(final Level level) {
        this.level = level;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(final boolean busy) {
        this.busy = busy;
    }

    //todo javadoc


    public Speciality getSpeciality() {
        return speciality;
    }

    protected void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (!name.equals(employee.name)) return false;
        return level == employee.level;
    }

    //todo javadoc
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + level.hashCode();
        return result;
    }

    //todo javadoc
    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + " id = " + id
                + ", name = " + name
                + ", level = " + level;
    }

    /**
     * Get Employees Rate.
     *
     * @return base rate of Employee.
     */
    public abstract double getRate();

}
