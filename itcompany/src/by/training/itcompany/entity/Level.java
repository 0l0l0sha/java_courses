package by.training.itcompany.entity;

/**
 * enum of possible skill Levels of Employee.
 */
public enum Level {
    /** First level of skill.*/
    FIRST,
    /** Second level of skill.*/
    SECOND,
    /** First level of skill.*/
    THIRD,
    /** First level of skill.*/
    FOURTH
}
