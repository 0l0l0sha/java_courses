package by.training.itcompany.entity.developer;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Net;

//todo javadoc
public class DeveloperNet extends Developer implements Net {

    //todo javadoc
    public DeveloperNet() {
    }

    //todo javadoc
    public DeveloperNet(final Employee employee) {
        super(employee);
    }

    //todo javadoc
    public DeveloperNet(final String name, final Level level) {
        super(name, level);
    }


    @Override
    public double getRate() {
        return super.getRate() * NET_RATE;
    }

}
