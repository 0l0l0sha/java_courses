package by.training.itcompany.entity.developer;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.handler.EmployeeSalaryCalculator;

//todo javadoc

public class Developer extends Employee {
    //todo javadoc
    private static final double RATE = 10;
    private static final double TEAM_LEAD_RATE = 1.5;

    //todo javadoc
    private boolean teamLead = false;

    /**
     * Developer's empty constructor.
     */
    public Developer() {
        super("", Level.FIRST);
        setSpeciality(Speciality.DEV);
    }

    /**
     * Developers's copy constructor.
     *
     * @param employee copy example.
     */
    public Developer(final Employee employee) {
        super(employee);
        setSpeciality(Speciality.DEV);
        if (employee instanceof Developer) {
            this.teamLead = ((Developer) employee).teamLead;
        }
    }

    /**
     * Developer's constructor.
     *
     * @param name  of new Developer.
     * @param level of skill of new Developer
     */
    public Developer(final String name,final Level level) {
        super(name, level);
    }

    //todo javadoc
    @Override
    public String toString() {
        return super.toString()
                + ", base rate = " + RATE
                + ", team lead = " + teamLead
                + ", Salary = " + EmployeeSalaryCalculator.salary(this);
    }

    //todo javadoc
    @Override
    public boolean equals(final Object o) {
        if (!super.equals(o)) {
            return false;
        }

        Developer developer = (Developer) o;

        return teamLead == developer.teamLead;
    }
    //todo javadoc


    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (teamLead ? 1 : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getRate() {
        if (teamLead) {
            return RATE * TEAM_LEAD_RATE;
        }
        return RATE;
    }

    /**
     * Check Dev on TeamLead.
     *
     * @return true or false.
     */
    public boolean isTeamLead() {
        return teamLead;
    }

    //todo javadoc
    public void setTeamLead(final boolean teamLead) {
        this.teamLead = teamLead;
    }
}
