package by.training.itcompany.entity.developer;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Java;
import by.training.itcompany.entity.Level;
//todo javadoc
public class DeveloperJava extends Developer implements Java {


    //todo javadoc
    public DeveloperJava() {
    }

    //todo javadoc
    public DeveloperJava(final Employee employee) {
        super(employee);
    }

    //todo javadoc
    public DeveloperJava(final String name,final Level level) {
        super(name, level);
    }


    @Override
    public double getRate() {
        return super.getRate() * JAVA_RATE;
    }

}
