package by.training.itcompany.entity.qualityspecialist;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Java;
import by.training.itcompany.entity.Level;

public class QualitySpecialistJava extends QualitySpecialist implements Java {

    //todo javadoc
    public QualitySpecialistJava() {
        super();
    }

    //todo javadoc
    public QualitySpecialistJava(Employee employee) {
        super(employee);
    }

    //todo javadoc
    public QualitySpecialistJava(String name, Level level) {
        super(name, level);
    }

    @Override
    public double getRate() {
        return super.getRate()*JAVA_RATE;
    }
}
