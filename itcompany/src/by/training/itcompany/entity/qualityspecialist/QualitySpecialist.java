package by.training.itcompany.entity.qualityspecialist;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.handler.EmployeeSalaryCalculator;

//todo javadoc
public class QualitySpecialist extends Employee {
    //todo javadoc
    private static final double RATE = 2;

    /**
     * Empty constructor of Quality Specialist.
     */
    public QualitySpecialist() {
        super("", Level.FIRST);
        setSpeciality(Speciality.QA);
    }

    //todo javadoc
    public QualitySpecialist(final Employee employee) {
        super(employee);
        setSpeciality(Speciality.QA);
    }

    //todo javadoc
    public QualitySpecialist(final String name, final Level level) {
        super(name, level);
        setSpeciality(Speciality.QA);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return super.toString()
                + ", spec = QA"
                + ", base rate = " + RATE
                + ", Salary = " + EmployeeSalaryCalculator.salary(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getRate() {
        return RATE;
    }


}
