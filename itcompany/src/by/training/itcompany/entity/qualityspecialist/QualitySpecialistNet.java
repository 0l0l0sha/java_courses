package by.training.itcompany.entity.qualityspecialist;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Net;

public class QualitySpecialistNet extends QualitySpecialist implements Net {
    //todo javadoc
    public QualitySpecialistNet() {
        super();
    }

    //todo javadoc
    public QualitySpecialistNet(final Employee employee) {
        super(employee);
    }

    //todo javadoc
    public QualitySpecialistNet(final String name, final Level level) {
        super(name, level);
    }


    @Override
    public double getRate() {
        return super.getRate() * NET_RATE;
    }
}
