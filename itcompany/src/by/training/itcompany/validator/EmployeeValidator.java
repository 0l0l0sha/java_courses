package by.training.itcompany.validator;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.Technology;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

//todo javadoc
public final class EmployeeValidator {
    //todo javadoc
    private EmployeeValidator() {
    }

    /**
     * Our logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
//todo javadoc

    public static Optional<String[]> validate(String data) { // true - false - isValid
        if (data.trim().isEmpty()) {
            LOGGER.error("Empty Line");
            return Optional.empty();
        }
        String[] checkString = data.split(",");
        for (int i = 0; i < checkString.length; i++) {
            checkString[i] = checkString[i].trim();
            checkString[i] = checkString[i].toUpperCase();
        }
        if (checkString.length != 4) {
            LOGGER.error("Not enough parameters in " + data);
            return Optional.empty();
        }
        try {
            Technology.valueOf(checkString[0]);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Not valid Technology in " + data);
            return Optional.empty();
        }
        try {
            Speciality.valueOf(checkString[1]);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Not valid specialization in " + data);
            return Optional.empty();
        }
        try {
            Level.valueOf(checkString[2]);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Not valid level in " + data);
            return Optional.empty();
        }
        if (checkString[3].isEmpty()) {
            LOGGER.error("Empty name in " + data);
            return Optional.empty();
        }
        return Optional.of(checkString);
    }
}
