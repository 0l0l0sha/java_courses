package by.training.itcompany.validator;

import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.Technology;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TargetTeamValidator {
    private TargetTeamValidator() {
    }

    /**
     * Our logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
//todo javadoc

    public static Optional<List<String[]>> validate(List<String> data) {
        List<String[]> result = new ArrayList<>();
        int lineNum = 0;
        String[] checkString = data.remove(0).split(",");
        if (checkString.length != 1) {
            LOGGER.error("Not valid Tech " + checkString[0]);
            return Optional.empty();
        }
        for (int i = 0; i < checkString.length; i++) {
            checkString[i] = checkString[i].trim();
            checkString[i] = checkString[i].toUpperCase();
        }
        try {
            Technology.valueOf(checkString[0]);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Not valid Tech" + checkString[0]);
            return Optional.empty();
        }
        result.add(checkString);
        for (String dataLine : data) {
            if (dataLine.trim().isEmpty()) {
                continue;
            }
            checkString = dataLine.split(",");
            for (int i = 0; i < checkString.length; i++) {
                checkString[i] = checkString[i].trim();
                checkString[i] = checkString[i].toUpperCase();
            }
            if (checkString.length != 3) {
                LOGGER.error("Not enough parameters in " + lineNum);
                return Optional.empty();
            }
            int hours;
            try {
                hours = Integer.valueOf(checkString[0]);
            } catch (IllegalArgumentException e) {
                LOGGER.error("Not valid amout hours in " + lineNum);
                return Optional.empty();
            }
            if (hours < 1) {
                LOGGER.error("Not valid amout hours in " + lineNum);
                return Optional.empty();
            }
            try {
                Speciality.valueOf(checkString[1]);
            } catch (IllegalArgumentException e) {
                LOGGER.error("Not valid specialization in " + lineNum);
                return Optional.empty();
            }
            try {
                Level.valueOf(checkString[2]);
            } catch (IllegalArgumentException e) {
                LOGGER.error("Not valid level in " + lineNum);
                return Optional.empty();
            }
            result.add(checkString);
            lineNum++;
        }
        return Optional.of(result);
    }
}
