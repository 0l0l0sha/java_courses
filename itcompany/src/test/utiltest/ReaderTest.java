package test.utiltest;

import by.training.itcompany.util.Reader;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class ReaderTest {
    @DataProvider(name = "correctTest + incorrectPath")
    public static Object[][] correctData() {
        List<String> temp = new ArrayList<>();
        temp.add("Java DEV Second Hala");
        temp.add("Java QA First lol");
        return new Object[][]{
                {"data/test1.txt", temp},
                {"data/test.txt", new ArrayList<>()}};
    }

    @Test (description = "checking on correct reading and correct behavior with incorrect filepath",
            dataProvider = "correctTest + incorrectPath")
    public void correctReaderTest(String filePath, List<String> expected) {
        List<String> actual = Reader.readFileToString(filePath);
        Assert.assertEquals(actual, expected);
    }

}
