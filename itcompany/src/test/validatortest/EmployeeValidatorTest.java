package test.validatortest;



import by.training.itcompany.validator.EmployeeValidator;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmployeeValidatorTest {

    @DataProvider(name = "validData")
    public static Object[][] validData() {
        return new Object[][]{
                {"Java, DeV, Second, Vitya", new String[] {"JAVA", "DEV",  "SECOND", "VITYA"}},
                {"Java,    DEV    ,  First,    Vika", new String[] {"JAVA", "DEV",  "FIRST", "VIKA"}},
                {"Java, qa,  Second, Vika    ", new String[] {"JAVA", "QA",  "SECOND", "VIKA"}},
                {"Java    , DEV,    Second, Vika", new String[] {"JAVA", "DEV",  "SECOND", "VIKA"}},
                {"Net    , DEV,  SeCond, Vika", new String[] {"NET", "DEV",  "SECOND", "VIKA"}}
        };
    }

    @DataProvider(name = "inValidData")
    public static Object[][] inValidData() {
        return new Object[][]{
                {"Java, DEV,  First, ", Optional.empty()},
                {"Java, DEV,  Fifth, Kolya", Optional.empty()},
                {"Java, DEV,, Kolya", Optional.empty()},
                {"Java, DEV, , Kolya", Optional.empty()},
                {"Java, HR,  First, Lola", Optional.empty()},
                {"Java,,  First, Kolya", Optional.empty()},
                {"Java, ,  First, Lola", Optional.empty()},
                {"Ruby, QA,  First, Lola", Optional.empty()},
                {", DEV,  First, Kolya", Optional.empty()},
                {" , DEV,  First, Kolya", Optional.empty()},
                {"DEV,  First, Kolya", Optional.empty()},
                {"Java, First, Kolya", Optional.empty()},
                {"Java, DEV, First,", Optional.empty()},
                {"Java, DEV, First", Optional.empty()},
                {"Java, DEV,  First, Lel, ", Optional.empty()},
                {" ", Optional.empty()},
                {" \t", Optional.empty()}

        };
    }

    @Test(description = "check validator on correct data", dataProvider = "validData")
    public void correctValidatorTest(String data, String[] expected) {
        Optional<String[]> actual = EmployeeValidator.validate(data);
        Assert.assertEquals(actual.get(), expected);
    }

    @Test(description = "check validator on not enough/too many/not valid params",
            dataProvider = "inValidData")
    public void inCorrectNameValidatorTest(String data, Optional<String[]> expected) {
        Optional<String[]> actual = EmployeeValidator.validate(data);
        Assert.assertEquals(actual, expected);
    }

}

