package test.validatortest;

import by.training.itcompany.util.Reader;
import by.training.itcompany.validator.TargetTeamValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TargetTeamValidatorTest {
    private static String data = "data/";

    @DataProvider(name = "inValidData")
    public static Object[][] invalidData() {
        List<String> data1 = Reader.readFileToString(data + "IncorrectHour1.txt");
        List<String> data2 = Reader.readFileToString(data + "IncorrectHour2.txt");
        List<String> data3 = Reader.readFileToString(data + "IncorrectHour3.txt");
        List<String> data4 = Reader.readFileToString(data + "IncorrectHour4.txt");
        List<String> data5 = Reader.readFileToString(data + "IncorrectHour5.txt");
        List<String> data6 = Reader.readFileToString(data + "IncorrectLevel1.txt");
        List<String> data7 = Reader.readFileToString(data + "IncorrectLevel2.txt");
        List<String> data8 = Reader.readFileToString(data + "IncorrectSpec1.txt");
        List<String> data9 = Reader.readFileToString(data + "IncorrectSpec2.txt");
        List<String> data10 = Reader.readFileToString(data + "IncorrectSpec3.txt");
        List<String> data11 = Reader.readFileToString(data + "IncorrectTech1.txt");
        List<String> data12 = Reader.readFileToString(data + "IncorrectTech2.txt");
        List<String> data13 = Reader.readFileToString(data + "IncorrectTech3.txt");
        List<String> data14 = Reader.readFileToString(data + "NotEnoughParamEmpl1.txt");
        List<String> data15 = Reader.readFileToString(data + "NotEnoughParamEmpl2.txt");
        List<String> data16 = Reader.readFileToString(data + "NotEnoughParamEmpl3.txt");
        return new Object[][]{
                {data1, Optional.empty()},
                {data2, Optional.empty()},
                {data3, Optional.empty()},
                {data4, Optional.empty()},
                {data5, Optional.empty()},
                {data6, Optional.empty()},
                {data7, Optional.empty()},
                {data8, Optional.empty()},
                {data9, Optional.empty()},
                {data10, Optional.empty()},
                {data11, Optional.empty()},
                {data12, Optional.empty()},
                {data13, Optional.empty()},
                {data14, Optional.empty()},
                {data15, Optional.empty()},
                {data16, Optional.empty()}
        };
    }

    @DataProvider(name = "validData")
    public static Object[][] validData() {
        List<String> data1 = Reader.readFileToString(data + "CorrectJavaTargetData.txt");
        List<String> data2 = Reader.readFileToString(data + "CorrectNetTargetData.txt");
        List<String[]> temp1 = new ArrayList<>();
        List<String[]> temp2 = new ArrayList<>();
        temp1.add(new String[] {"JAVA"});
        temp2.add(new String[] {"NET"});
        String[] temp = new String[] {"20", "DEV",  "FOURTH"};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"10", "DEV",  "Third".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"5", "DEV",  "Second".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"7", "DEV",  "Second".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"8", "DEV",  "First".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"10", "DEV",  "First".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"17", "DEV",  "First".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"16", "QA",  "Second".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"17", "QA",  "Third".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);
        temp = new String[] {"7", "QA",  "First".toUpperCase()};
        temp1.add(temp);
        temp2.add(temp);

        return new Object[][]{
                {data1, temp1},
                {data2, temp2}};
    }

    @Test(description = "check validator on not enough/too many/not valid params",
            dataProvider = "inValidData")
    public void inCorrectValidatorTest(List<String> data, Optional<List<String[]>> expected) {
        Optional<List<String[]>> actual = TargetTeamValidator.validate(data);
        Assert.assertEquals(actual, expected);
    }


    @Test(description = "check validator on correct data", dataProvider = "validData")
    public void correctValidatorTest(List<String> data, List<String[]> expected) {
        Optional<List<String[]>> oActual = TargetTeamValidator.validate(data);
        List<String[]>actual = oActual.get();
        int i = 0;
        for (String[] str:actual) {
            Assert.assertEquals(str, expected.get(i));
            i++;
        }

    }
}
