package test.factorytest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.factory.EmployeeFactoryNet;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class EmployeeFactoryNetTest {

    @DataProvider(name = "correctTestDev")
    public static Object[][] correctDev() {
        return new Object[][]{
                {"Kiril", Level.FIRST, new DeveloperNet("Kiril", Level.FIRST)},
                {"Petya", Level.SECOND, new DeveloperNet("Petya", Level.SECOND)}};
    }

    @DataProvider(name = "correctTestQA")
    public static Object[][] correctQA() {
        return new Object[][]{
                {"Kiril", Level.FIRST, new QualitySpecialistNet("Kiril", Level.FIRST)},
                {"Petya", Level.SECOND, new QualitySpecialistNet("Petya", Level.SECOND)}};
    }

    @Test(description = "correct net developer", dataProvider = "correctTestDev")
    public void getDeveloperTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryNet factoryNet = new EmployeeFactoryNet();
        Employee actual = factoryNet.getDeveloper(name, level);
        Assert.assertEquals(actual, expected);
    }

    @Test(description = "QA from dev factory (incorrect)", dataProvider = "correctTestDev")
    public void getDeveloperWrongTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryNet factoryNet = new EmployeeFactoryNet();
        Employee actual = factoryNet.getQualitySpecialist(name, level);
        Assert.assertNotEquals(actual, expected);
    }

    @Test(dataProvider = "correctTestQA")
    public void getQATest(final String name, Level level, final Employee expected) {
        EmployeeFactoryNet factoryNet = new EmployeeFactoryNet();
        Employee actual = factoryNet.getQualitySpecialist(name, level);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "correctTestQA")
    public void getQAWrongTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryNet factoryNet = new EmployeeFactoryNet();
        Employee actual = factoryNet.getDeveloper(name, level);
        Assert.assertNotEquals(actual, expected);
    }
}
