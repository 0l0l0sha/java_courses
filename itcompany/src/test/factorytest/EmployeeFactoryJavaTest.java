package test.factorytest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.factory.EmployeeFactoryJava;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class EmployeeFactoryJavaTest {

    @DataProvider(name = "correctTestDev")
    public static Object[][] correctDev() {
        return new Object[][]{
                {"Kiril", Level.FIRST, new DeveloperJava("Kiril", Level.FIRST)},
                {"Petya", Level.SECOND, new DeveloperJava("Petya", Level.SECOND)}};
    }

    @DataProvider(name = "correctTestQA")
    public static Object[][] correctQa() {
        return new Object[][]{
                {"Kiril", Level.FIRST, new QualitySpecialistJava("Kiril", Level.FIRST)},
                {"Petya", Level.SECOND, new QualitySpecialistJava("Petya", Level.SECOND)}};
    }

    @Test(description = "correct java developer",dataProvider = "correctTestDev")
    public void getDeveloperTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryJava factoryJava = new EmployeeFactoryJava();
        Employee actual = factoryJava.getDeveloper(name, level);
        Assert.assertEquals(actual, expected);
    }

    @Test(description = "QA from dev factory (incorrect)",dataProvider = "correctTestDev")
    public void getDeveloperWrongTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryJava factoryJava = new EmployeeFactoryJava();
        Employee actual = factoryJava.getQualitySpecialist(name, level);
        Assert.assertNotEquals(actual, expected);
    }

    @Test(dataProvider = "correctTestQA")
    public void getQaTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryJava factoryJava = new EmployeeFactoryJava();
        Employee actual = factoryJava.getQualitySpecialist(name, level);
        Assert.assertEquals(actual, expected);
    }

    @Test(dataProvider = "correctTestQA")
    public void getQaWrongTest(final String name, Level level, final Employee expected) {
        EmployeeFactoryJava factoryJava = new EmployeeFactoryJava();
        Employee actual = factoryJava.getDeveloper(name, level);
        Assert.assertNotEquals(actual, expected);
    }

}
