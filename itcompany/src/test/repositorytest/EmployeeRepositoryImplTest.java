package test.repositorytest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.repository.EmployeeRepositoryImpl;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByBusinessAndTech;
import by.training.itcompany.repository.specification.EmployeeSpecificationByID;
import by.training.itcompany.repository.specification.EmployeeSpecificationByLevel;
import by.training.itcompany.repository.specification.EmployeeSpecificationByName;
import by.training.itcompany.repository.specification.EmployeeSpecificationByNameAndLevel;
import by.training.itcompany.repository.specification.EmployeeSpecificationBySpec;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import test.repositorytest.findtest.EmployeeSpecificationByIDTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class EmployeeRepositoryImplTest {
    private static final Logger LOGGER = LogManager.getLogger();

    @DataProvider(name = "specFindQueryData")
    public static Object[][] validData() {
        List<Employee> expectedID6 = new ArrayList<>();
        List<Employee> expectedID5 = new ArrayList<>();
        List<Employee> expectedLevel2 = new ArrayList<>();
        List<Employee> expectedLevel3 = new ArrayList<>();
        List<Employee> expectedNameLevelDJFO = new ArrayList<>();
        List<Employee> expectedNameLevelQANETFI = new ArrayList<>();
        List<Employee> expectedNameDNETTH = new ArrayList<>();
        List<Employee> expectedSpecDev = new ArrayList<>();
        List<Employee> expectedSpecQA = new ArrayList<>();
        List<Employee> expectedJavaBusy = new ArrayList<>();
        List<Employee> expectedJavaNotBusy = new ArrayList<>();
        List<Employee> expectedNetBusy = new ArrayList<>();
        List<Employee> expectedNetNotBusy = new ArrayList<>();

        EmployeeRepository specRep = new EmployeeRepositoryImpl();

        Employee djsec = new DeveloperJava("DJSEC", Level.SECOND);
        Employee djsec1 = new DeveloperJava(djsec);
        specRep.addEmployee(djsec1);
        djsec.setId(1);
        expectedLevel2.add(djsec);
        expectedSpecDev.add(djsec);
        expectedJavaNotBusy.add(djsec);


        Employee djth = new DeveloperJava("DJTH", Level.THIRD);
        Employee djth1 = new DeveloperJava(djth);
        specRep.addEmployee(djth1);
        djth.setId(2);
        expectedLevel3.add(djth);
        expectedSpecDev.add(djth);
        expectedJavaNotBusy.add(djth);

        Employee dnetsec = new DeveloperNet("DNETSEC", Level.SECOND);
        Employee dnetsec1 = new DeveloperNet(dnetsec);
        specRep.addEmployee(dnetsec1);
        dnetsec.setId(3);
        expectedLevel2.add(dnetsec);
        expectedSpecDev.add(dnetsec);
        expectedNetNotBusy.add(dnetsec);

        Employee dnetth = new DeveloperNet("DNETTH", Level.THIRD);
        Employee dnetth1 = new DeveloperNet(dnetth);
        specRep.addEmployee(dnetth1);
        dnetth.setId(4);
        expectedLevel3.add(dnetth);
        expectedNameDNETTH.add(dnetth);
        expectedSpecDev.add(dnetth);
        expectedNetNotBusy.add(dnetth);

        Employee djfi = new DeveloperJava("DJFI", Level.FIRST);
        Employee djfi1 = new DeveloperJava(djfi);
        specRep.addEmployee(djfi1);
        djfi.setId(5);
        expectedID5.add(djfi);
        expectedSpecDev.add(djfi);
        expectedJavaNotBusy.add(djfi);

        Employee djfo = new DeveloperJava("DJFO", Level.FOURTH);
        Employee djfo1 = new DeveloperJava(djfo);
        specRep.addEmployee(djfo1);
        djfo.setId(6);
        expectedID6.add(djfo);
        expectedNameLevelDJFO.add(djfo);
        expectedSpecDev.add(djfo);
        expectedJavaNotBusy.add(djfo);

        Employee dnetfo = new DeveloperNet("DNETFO", Level.FOURTH);
        dnetfo.setBusy(true);
        Employee dnetfo1 = new DeveloperNet(dnetfo);
        specRep.addEmployee(dnetfo1);
        dnetfo.setId(7);
        expectedNetBusy.add(dnetfo);
        expectedSpecDev.add(dnetfo);

        Employee dnetfi = new DeveloperNet("DNETFI", Level.FIRST);
        dnetfi.setBusy(true);
        Employee dnetfi1 = new DeveloperNet(dnetfi);
        specRep.addEmployee(dnetfi1);
        dnetfi.setId(8);
        expectedNetBusy.add(dnetfi);
        expectedSpecDev.add(dnetfi);

        Employee qajfo = new QualitySpecialistJava("QAJFO", Level.FOURTH);
        qajfo.setBusy(true);
        Employee qajfo1 = new QualitySpecialistJava(qajfo);
        specRep.addEmployee(qajfo1);
        qajfo.setId(9);
        expectedJavaBusy.add(qajfo);
        expectedSpecQA.add(qajfo);


        Employee qajfi = new QualitySpecialistJava("QAJFI", Level.FIRST);
        qajfi.setBusy(true);
        Employee qajfi1 = new QualitySpecialistJava(qajfi);
        specRep.addEmployee(qajfi1);
        qajfi.setId(10);
        expectedJavaBusy.add(qajfi);
        expectedSpecQA.add(qajfi);

        Employee qajsec = new QualitySpecialistJava("QAJSEC", Level.SECOND);
        Employee qajsec1 = new QualitySpecialistJava(qajsec);
        specRep.addEmployee(qajsec1);
        qajsec.setId(11);
        expectedLevel2.add(qajsec);
        expectedSpecQA.add(qajsec);
        expectedJavaNotBusy.add(qajsec);

        Employee qajth = new QualitySpecialistJava("QAJTH", Level.THIRD);
        Employee qajth1 = new QualitySpecialistJava(qajth);
        specRep.addEmployee(qajth1);
        qajth.setId(12);
        expectedLevel3.add(qajth);
        expectedSpecQA.add(qajth);
        expectedJavaNotBusy.add(qajth);


        Employee qanetfo = new QualitySpecialistNet("QANETFO", Level.FOURTH);
        Employee qanetfo1 = new QualitySpecialistNet(qanetfo);
        specRep.addEmployee(qanetfo1);
        qanetfo.setId(13);
        expectedSpecQA.add(qanetfo);
        expectedNetNotBusy.add(qanetfo);

        Employee qanetfi = new QualitySpecialistNet("QANETFI", Level.FIRST);
        Employee qanetfi1 = new QualitySpecialistNet(qanetfi);
        specRep.addEmployee(qanetfi1);
        qanetfi.setId(14);
        expectedNameLevelQANETFI.add(qanetfi);
        expectedSpecQA.add(qanetfi);
        expectedNetNotBusy.add(qanetfi);

        Employee qanetsec = new QualitySpecialistNet("QANETSEC", Level.SECOND);
        Employee qanetsec1 = new QualitySpecialistNet(qanetsec);
        specRep.addEmployee(qanetsec1);
        qanetsec.setId(15);
        expectedLevel2.add(qanetsec);
        expectedSpecQA.add(qanetsec);
        expectedNetNotBusy.add(qanetsec);

        Employee qanetth = new QualitySpecialistNet("QANETTH", Level.THIRD);
        Employee qanetth1 = new QualitySpecialistNet(qanetth);
        specRep.addEmployee(qanetth1);
        qanetth.setId(16);
        expectedLevel3.add(qanetth);
        expectedSpecQA.add(qanetth);
        expectedNetNotBusy.add(qanetth);

        EmployeeSpecification specificationID5 = new EmployeeSpecificationByID(5);
        EmployeeSpecification specificationID6 = new EmployeeSpecificationByID(6);
        EmployeeSpecification specificationBusyJava = new EmployeeSpecificationByBusinessAndTech(true, Technology.JAVA);
        EmployeeSpecification specificationFreeJava = new EmployeeSpecificationByBusinessAndTech(false, Technology.JAVA);
        EmployeeSpecification specificationBusyNet = new EmployeeSpecificationByBusinessAndTech(true, Technology.NET);
        EmployeeSpecification specificationFreeNet = new EmployeeSpecificationByBusinessAndTech(false, Technology.NET);
        EmployeeSpecification specificationNameDNETTH = new EmployeeSpecificationByName("dnetth");
        EmployeeSpecification specificationNameLevelDJFO = new EmployeeSpecificationByNameAndLevel("djfo", Level.FOURTH);
        EmployeeSpecification specificationNameLevelQANETFI = new EmployeeSpecificationByNameAndLevel("qanetfi", Level.FIRST);
        EmployeeSpecification specificationLevel2 = new EmployeeSpecificationByLevel(Level.SECOND);
        EmployeeSpecification specificationLevel3 = new EmployeeSpecificationByLevel(Level.THIRD);
        EmployeeSpecification specificationSpecDev = new EmployeeSpecificationBySpec(Speciality.DEV);
        EmployeeSpecification specificationSpecQA = new EmployeeSpecificationBySpec(Speciality.QA);
        return new Object[][]{
                {specRep, specificationID5, expectedID5},
                {specRep, specificationID6, expectedID6},
                {specRep, specificationBusyJava, expectedJavaBusy},
                {specRep, specificationFreeJava, expectedJavaNotBusy},
                {specRep, specificationBusyNet, expectedNetBusy},
                {specRep, specificationFreeNet, expectedNetNotBusy},
                {specRep, specificationNameDNETTH,expectedNameDNETTH},
                {specRep, specificationNameLevelDJFO,expectedNameLevelDJFO},
                {specRep, specificationNameLevelQANETFI,expectedNameLevelQANETFI},
                {specRep, specificationLevel2,expectedLevel2},
                {specRep, specificationLevel3,expectedLevel3},
                {specRep, specificationSpecDev, expectedSpecDev},
                {specRep, specificationSpecQA, expectedSpecQA},
                {specRep, new EmployeeSpecificationByID(0), new ArrayList<>()},
                {specRep, new EmployeeSpecificationByName("cheburek"), new ArrayList<>()},
                {specRep, new EmployeeSpecificationByNameAndLevel("qanetfo", Level.FIRST), new ArrayList<>()},
        };
    }

    @DataProvider(name = "addData")
    public static Object[][] addData() {
        Employee lelek = new DeveloperNet("lelek", Level.FIRST);
        Employee bolek = new DeveloperNet("bolek", Level.SECOND);
        Employee golek = new DeveloperNet("gollek", Level.THIRD);
        Employee molek = new DeveloperNet("molek", Level.FOURTH);
        EmployeeRepository rep = new EmployeeRepositoryImpl();

        return new Object[][]{
                {rep, lelek, 1},
                {rep, bolek, 2},
                {rep, golek, 3},
                {rep, molek, 4},
        };
    }





    @Test(description = "checking on correct add in repository", dataProvider = "addData")
    public void addTest(EmployeeRepository rep, Employee employee, int expected) {
        int actual = rep.addEmployee(employee);
        Assert.assertEquals(actual, expected);
    }

    @Test(description = "checking on correct getting from repository by specification (all except salary)", dataProvider = "specFindQueryData")
    public void queryTest(EmployeeRepository rep, EmployeeSpecification specification, List<Employee> expected) {
        List<Employee> actual = rep.query(specification);
        LOGGER.debug(specification.getClass().toString());
        Assert.assertEquals(actual, expected);
    }

}
