package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByID;
import by.training.itcompany.util.Reader;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class EmployeeSpecificationByIDTest  {

    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.FIRST);
        lelek.setId(5);
        Employee bolek = new DeveloperNet("bolek", Level.FIRST);
        bolek.setId(6);
        return new Object[][]{
                {5,lelek,true},
                {5,bolek,false}};
    }

    @Test(description = "checking on correct check by ID", dataProvider = "correctTest")
    public void correctSpecifideIDTest(int desiredEmployeeID, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationByID(desiredEmployeeID);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }

}
