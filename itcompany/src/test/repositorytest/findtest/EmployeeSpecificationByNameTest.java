package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByName;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmployeeSpecificationByNameTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.FIRST);
        Employee bolek = new DeveloperNet("bolek", Level.SECOND);
        Employee golek = new DeveloperNet("gollek", Level.THIRD);
        Employee molek = new DeveloperNet("molek", Level.FOURTH);
        return new Object[][]{
                {"lelek", lelek, true},
                {"bolek", bolek, true},
                {"gollek", golek, true},
                {"molek", molek, true},
                {"kek", molek, false},
        };
    }

    @Test(description = "checking on correct check by Name", dataProvider = "correctTest")
    public void correctSpecifideNameTest(String desiredEmployeeName, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationByName(desiredEmployeeName);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }

}
