package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialist;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByLevel;
import by.training.itcompany.repository.specification.EmployeeSpecificationBySpec;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static by.training.itcompany.entity.Level.*;
import static by.training.itcompany.entity.Level.FIRST;
import static by.training.itcompany.entity.Speciality.DEV;
import static by.training.itcompany.entity.Speciality.QA;

public class EmployeeSpecificationBySpecTest {

    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", FIRST);
        Employee bolek = new DeveloperJava("bolek", Level.SECOND);
        Employee golek = new QualitySpecialistNet("gollek", Level.THIRD);
        Employee molek = new QualitySpecialistJava("molek", Level.FOURTH);
        return new Object[][]{
                {DEV, lelek, true},
                {DEV, bolek, true},
                {QA, golek, true},
                {QA, molek, true},
                {DEV, molek, false},
                {QA, lelek, false},

        };
    }

    @Test(description = "checking on correct check by Speciality", dataProvider = "correctTest")
    public void correctSpecifideLevelTest(Speciality desiredEmployeeSpec, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationBySpec(desiredEmployeeSpec);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }
}
