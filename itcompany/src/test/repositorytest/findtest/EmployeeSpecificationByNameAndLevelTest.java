package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByLevel;
import by.training.itcompany.repository.specification.EmployeeSpecificationByName;
import by.training.itcompany.repository.specification.EmployeeSpecificationByNameAndLevel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static by.training.itcompany.entity.Level.*;
import static by.training.itcompany.entity.Level.FIRST;

public class EmployeeSpecificationByNameAndLevelTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", FIRST);
        Employee bolek = new DeveloperNet("bolek", Level.SECOND);
        Employee golek = new DeveloperNet("gollek", Level.THIRD);
        Employee molek = new DeveloperNet("molek", Level.FOURTH);
        return new Object[][]{
                {"lelek", FIRST, lelek, true},
                {"bolek", SECOND, bolek, true},
                {"gollek",THIRD, golek, true},
                {"molek",FOURTH, molek, true},
                {"molek",FIRST, molek, false},
                {"molek", SECOND, bolek, false},
                {"lelek", FOURTH, golek, false}

        };
    }
    @Test(description = "checking on correct check by name and level", dataProvider = "correctTest")
    public void correctSpecifideIDTest(String desiredEmployeeName, Level desiredEmployeeLevel, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationByNameAndLevel(desiredEmployeeName,desiredEmployeeLevel);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }
}
