package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.handler.EmployeeSalaryCalculator;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationBySalary;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmployeeSpecificationBySalaryTest  {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee dnfi = new DeveloperNet("lelek", Level.FIRST);
        Employee dnsec = new DeveloperNet("bolek", Level.SECOND);
        Employee dnth = new DeveloperNet("gollek", Level.THIRD);
        Employee dnfo = new DeveloperNet("molek", Level.FOURTH);

        Employee djth = new DeveloperJava("gollek", Level.THIRD);
        Employee djfo = new DeveloperJava("molek", Level.FOURTH);

        Employee qansec = new QualitySpecialistNet("bolek", Level.SECOND);


        return new Object[][]{
                { dnfi, 130,150,true},
                { dnsec, 280,290,true},
                { dnth, 410,420,true},
                { dnfo, 561,570,false},
                { djth, 380,389,false},
                { djfo, 530,540,false},
                { qansec, 40,50,false},
        };
    }


    @Test(description = "checking on correct check by salary (in the range)", dataProvider = "correctTest")
    public void correctSpecSalaryTest(Employee employee, double min, double max, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationBySalary(min,max);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }
}
