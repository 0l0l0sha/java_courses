package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Technology;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByBusinessAndTech;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static by.training.itcompany.entity.Level.FIRST;

public class EmployeeSpecificationByBusinessAndTechTest {

    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelekBusy = new DeveloperNet("lelekBusy", FIRST);
        lelekBusy.setBusy(true);
        Employee bolekBusy = new DeveloperJava("bolekBusy", Level.SECOND);
        bolekBusy.setBusy(true);
        Employee golekBusy = new QualitySpecialistNet("gollekBusy", Level.THIRD);
        golekBusy.setBusy(true);
        Employee molekBusy = new QualitySpecialistJava("molekBusy", Level.FOURTH);
        molekBusy.setBusy(true);
        Employee lelekFree = new DeveloperNet("lelekFree", FIRST);
        Employee bolekFree = new DeveloperJava("bolekFree", Level.SECOND);
        Employee golekFree = new QualitySpecialistNet("gollekFree", Level.THIRD);
        Employee molekFree = new QualitySpecialistJava("molekFree", Level.FOURTH);

        return new Object[][]{
                {true, Technology.NET, lelekBusy, true},
                {true, Technology.JAVA, bolekBusy, true},
                {true, Technology.NET, golekBusy, true},
                {true, Technology.JAVA, molekBusy, true},
                {false, Technology.NET, lelekFree, true},
                {false, Technology.JAVA, bolekFree, true},
                {false, Technology.NET, golekFree, true},
                {false, Technology.JAVA, molekFree, true},

                {false, Technology.NET, lelekBusy, false},
                {true, Technology.NET, bolekBusy, false},
                {true, Technology.JAVA, golekBusy, false},
                {false, Technology.JAVA, molekBusy, false},
                {true, Technology.NET, lelekFree, false},
                {false, Technology.NET, bolekFree, false},
                {false, Technology.JAVA, golekFree, false},
                {true, Technology.JAVA, molekFree, false},

                {false,Technology.JAVA, lelekBusy,false},
                {false,Technology.NET, bolekBusy,false},
                {true,Technology.JAVA, lelekFree,false},
                {true,Technology.NET, bolekFree,false},
        };
    }

    @Test(description = "checking on correct check by business and tech", dataProvider = "correctTest")
    public void correctSpecifideBusinessAndTechTest(boolean desiredEmployeeBusiness, Technology desiredEmployeeTech, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationByBusinessAndTech(desiredEmployeeBusiness,desiredEmployeeTech);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }

}
