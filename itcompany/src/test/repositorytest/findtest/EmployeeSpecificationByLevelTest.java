package test.repositorytest.findtest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByLevel;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static by.training.itcompany.entity.Level.*;

public class EmployeeSpecificationByLevelTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", FIRST);
        Employee bolek = new DeveloperNet("bolek", Level.SECOND);
        Employee golek = new DeveloperNet("gollek", Level.THIRD);
        Employee molek = new DeveloperNet("molek", Level.FOURTH);
        return new Object[][]{
                {FIRST, lelek, true},
                {SECOND, bolek, true},
                {THIRD, golek, true},
                {FOURTH, molek, true},
                {FIRST, molek, false},

        };
    }

    @Test(description = "checking on correct check by level", dataProvider = "correctTest")
    public void correctSpecifideLevelTest(Level desiredEmployeeLevel, Employee employee, boolean expected) {
        EmployeeSpecification specification = new EmployeeSpecificationByLevel(desiredEmployeeLevel);
        boolean actual = specification.specified(employee);
        Assert.assertEquals(actual, expected);
    }
}
