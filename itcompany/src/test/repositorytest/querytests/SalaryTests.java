package test.repositorytest.querytests;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.repository.EmployeeRepositoryImpl;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationBySalary;
import by.training.itcompany.repository.specification.EmployeeSpecificationSortBySalary;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SalaryTests {

    private static final Logger LOGGER = LogManager.getLogger();

    @DataProvider(name = "salaryData")
    public static Object[][] salaryData() {


        Employee dnfi = new DeveloperNet("dnfi", Level.FIRST);
        Employee dnsec = new DeveloperNet("dnsec", Level.SECOND);
        Employee dnfo = new DeveloperNet("dnfo", Level.FOURTH);

        Employee djth = new DeveloperJava("djth", Level.THIRD);
        Employee djfo = new DeveloperJava("djfo ", Level.FOURTH);

        Employee qansec = new QualitySpecialistNet("qansec", Level.SECOND);
        Employee qanth = new QualitySpecialistNet("qanth", Level.THIRD);

        Employee qajfi = new QualitySpecialistJava("qajfi", Level.FIRST);
        Employee qajfo = new QualitySpecialistJava("qajfo", Level.FOURTH);

        Employee dnsecTl = new DeveloperNet("dnsecTl", Level.SECOND);
        Employee djthTl = new DeveloperJava("djthTl", Level.THIRD);
        ((DeveloperNet) dnsecTl).setTeamLead(true);
        ((DeveloperJava) djthTl).setTeamLead(true);

        EmployeeRepository salaryRep = new EmployeeRepositoryImpl();
        salaryRep.addEmployee(dnfi);
        salaryRep.addEmployee(dnsec);
        salaryRep.addEmployee(dnfo);
        salaryRep.addEmployee(djth);
        salaryRep.addEmployee(djfo);
        salaryRep.addEmployee(qansec);
        salaryRep.addEmployee(qanth);
        salaryRep.addEmployee(qajfi);
        salaryRep.addEmployee(qajfo);
        salaryRep.addEmployee(dnsecTl);
        salaryRep.addEmployee(djthTl);

        List<Employee> expectedAll = new ArrayList<>();
        expectedAll.add(dnfi);
        expectedAll.add(dnsec);
        expectedAll.add(dnfo);
        expectedAll.add(djth);
        expectedAll.add(djfo);
        expectedAll.add(qansec);
        expectedAll.add(qanth);
        expectedAll.add(qajfi);
        expectedAll.add(qajfo);
        expectedAll.add(dnsecTl);
        expectedAll.add(djthTl);


        List<Employee> expected100500 = new ArrayList<>();
        expected100500.add(dnfi);
        expected100500.add(dnsec);
        expected100500.add(djth);
        expected100500.add(qajfo);
        expected100500.add(dnsecTl);

        List<Employee> expected200400 = new ArrayList<>();
        expected200400.add(dnsec);
        expected200400.add(djth);

        List<Employee> expected250350 = new ArrayList<>();
        expected250350.add(dnsec);

        List<Employee> expected0100 = new ArrayList<>();
        expected0100.add(qansec);
        expected0100.add(qanth);
        expected0100.add(qajfi);

        List<Employee> expected100200 = new ArrayList<>();
        expected100200.add(dnfi);
        expected100200.add(qajfo);


        List<Employee> expected200300 = new ArrayList<>();
        expected200300.add(dnsec);

        List<Employee> expected300400 = new ArrayList<>();
        expected300400.add(djth);

        List<Employee> expected400500 = new ArrayList<>();
        expected400500.add(dnsecTl);

        List<Employee> expected500600 = new ArrayList<>();
        expected500600.add(dnfo);
        expected500600.add(djfo);
        expected500600.add(djthTl);

        List<Employee> expected600700 = new ArrayList<>();

        List<Employee> expectedSorted = new ArrayList<>();
        expectedSorted.add(qajfi);
        expectedSorted.add(qansec);
        expectedSorted.add(qanth);
        expectedSorted.add(qajfo);
        expectedSorted.add(dnfi);
        expectedSorted.add(dnsec);
        expectedSorted.add(djth);
        expectedSorted.add(dnsecTl);
        expectedSorted.add(djfo);
        expectedSorted.add(dnfo);
        expectedSorted.add(djthTl);

        EmployeeSpecification specificationAll = new EmployeeSpecificationBySalary(0,1000);
        EmployeeSpecification specification0100 = new EmployeeSpecificationBySalary(0,100);
        EmployeeSpecification specification100200 = new EmployeeSpecificationBySalary(100,200);
        EmployeeSpecification specification200300 = new EmployeeSpecificationBySalary(200,300);
        EmployeeSpecification specification300400 = new EmployeeSpecificationBySalary(300,400);
        EmployeeSpecification specification400500 = new EmployeeSpecificationBySalary(400,500);
        EmployeeSpecification specification500600 = new EmployeeSpecificationBySalary(500,600);
        EmployeeSpecification specification600700 = new EmployeeSpecificationBySalary(600,700);
        EmployeeSpecification specification100500 = new EmployeeSpecificationBySalary(100,500);
        EmployeeSpecification specification200400 = new EmployeeSpecificationBySalary(200,400);
        EmployeeSpecification specification250350 = new EmployeeSpecificationBySalary(250,350);
        EmployeeSpecification specificationSorted = new EmployeeSpecificationSortBySalary();

        return new Object[][]{
                { salaryRep,specificationAll,expectedAll},
                { salaryRep,specification0100,expected0100},
                { salaryRep,specification100200,expected100200},
                { salaryRep,specification200300,expected200300},
                { salaryRep,specification300400,expected300400},
                { salaryRep,specification400500,expected400500},
                { salaryRep,specification500600,expected500600},
                { salaryRep,specification600700,expected600700},
                { salaryRep,specification100500,expected100500},
                { salaryRep,specification200400,expected200400},
                { salaryRep,specification250350,expected250350},
                { salaryRep,specificationSorted,expectedSorted}
        };
    }

    @Test(description = "checking on correct getting employees by salary", dataProvider = "salaryData")
    public void queryTest(EmployeeRepository rep, EmployeeSpecification specification, List<Employee> expected) {
        List<Employee> actual = rep.query(specification);
        LOGGER.debug(specification.getClass().toString());
        Assert.assertEquals(actual, expected);
    }
}
