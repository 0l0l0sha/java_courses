package test.repositorytest.querytests;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.repository.EmployeeRepositoryImpl;
import by.training.itcompany.repository.comparator.SalaryComparator;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationSortBySalary;
import by.training.itcompany.repository.specification.EmployeeSpecificationSortBySalaryAndName;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class EmployeeSpecificationSortBySalaryTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee dnfi = new DeveloperNet("lelek", Level.FIRST);
        Employee dnfi1 = new DeveloperNet("lelekk", Level.FIRST);
        Employee dnsec = new DeveloperNet("bolek", Level.SECOND);
        Employee dnfo = new DeveloperNet("molek", Level.FOURTH);

        Employee djth = new DeveloperJava("gollek", Level.THIRD);
        Employee djfo = new DeveloperJava("molek", Level.FOURTH);

        Employee qansec = new QualitySpecialistNet("bolek", Level.SECOND);

        EmployeeRepository salaryRep = new EmployeeRepositoryImpl();
        salaryRep.addEmployee(dnsec);
        salaryRep.addEmployee(qansec);
        salaryRep.addEmployee(djth);
        salaryRep.addEmployee(dnfo);
        salaryRep.addEmployee(dnfi);
        salaryRep.addEmployee(djfo);
        salaryRep.addEmployee(dnfi1);

        List<Employee> expectedAll = new ArrayList<>();
        expectedAll.add(qansec);
        expectedAll.add(dnfi);
        expectedAll.add(dnfi1);
        expectedAll.add(dnsec);
        expectedAll.add(djth);
        expectedAll.add(djfo);
        expectedAll.add(dnfo);

        EmployeeSpecification specification = new EmployeeSpecificationSortBySalary();

        return new Object[][]{
                { salaryRep,specification, expectedAll}
        };
    }

    @Test(description = "checking on correct sorting by Salary", dataProvider = "correctTest")
    public void queryTest(EmployeeRepository rep, EmployeeSpecification specification, List<Employee> expected) {
        List<Employee> actual = rep.query(specification);
        Assert.assertEquals(actual, expected);
    }

}
