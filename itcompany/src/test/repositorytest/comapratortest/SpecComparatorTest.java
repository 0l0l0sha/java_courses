package test.repositorytest.comapratortest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.comparator.SpecComparator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Comparator;

public class SpecComparatorTest  {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.FIRST);
        Employee lelekk = new DeveloperNet("lelekk", Level.FIRST);
        Employee bolek = new DeveloperJava("bolek", Level.SECOND);
        Employee golek = new QualitySpecialistNet("gollek", Level.THIRD);
        Employee molek = new QualitySpecialistJava("molek", Level.FOURTH);
        return new Object[][]{
                {lelekk, lelek, 0},
                {bolek, lelek, -4},
                {molek, bolek, 13},
                {bolek, golek, -13},
                {golek, molek, 4},

        };
    }

    @Test(description = "checking on correct spec comparison", dataProvider = "correctTest")
    public void correctSpecTest(Employee employee1, Employee employee2, int expected) {
        Comparator comp = new SpecComparator();
        int actual = comp.compare(employee1, employee2);
        Assert.assertEquals(actual, expected);
    }
}
