package test.repositorytest.comapratortest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.comparator.LevelComparator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Comparator;

public class LevelComparatorTest  {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.SECOND);
        Employee bolek = new DeveloperNet("bolek", Level.FIRST);
        Employee golek = new DeveloperNet("golek", Level.FIRST);
        Employee holek = new DeveloperNet("holek", Level.FOURTH);
        return new Object[][]{
                {bolek, lelek, -1},
                {bolek, golek, 0},
                {holek, golek, 1},
        };
    }

    @Test(description = "checking on correct level comparison", dataProvider = "correctTest")
    public void correctLevelcompTest(Employee employee1, Employee employee2, int expected) {
        Comparator comp = new LevelComparator();
        int actual = comp.compare(employee1, employee2);
        Assert.assertEquals(actual, expected);
    }

}
