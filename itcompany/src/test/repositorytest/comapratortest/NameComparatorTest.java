package test.repositorytest.comapratortest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.comparator.NameComparator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Comparator;

public class NameComparatorTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.SECOND);
        Employee bolek = new DeveloperNet("bolek", Level.FIRST);
        Employee golek = new DeveloperNet("golek", Level.FIRST);
        Employee holek = new DeveloperNet("holek", Level.FOURTH);
        Employee lolek = new DeveloperNet("lolek", Level.FOURTH);
        Employee lolekk = new DeveloperNet("lolekk", Level.FOURTH);
        return new Object[][]{
                {lelek, lelek, 0},
                {bolek, lelek, -10},
                {bolek, golek, -5},
                {holek, golek, 1},
                {lolek, lelek, 10},
                {lolek, lolekk, -1},
                {lolekk, lolek, 1},
        };
    }

    @Test(description = "checking on correct name comparison", dataProvider = "correctTest")
    public void correctNameCompTest(Employee employee1, Employee employee2, int expected) {
        Comparator comp = new NameComparator();
        int actual = comp.compare(employee1, employee2);
        Assert.assertEquals(actual, expected);
    }

}
