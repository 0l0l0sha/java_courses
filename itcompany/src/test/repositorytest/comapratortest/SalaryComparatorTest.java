package test.repositorytest.comapratortest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.comparator.SalaryComparator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Comparator;

public class SalaryComparatorTest  {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee dnfi = new DeveloperNet("lelek", Level.FIRST);
        Employee dnfi1 = new DeveloperNet("lelekk", Level.FIRST);
        Employee dnsec = new DeveloperNet("bolek", Level.SECOND);
        Employee dnth = new DeveloperNet("gollek", Level.THIRD);
        Employee dnfo = new DeveloperNet("molek", Level.FOURTH);

        Employee djth = new DeveloperJava("gollek", Level.THIRD);
        Employee djfo = new DeveloperJava("molek", Level.FOURTH);

        Employee qansec = new QualitySpecialistNet("bolek", Level.SECOND);


        return new Object[][]{
                { dnfi, dnfi1,0},
                { qansec, djfo,-1},
                { dnsec, djth,-1},
                { dnfo, dnth ,1},
                { djth, dnsec,1},

        };
    }

    @Test(description = "checking on correct salary comparison", dataProvider = "correctTest")
    public void correctSalaryTest(Employee employee1, Employee employee2, int expected) {
        Comparator comp = new SalaryComparator();
        int actual = comp.compare(employee1, employee2);
        Assert.assertEquals(actual, expected);
    }

}
