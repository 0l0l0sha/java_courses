package test.repositorytest.comapratortest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.repository.comparator.IdComparator;
import by.training.itcompany.repository.specification.EmployeeSpecification;
import by.training.itcompany.repository.specification.EmployeeSpecificationByID;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Comparator;


public class IdComparatorTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee lelek = new DeveloperNet("lelek", Level.FIRST);
        lelek.setId(5);
        Employee bolek = new DeveloperNet("bolek", Level.FIRST);
        bolek.setId(6);
        Employee golek = new DeveloperNet("golek", Level.FIRST);
        golek.setId(7);
        Employee holek = new DeveloperNet("holek", Level.FIRST);
        holek.setId(8);
        return new Object[][]{
                {lelek, lelek, 0},
                {bolek, golek, -1},
                {holek, golek, 1},
        };
    }

    @Test(description = "checking on correct id comparison",dataProvider = "correctTest")
    public void correctIDcompTest(Employee employee1, Employee employee2, int expected) {
        Comparator comp = new IdComparator();
        int actual = comp.compare(employee1, employee2);
        Assert.assertEquals(actual, expected);
    }


}
