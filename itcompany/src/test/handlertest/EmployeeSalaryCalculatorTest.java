package test.handlertest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.handler.EmployeeSalaryCalculator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public final class EmployeeSalaryCalculatorTest {
    @DataProvider(name = "correctTest")
    public static Object[][] correctData() {
        Employee dnfi = new DeveloperNet("lelek", Level.FIRST);
        Employee dnsec = new DeveloperNet("bolek", Level.SECOND);
        Employee dnth = new DeveloperNet("gollek", Level.THIRD);
        Employee dnfo = new DeveloperNet("molek", Level.FOURTH);

        Employee djth = new DeveloperJava("gollek", Level.THIRD);
        Employee djfo = new DeveloperJava("molek", Level.FOURTH);

        Employee qansec = new QualitySpecialistNet("bolek", Level.SECOND);
        Employee qanth = new QualitySpecialistNet("gollek", Level.THIRD);

        Employee qajfi = new QualitySpecialistJava("bolek", Level.FIRST);
        Employee qajfo = new QualitySpecialistJava("gollek", Level.FOURTH);

        Employee dnsecTl = new DeveloperNet("bolek", Level.SECOND);
        Employee djthTl = new DeveloperJava("gollek", Level.THIRD);
        ((DeveloperNet) dnsecTl).setTeamLead(true);
        ((DeveloperJava) djthTl).setTeamLead(true);

        return new Object[][]{
                { dnfi, 140},
                { dnsec, 280},
                { dnth, 420},
                { dnfo, 560},
                { djth, 390},
                { djfo, 520},
                { qansec, 56},
                { qanth, 84},
                { qajfi, 26},
                { qajfo, 104},
                { dnsecTl, 420},
                { djthTl, 585},

        };
    }

    @Test(description = "checking on correct salary calculation",dataProvider = "correctTest")
    public void correctSalaryTest(Employee employee, double expected) {
        double actual = EmployeeSalaryCalculator.salary(employee);
        Assert.assertEquals(actual, expected);
    }

}
