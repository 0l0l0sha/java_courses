package test.strategytest;

import by.training.itcompany.entity.Employee;
import by.training.itcompany.entity.Level;
import by.training.itcompany.entity.Speciality;
import by.training.itcompany.entity.developer.DeveloperJava;
import by.training.itcompany.entity.developer.DeveloperNet;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistJava;
import by.training.itcompany.entity.qualityspecialist.QualitySpecialistNet;
import by.training.itcompany.repository.EmployeeRepository;
import by.training.itcompany.repository.EmployeeRepositoryImpl;
import by.training.itcompany.builder.strategy.TeamStrategy;
import by.training.itcompany.team.TargetTeamMember;
import by.training.itcompany.team.Team;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Optional;


public class TeamStrategyTest {

    private static String DATA = "DATA/";
    private static final Logger LOGGER = LogManager.getLogger();

    @DataProvider(name = "StrategyFromFileIncorrect")
    public static Object[][] inCorrectData() {
        String dataEmployee = DATA + "CorrectEmployeeData";
        String dataNoEmployee = DATA + "IncorrectEmployeeData";
        String dataJava = DATA + "CorrectJavaTargetData";
        String dataNet = DATA + "CorrectNetTargetData";
        String data1 = DATA + "IncorrectHour1.txt";
        String data2 = DATA + "IncorrectHour2.txt";
        String data3 = DATA + "IncorrectHour3.txt";
        String data4 = DATA + "IncorrectHour4.txt";
        String data5 = DATA + "IncorrectHour5.txt";
        String data6 = DATA + "IncorrectLevel1.txt";
        String data7 = DATA + "IncorrectLevel2.txt";
        String data8 = DATA + "IncorrectSpec1.txt";
        String data9 = DATA + "IncorrectSpec2.txt";
        String data10 = DATA + "IncorrectSpec3.txt";
        String data11 = DATA + "IncorrectTech1.txt";
        String data12 = DATA + "IncorrectTech2.txt";
        String data13 = DATA + "IncorrectTech3.txt";
        String data14 = DATA + "NotEnoughParamEmpl1.txt";
        String data15 = DATA + "NotEnoughParamEmpl2.txt";
        String data16 = DATA + "NotEnoughParamEmpl3.txt";
        String data17 = DATA + "TooMuchFourth.txt";

        return new Object[][]{
                {data1,dataEmployee, Optional.empty()},
                {data2,dataEmployee, Optional.empty()},
                {data3,dataEmployee, Optional.empty()},
                {data4,dataEmployee, Optional.empty()},
                {data5,dataEmployee, Optional.empty()},
                {data6,dataEmployee, Optional.empty()},
                {data7,dataEmployee, Optional.empty()},
                {data8,dataEmployee, Optional.empty()},
                {data9,dataEmployee, Optional.empty()},
                {data10,dataEmployee, Optional.empty()},
                {data11,dataEmployee, Optional.empty()},
                {data12,dataEmployee, Optional.empty()},
                {data13,dataEmployee, Optional.empty()},
                {data14,dataEmployee, Optional.empty()},
                {data15,dataEmployee, Optional.empty()},
                {data16,dataEmployee, Optional.empty()},
                {data17,dataEmployee, Optional.empty()},
                {dataJava,dataNoEmployee, Optional.empty()},
                {dataNet,dataNoEmployee, Optional.empty()},
        };
    }

    @DataProvider(name = "StrategyFromFileCorrect")
    public static Object[][] correctData() {
        String dataEmployee = DATA + "CorrectEmployeeData.txt";
        String dataJava = DATA + "CorrectJavaTargetData.txt";
        String dataNet = DATA + "CorrectNetTargetData.txt";
        TargetTeamMember d4 = new TargetTeamMember(20, Speciality.DEV, Level.FOURTH);
        TargetTeamMember d3 = new TargetTeamMember(10, Speciality.DEV,Level.THIRD);
        TargetTeamMember d21 = new TargetTeamMember(5, Speciality.DEV,Level.SECOND);
        TargetTeamMember d22 = new TargetTeamMember(7, Speciality.DEV,Level.SECOND);
        TargetTeamMember d11 = new TargetTeamMember(8, Speciality.DEV,Level.FIRST);
        TargetTeamMember d12 = new TargetTeamMember(10, Speciality.DEV,Level.FIRST);
        TargetTeamMember d13 = new TargetTeamMember(17, Speciality.DEV,Level.FIRST);
        TargetTeamMember q3 = new TargetTeamMember(17, Speciality.QA,Level.THIRD);
        TargetTeamMember q2 = new TargetTeamMember(16, Speciality.QA,Level.SECOND);
        TargetTeamMember q1 = new TargetTeamMember(7, Speciality.QA,Level.FIRST);
        d4.setFlag(true);
        d3.setFlag(true);
        d21.setFlag(true);
        d22.setFlag(true);
        d11.setFlag(true);
        d12.setFlag(true);
        d13.setFlag(true);
        q3.setFlag(true);
        q2.setFlag(true);
        q1.setFlag(true);

        Team teamJava = new Team();
        Team teamNet = new Team();
        Employee vasya = new DeveloperJava("VASYA",Level.FOURTH);
        ((DeveloperJava) vasya).setTeamLead(true);
        Employee ilya = new DeveloperJava("ILYA",Level.THIRD);
        Employee helen = new DeveloperJava("HELEN",Level.SECOND);
        Employee olya = new DeveloperJava("OLYA",Level.SECOND);
        Employee petya = new DeveloperJava("PETYA",Level.FIRST);
        Employee kolya = new DeveloperJava("KOLYA",Level.FIRST);
        Employee katya = new DeveloperJava("KATYA",Level.FIRST);
        Employee santa = new QualitySpecialistJava("SANTA",Level.THIRD);
        Employee jacob = new QualitySpecialistJava("JACOB",Level.SECOND);
        Employee kirill = new QualitySpecialistJava("KIRILL",Level.FIRST);
        teamJava.addMember(helen,d21);
        teamJava.addMember(ilya,d3);
        teamJava.addMember(petya,d11);
        teamJava.addMember(vasya,d4);
        teamJava.addMember(kolya,d12);
        teamJava.addMember(olya,d22);
        teamJava.addMember(katya,d13);
        teamJava.addMember(jacob,q2);
        teamJava.addMember(santa,q3);
        teamJava.addMember(kirill,q1);

        Employee umbra = new DeveloperNet("UMBRA",Level.FOURTH);
        ((DeveloperNet) umbra).setTeamLead(true);
        Employee lauren = new DeveloperNet("LAUREN",Level.THIRD);
        Employee alabama = new DeveloperNet("ALABAMA",Level.SECOND);
        Employee ciri = new DeveloperNet("CIRI",Level.SECOND);
        Employee sergey = new DeveloperNet("SERGEY",Level.FIRST);
        Employee nikita = new DeveloperNet("NIKITA",Level.FIRST);
        Employee gosha = new DeveloperNet("GOSHA",Level.FIRST);
        Employee stiven = new QualitySpecialistNet("STIVEN",Level.SECOND);
        Employee oleg = new QualitySpecialistNet("OLEG",Level.THIRD);
        Employee danila = new QualitySpecialistNet("DANILA",Level.FIRST);
        teamNet.addMember(sergey,d11);
        teamNet.addMember(nikita,d12);
        teamNet.addMember(gosha,d13);
        teamNet.addMember(ciri,d21);
        teamNet.addMember(alabama,d22);
        teamNet.addMember(lauren,d3);
        teamNet.addMember(umbra,d4);
        teamNet.addMember(stiven,q2);
        teamNet.addMember(oleg,q3);
        teamNet.addMember(danila,q1);
        return new Object[][]{
                {dataJava,dataEmployee, teamJava},
                {dataNet,dataEmployee, teamNet},
        };
    }

    @DataProvider(name = "StrategyFromRepositoryCorrect")
    public static Object[][] repCorData() {
        String dataJava = DATA + "CorrectJavaTargetData.txt";
        String dataNet = DATA + "CorrectNetTargetData.txt";
        TargetTeamMember d4 = new TargetTeamMember(20, Speciality.DEV, Level.FOURTH);
        TargetTeamMember d3 = new TargetTeamMember(10, Speciality.DEV,Level.THIRD);
        TargetTeamMember d21 = new TargetTeamMember(5, Speciality.DEV,Level.SECOND);
        TargetTeamMember d22 = new TargetTeamMember(7, Speciality.DEV,Level.SECOND);
        TargetTeamMember d11 = new TargetTeamMember(8, Speciality.DEV,Level.FIRST);
        TargetTeamMember d12 = new TargetTeamMember(10, Speciality.DEV,Level.FIRST);
        TargetTeamMember d13 = new TargetTeamMember(17, Speciality.DEV,Level.FIRST);
        TargetTeamMember q3 = new TargetTeamMember(17, Speciality.QA,Level.THIRD);
        TargetTeamMember q2 = new TargetTeamMember(16, Speciality.QA,Level.SECOND);
        TargetTeamMember q1 = new TargetTeamMember(7, Speciality.QA,Level.FIRST);
        d4.setFlag(true);
        d3.setFlag(true);
        d21.setFlag(true);
        d22.setFlag(true);
        d11.setFlag(true);
        d12.setFlag(true);
        d13.setFlag(true);
        q3.setFlag(true);
        q2.setFlag(true);
        q1.setFlag(true);

        EmployeeRepository repository = new EmployeeRepositoryImpl();
        Team teamJava = new Team();
        Team teamNet = new Team();

        Employee vasya = new DeveloperJava("VASYA",Level.FOURTH);
        Employee ilya = new DeveloperJava("ILYA",Level.THIRD);
        Employee helen = new DeveloperJava("HELEN",Level.SECOND);
        Employee olya = new DeveloperJava("OLYA",Level.SECOND);
        Employee petya = new DeveloperJava("PETYA",Level.FIRST);
        Employee kolya = new DeveloperJava("KOLYA",Level.FIRST);
        Employee katya = new DeveloperJava("KATYA",Level.FIRST);
        Employee santa = new QualitySpecialistJava("SANTA",Level.THIRD);
        Employee jacob = new QualitySpecialistJava("JACOB",Level.SECOND);
        Employee kirill = new QualitySpecialistJava("KIRILL",Level.FIRST);

        Employee vasya1 = new DeveloperJava("VASYA",Level.FOURTH);
        Employee ilya1 = new DeveloperJava("ILYA",Level.THIRD);
        Employee helen1 = new DeveloperJava("HELEN",Level.SECOND);
        Employee olya1 = new DeveloperJava("OLYA",Level.SECOND);
        Employee petya1 = new DeveloperJava("PETYA",Level.FIRST);
        Employee kolya1 = new DeveloperJava("KOLYA",Level.FIRST);
        Employee katya1 = new DeveloperJava("KATYA",Level.FIRST);
        Employee santa1 = new QualitySpecialistJava("SANTA",Level.THIRD);
        Employee jacob1 = new QualitySpecialistJava("JACOB",Level.SECOND);
        Employee kirill1 = new QualitySpecialistJava("KIRILL",Level.FIRST);


        repository.addEmployee(helen1);
        repository.addEmployee(ilya1);
        repository.addEmployee(petya1);
        repository.addEmployee(vasya1);
        repository.addEmployee(kolya1);
        repository.addEmployee(olya1);
        repository.addEmployee(katya1);
        repository.addEmployee(jacob1);
        repository.addEmployee(santa1);
        repository.addEmployee(kirill1);

        ((DeveloperJava) vasya).setTeamLead(true);
        teamJava.addMember(helen,d21);
        teamJava.addMember(ilya,d3);
        teamJava.addMember(petya,d11);
        teamJava.addMember(vasya,d4);
        teamJava.addMember(kolya,d12);
        teamJava.addMember(olya,d22);
        teamJava.addMember(katya,d13);
        teamJava.addMember(jacob,q2);
        teamJava.addMember(santa,q3);
        teamJava.addMember(kirill,q1);

        Employee umbra = new DeveloperNet("UMBRA",Level.FOURTH);
        Employee lauren = new DeveloperNet("LAUREN",Level.THIRD);
        Employee alabama = new DeveloperNet("ALABAMA",Level.SECOND);
        Employee ciri = new DeveloperNet("CIRI",Level.SECOND);
        Employee sergey = new DeveloperNet("SERGEY",Level.FIRST);
        Employee nikita = new DeveloperNet("NIKITA",Level.FIRST);
        Employee gosha = new DeveloperNet("GOSHA",Level.FIRST);
        Employee stiven = new QualitySpecialistNet("STIVEN",Level.SECOND);
        Employee oleg = new QualitySpecialistNet("OLEG",Level.THIRD);
        Employee danila = new QualitySpecialistNet("DANILA",Level.FIRST);

        Employee umbra1 = new DeveloperNet("UMBRA",Level.FOURTH);
        Employee lauren1 = new DeveloperNet("LAUREN",Level.THIRD);
        Employee alabama1 = new DeveloperNet("ALABAMA",Level.SECOND);
        Employee ciri1 = new DeveloperNet("CIRI",Level.SECOND);
        Employee sergey1 = new DeveloperNet("SERGEY",Level.FIRST);
        Employee nikita1 = new DeveloperNet("NIKITA",Level.FIRST);
        Employee gosha1 = new DeveloperNet("GOSHA",Level.FIRST);
        Employee stiven1 = new QualitySpecialistNet("STIVEN",Level.SECOND);
        Employee oleg1 = new QualitySpecialistNet("OLEG",Level.THIRD);
        Employee danila1 = new QualitySpecialistNet("DANILA",Level.FIRST);

        repository.addEmployee(sergey1);
        repository.addEmployee(nikita1);
        repository.addEmployee(gosha1);
        repository.addEmployee(ciri1);
        repository.addEmployee(alabama1);
        repository.addEmployee(lauren1);
        repository.addEmployee(umbra1);
        repository.addEmployee(stiven1);
        repository.addEmployee(oleg1);
        repository.addEmployee(danila1);

        ((DeveloperNet) umbra).setTeamLead(true);
        teamNet.addMember(sergey,d11);
        teamNet.addMember(nikita,d12);
        teamNet.addMember(gosha,d13);
        teamNet.addMember(ciri,d21);
        teamNet.addMember(alabama,d22);
        teamNet.addMember(lauren,d3);
        teamNet.addMember(umbra,d4);
        teamNet.addMember(stiven,q2);
        teamNet.addMember(oleg,q3);
        teamNet.addMember(danila,q1);
        return new Object[][]{
                {dataJava,repository, teamJava},
                {dataNet,repository, teamNet},
        };
    }

    @DataProvider(name = "StrategyFromHalfEmptyRepository")
    public static Object[][] repInCorData() {
        String dataJava = DATA + "CorrectJavaTargetData.txt";

        EmployeeRepository repository = new EmployeeRepositoryImpl();

        Employee vasya = new DeveloperJava("VASYA",Level.FOURTH);
        Employee ilya = new DeveloperJava("ILYA",Level.THIRD);
        Employee helen = new DeveloperJava("HELEN",Level.SECOND);
        Employee petya = new DeveloperJava("PETYA",Level.FIRST);
        Employee kolya = new DeveloperJava("KOLYA",Level.FIRST);

        repository.addEmployee(helen);
        repository.addEmployee(ilya);
        repository.addEmployee(petya);
        repository.addEmployee(vasya);
        repository.addEmployee(kolya);

        Employee alabama = new DeveloperNet("ALABAMA",Level.SECOND);
        Employee ciri = new DeveloperNet("CIRI",Level.SECOND);
        Employee sergey = new DeveloperNet("SERGEY",Level.FIRST);
        Employee nikita = new DeveloperNet("NIKITA",Level.FIRST);
        Employee gosha = new DeveloperNet("GOSHA",Level.FIRST);

        repository.addEmployee(sergey);
        repository.addEmployee(nikita);
        repository.addEmployee(gosha);
        repository.addEmployee(ciri);
        repository.addEmployee(alabama);


        return new Object[][]{
                {dataJava,repository, Optional.empty()},
        };
    }

    @Test(description = "checking on correct behavior with not enough employees in file", dataProvider = "StrategyFromFileIncorrect")
    public void inCorrectStratTest(String teamPath,String employeePath, Optional<Team> expected) {
        EmployeeRepository repository = new EmployeeRepositoryImpl();
        Optional<Team> actual = TeamStrategy.fileTeamCreate(repository,teamPath,employeePath);
        Assert.assertEquals(actual, expected);
    }


    @Test(description = "creating team from a file", dataProvider = "StrategyFromFileCorrect")
    public void correctStratTest(String teamPath,String employeePath, Team expected) {
        EmployeeRepository repository = new EmployeeRepositoryImpl();
        Optional<Team> actual = TeamStrategy.fileTeamCreate(repository,teamPath,employeePath);
        Assert.assertEquals(actual.get(), expected);
    }


    @Test(description = "creating team from a rep", dataProvider = "StrategyFromRepositoryCorrect")
    public void correctStratRepTest(String teamPath,EmployeeRepository repository, Team expected) {
        Optional<Team> actual = TeamStrategy.reposytoryTeamCreate(repository,teamPath);
        Assert.assertEquals(actual.get(), expected);
    }

    @Test(description = "checking on correct behavior with not enough employees in repository", dataProvider = "StrategyFromHalfEmptyRepository")
    public void inCorrectStratRepTest(String teamPath,EmployeeRepository repository, Optional<Team> expected) {
        Optional<Team> actual = TeamStrategy.reposytoryTeamCreate(repository,teamPath);
        Assert.assertEquals(actual, expected);
    }
}
