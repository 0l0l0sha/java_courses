import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

class PhaseThread implements Runnable {
    Phaser phaser;
    String name;
    PhaseThread(Phaser p, String n) {
        this.phaser = p;
        this.name = n;
        phaser.register();
    }
    public void run() {
        System.out.println(name + " выполняет фазу "
        +phaser.getPhase());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        phaser.arriveAndAwaitAdvance(); // сообщаем, что первая фаза достигнута
        System.out.println(name + " выполняет фазу "
        +phaser.getPhase());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        phaser.arriveAndAwaitAdvance(); // сообщаем, что вторая фаза достигнута
        System.out.println(name + " выполняет фазу "
        +phaser.getPhase());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        phaser.arriveAndDeregister(); // сообщаем о завершении фаз и удаляем с регистрации объекты
    }
}
