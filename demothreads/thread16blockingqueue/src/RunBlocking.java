import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class RunBlocking {
    public static void main(String[] args) {
        final BlockingQueue<String> queue = new ArrayBlockingQueue<>(4);
        new Thread() {


            public void run() {
                for (int i = 1; i < 10; i++) {
                    try {
                        queue.put("Java" + i); // добавление 3-х
                        System.out.println("Element " + i + " added");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
        new Thread() {
            public void run() {
                for (int i = 1; i < 9; i++) {
                    try {
                        Thread.sleep(1_000);
// извлечение одного
                        System.out.println("Element " + queue.take() + " took");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
