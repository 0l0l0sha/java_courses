public class Store {
    int product = 0; // счетчик товаров

    // синхронизированный метод для производителей
    public synchronized void put() {
        while (product >= 3) //если товаров меньше
        {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        product++; // кладем товар
        System.out.println("Производитель добавил 1 товар");
        System.out.println("склад имеет " + product + " товар(ов)");
        notifyAll();

    }

    // метод для покупателей
    public synchronized void get() {
        while (product < 1) //если хоть один товар присутствует
        {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        product--; //берем товар
        System.out.println("Покупатель взял 1 товар");
        System.out.println("склад имеет " + product + " товар(ов)");
        notifyAll();
    }
}
